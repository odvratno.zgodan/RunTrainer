#<trkpt lat="45.7954020" lon="15.9821870">
#    <ele>114.1</ele>
#    <time>2018-11-15T16:54:09Z</time>
#</trkpt>	

import math
from datetime import datetime

header = """<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="StravaGPX Android" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd" version="1.1" xmlns="http://www.topografix.com/GPX/1/1">
 <metadata>
  <time>2018-11-15T16:52:34Z</time>
 </metadata>
 <trk>
  <name>Constant speed</name>
  <type>9</type>
  <trkseg>
"""

footer = """</trkseg>
 </trk>
</gpx>
"""


R = 6378.1 #Radius of the Earth
brng = math.radians(270) #Bearing is 90 degrees converted to radians.
d = (1.0/(6.0*60.0)) #Distance in km for pace 6:00 min/Km
t = 1542300849

lat1 = math.radians(45.7954020) #Current lat point converted to radians
lon1 = math.radians(15.9821870) #Current long point converted to radians

f = open("constant_speed.gpx","w") #opens file with name of "test.txt"
f.write("")
f.close()

f = open("constant_speed.gpx","a") #opens file with name of "test.txt"

duration = 30*60 # 30 minutes * 60 seconds
f.write(header)

for num in range(1,duration):
	f.write("   <trkpt lat=\"{0}\" lon=\"{1}\">\n".format(math.degrees(lat1), math.degrees(lon1)))
	f.write("    <ele>114.1</ele>\n")
	f.write("    <time>{0}</time>\n".format( datetime.utcfromtimestamp(t).strftime('%Y-%m-%dT%H:%M:%SZ') ))
	f.write("   </trkpt>\n")

	lat2 = math.asin( math.sin(lat1)*math.cos(d/R) +
    math.cos(lat1)*math.sin(d/R)*math.cos(brng))

	lon2 = lon1 + math.atan2(math.sin(brng)*math.sin(d/R)*math.cos(lat1),
	             math.cos(d/R)-math.sin(lat1)*math.sin(lat2))

	lat1 = lat2
	lon1 = lon2
	t+=1
f.write(footer)

f.close()