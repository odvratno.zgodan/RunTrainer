package no.none.runtrainer

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import mu.KotlinLogging
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.UserModel
import no.none.runtrainer.ui.databaseutil.DatabaseUtilActivity
import no.none.runtrainer.ui.login.LoginActivity
import no.none.runtrainer.ui.main.ProgramsFragment
import no.none.runtrainer.ui.main.SessionsFragment
import no.none.runtrainer.ui.main.TabsPagerAdapter
import no.none.runtrainer.ui.programdetails.ProgramDetailsActivity
import no.none.runtrainer.ui.settings.SettingsActivity


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val logger = KotlinLogging.logger {}

    private var userModel = UserModel()

    private lateinit var tab: TabLayout
    private lateinit var vp: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        logger.info ( "onCreate()" )

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { _ ->
            var program = DataVOs.Program()
            startActivity(ProgramDetailsActivity.startIntent(this, program), null)
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        vp= findViewById(R.id.viewpager)
        addPages(vp)

        tab= findViewById(R.id.tabs)
        tab.tabGravity = TabLayout.GRAVITY_FILL
        tab.setupWithViewPager(vp)

        if(!userModel.getUser()!!.isAnonymous){
            nav_view.menu.findItem(R.id.nav_authentication).isVisible = false
        }
        if(!BuildConfig.DEBUG){
            nav_view.menu.findItem(R.id.nav_database).isVisible = false
        }

    }

    private fun addPages(viewPager: ViewPager) {
        val myPagerAdapter = TabsPagerAdapter(supportFragmentManager)
        myPagerAdapter.addPage(ProgramsFragment())
        myPagerAdapter.addPage(SessionsFragment())

        viewPager.adapter = myPagerAdapter
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_authentication -> {
                startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                finish()
            }
            R.id.nav_database -> {
                startActivity(Intent(this@MainActivity, DatabaseUtilActivity::class.java))
                finish()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
