package no.none.runtrainer.data.DAOs

import androidx.lifecycle.LiveData
import androidx.room.*
import no.none.runtrainer.data.DataVOs

@Dao
interface ProgramDao {

    @Query("SELECT * FROM programs")
    fun all(): LiveData<List<DataVOs.Program>>

    @Insert
    fun insert(program: DataVOs.Program): Long

    @Update
    fun update(program: DataVOs.Program)

    @Delete
    fun delete(program: DataVOs.Program)

    @Query("SELECT * FROM programs WHERE programUid IS :progId LIMIT 1")
    fun getById(progId:Int):DataVOs.Program
}