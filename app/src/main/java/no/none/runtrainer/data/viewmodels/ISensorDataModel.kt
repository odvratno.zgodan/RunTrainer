package no.none.runtrainer.data.viewmodels

import androidx.lifecycle.LiveData
import android.os.AsyncTask
import no.none.runtrainer.data.AppDatabase
import no.none.runtrainer.data.DataVOs

interface ISensorDataModel {

    val appDb: AppDatabase
    var sensorDataLiveData: LiveData<List<DataVOs.SensorData>>

    fun getSensorData(): LiveData<List<DataVOs.SensorData>> {
        return sensorDataLiveData
    }

    fun getSensorDataById(id:Int, callback: (DataVOs.SensorData?) -> Unit){
        ISensorDataModel.GetByIdAsynTask(appDb, callback).execute(id)
    }

    fun getSensorDataByIdLiveData(id:Int): LiveData<DataVOs.SensorData?> {
        return appDb.sensorDataDao().getByIdLiveData(id)
    }

    fun getSensorDataBySessionId(sessionId:Int, callback: (DataVOs.SensorData?) -> Unit){
        ISensorDataModel.GetByIdSessionAsynTask(appDb, callback).execute(sessionId)
    }

    fun getSensorDataBySessionIdLiveData(sessionId:Int): LiveData<DataVOs.SensorData?> {
        return appDb.sensorDataDao().getByIdLiveData(sessionId)
    }

    fun addSensorData(sensorData: DataVOs.SensorData){
        ISensorDataModel.InsertAsynTask(appDb).execute(sensorData)
    }

    fun addSensorData(sensorData: DataVOs.SensorData, callback: (Long?) -> Unit){
        ISensorDataModel.InsertAsynTaskWithCallback(appDb, callback).execute(sensorData)
    }

    fun updateSensorData(sensorData: DataVOs.SensorData){
        ISensorDataModel.UpdateAsynTask(appDb).execute(sensorData)
    }

    fun updateSensorData(sensorData: DataVOs.SensorData, callback: () -> Unit){
        ISensorDataModel.UpdateAsynTaskWithCallback(appDb, callback).execute(sensorData)
    }

    fun deleteSensorData(sensorData: DataVOs.SensorData){
        ISensorDataModel.DeleteAsynTask(appDb).execute(sensorData)
    }

    class InsertAsynTask(private var db: AppDatabase) : AsyncTask<DataVOs.SensorData, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.SensorData): Void? {
            db.sensorDataDao().insert(params[0])
            return null
        }
    }

    class InsertAsynTaskWithCallback(private var db: AppDatabase, private val callback: (Long?) -> Unit) : AsyncTask<DataVOs.SensorData, Void, Long?>() {
        override fun doInBackground(vararg params: DataVOs.SensorData): Long? {
            db.sensorDataDao().insert(params[0])
            return null
        }

        override fun onPostExecute(result: Long?) {
            super.onPostExecute(result)
            if(result==null)
            {
                callback(null)
            }
            else
            {
                callback(result)
            }
        }

    }

    class UpdateAsynTask(private var db: AppDatabase) : AsyncTask<DataVOs.SensorData, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.SensorData): Void? {
            db.sensorDataDao().update(params[0])
            return null
        }
    }

    class UpdateAsynTaskWithCallback(private var db: AppDatabase, private val callback: () -> Unit) : AsyncTask<DataVOs.SensorData, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.SensorData): Void? {
            db.sensorDataDao().update(params[0])
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            //callback function to be executed after getting the result
            callback()
        }
    }


    class DeleteAsynTask(private var db: AppDatabase) : AsyncTask<DataVOs.SensorData, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.SensorData): Void? {
            db.sensorDataDao().delete(params[0])
            return null
        }
    }

    class GetByIdAsynTask(private var db: AppDatabase, private val callback: (DataVOs.SensorData?) -> Unit) : AsyncTask<Int, Void, DataVOs.SensorData?>() {
        override fun doInBackground(vararg params: Int?): DataVOs.SensorData? {
            return db.sensorDataDao().getById(params[0]!!)
        }

        override fun onPostExecute(result: DataVOs.SensorData?) {
            super.onPostExecute(result)
            if(result==null)
            {
                callback(null)
            }
            else
            {
                callback(result)
            }
        }
    }

    class GetByIdSessionAsynTask(private var db: AppDatabase, private val callback: (DataVOs.SensorData?) -> Unit) : AsyncTask<Int, Void, DataVOs.SensorData?>() {
        override fun doInBackground(vararg params: Int?): DataVOs.SensorData? {
            return db.sensorDataDao().getBySessionId(params[0]!!)
        }

        override fun onPostExecute(result: DataVOs.SensorData?) {
            super.onPostExecute(result)
            if(result==null)
            {
                callback(null)
            }
            else
            {
                callback(result)
            }
        }
    }
}