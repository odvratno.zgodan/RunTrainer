package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import no.none.runtrainer.data.DataVOs
import java.util.*


class SegmentConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun toString(segments: ArrayList<DataVOs.Segment>): String {
            val gs=Gson()
            return gs.toJson(segments)
        }

        @JvmStatic
        @TypeConverter
        fun toSegmentsArrayList(string:String): ArrayList<DataVOs.Segment> {
            val gson = Gson()
            val itemType = object : TypeToken<ArrayList<DataVOs.Segment>>() {}.type
            return gson.fromJson<ArrayList<DataVOs.Segment>>(string, itemType)
        }
    }
}