package no.none.runtrainer.data.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import no.none.runtrainer.data.AppDatabase
import no.none.runtrainer.data.DataVOs

class SessionViewModel(application: Application): AndroidViewModel(application), ISessionModel {

    override val appDb: AppDatabase = AppDatabase.getDataBase(application)
    override var sessionsLiveData: LiveData<List<DataVOs.Session>> = appDb.sessionDao().all()
}