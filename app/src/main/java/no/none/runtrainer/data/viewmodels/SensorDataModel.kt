package no.none.runtrainer.data.viewmodels

import android.app.Application
import androidx.lifecycle.LiveData
import android.os.AsyncTask
import no.none.runtrainer.data.AppDatabase
import no.none.runtrainer.data.DataVOs

class SensorDataModel(application: Application):ISensorDataModel {

    override val appDb: AppDatabase = AppDatabase.getDataBase(application)
    override var sensorDataLiveData: LiveData<List<DataVOs.SensorData>> = appDb.sensorDataDao().all()
}