package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class LongConverter {
    companion object {
        private val gs= Gson()
        @JvmStatic
        @TypeConverter
        fun toString(floatReadings: ArrayList<Long>?): String? {
            return if (floatReadings != null) {
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, LongConverter().DataGsonSerializer())
                        .create()
                gs.toJson(floatReadings)
            }else{
                ""
            }
        }

        @JvmStatic
        @TypeConverter
        fun toHeartRateReadings(value:String?): ArrayList<Long>? {
            return if(value != null && value.isNotEmpty()){
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, LongConverter().DataGsonDeserializer())
                        .create()
                val itemType = object : TypeToken<ArrayList<Long>>() {}.type
                gs.fromJson<ArrayList<Long>>(value, itemType)
            }else{
                null
            }
        }
    }

    inner class DataGsonSerializer: JsonSerializer<ArrayList<Long>> {
        private val gs= Gson()

        override fun serialize(src: ArrayList<Long>, typeOfSrc: Type, context: JsonSerializationContext): JsonArray? {
            val jsonLocationsArray = JsonArray()
            for (i in 0 until src.size){
                val jsonLocation = JsonPrimitive(gs.toJson(src[i]))
                jsonLocationsArray.add(jsonLocation)
            }
            return jsonLocationsArray
        }
    }

    inner class DataGsonDeserializer: JsonDeserializer<ArrayList<Long>> {
        private val gs = Gson()

        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ArrayList<Long> {
            val jsonGPSArray = json.asJsonArray

            val itemType = object : TypeToken<Long>() {}.type
            val list: ArrayList<Long> = ArrayList(1)
            for (i in 0 until jsonGPSArray.size()) {
                list.add(gs.fromJson<Long>(jsonGPSArray[i].asString, itemType))
            }
            return list
        }
    }
}