package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import java.util.*

class DateConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun fromDate(date: Date?): Long? {
            return if (date != null) {
                date.time
            }else{
                null
            }
        }

        @JvmStatic
        @TypeConverter
        fun toDate(time:Long?): Date? {
            return if(time != null){
                Date(time)
            }else{
                null
            }
        }
    }
}