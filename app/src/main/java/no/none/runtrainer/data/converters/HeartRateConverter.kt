package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import no.none.runtrainer.data.DataVOs
import java.lang.reflect.Type

class HeartRateConverter {
    companion object {
        private val gs= Gson()
        @JvmStatic
        @TypeConverter
        fun toString(heartRateReadings: ArrayList<DataVOs.HeartRateReading>?): String? {
            return if (heartRateReadings != null) {
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, HeartRateConverter().DataGsonSerializer())
                        .create()
                gs.toJson(heartRateReadings)
            }else{
                ""
            }
        }

        @JvmStatic
        @TypeConverter
        fun toHeartRateReadings(value:String?): ArrayList<DataVOs.HeartRateReading>? {
            return if(value != null && !value.isEmpty()){
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, HeartRateConverter().DataGsonDeserializer())
                        .create()
                val itemType = object : TypeToken<ArrayList<DataVOs.HeartRateReading>>() {}.type
                gs.fromJson<ArrayList<DataVOs.HeartRateReading>>(value, itemType)
            }else{
                null
            }
        }
    }

    inner class DataGsonSerializer: JsonSerializer<ArrayList<DataVOs.HeartRateReading>> {
        private val gs= Gson()

        override fun serialize(src: ArrayList<DataVOs.HeartRateReading>, typeOfSrc: Type, context: JsonSerializationContext): JsonArray? {
            val jsonLocationsArray = JsonArray()
            for (i in 0 until src.size){
                val jsonLocation = JsonPrimitive(gs.toJson(src[i]))
                jsonLocationsArray.add(jsonLocation)
            }
            return jsonLocationsArray
        }
    }

    inner class DataGsonDeserializer: JsonDeserializer<ArrayList<DataVOs.HeartRateReading>> {
        private val gs = Gson()

        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ArrayList<DataVOs.HeartRateReading> {
            val jsonGPSArray = json.asJsonArray

            val itemType = object : TypeToken<DataVOs.HeartRateReading>() {}.type
            val list: ArrayList<DataVOs.HeartRateReading> = ArrayList(1)
            for (i in 0 until jsonGPSArray.size()) {
                list.add(gs.fromJson<DataVOs.HeartRateReading>(jsonGPSArray[i].asString, itemType))
            }
            return list
        }
    }
}