package no.none.runtrainer.data.viewmodels

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import mu.KotlinLogging
import no.none.runtrainer.R


class UserModel {

    companion object {
        const val USERS = "users"
    }

    private val logger = KotlinLogging.logger {}

    private var initializationCallback: (() -> Unit)? = null

    private val firestoreDB = FirebaseFirestore.getInstance()
    private val fbAuth = FirebaseAuth.getInstance()
    private var fbUser = fbAuth.currentUser

    private var dataToSet = HashMap<String,Any>()

    init {
        val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build()
        firestoreDB.firestoreSettings = settings
        fbUser = fbAuth.currentUser
    }

    fun initializeUser(){
        if(fbAuth.currentUser==null){
            signInAnonymously()
        }else{
            initializationCallback?.invoke()
        }
    }

    fun getUser(): FirebaseUser? {
        return fbUser
    }

    suspend fun getUserCoroutine(): FirebaseUser? {
        return if(fbUser==null){
            fbAuth.signInAnonymously().await()
            fbUser = fbAuth.currentUser
            addUserToFirestore(fbUser!!)
            fbUser
        }else{
            fbUser
        }
    }

    private fun signInAnonymously(){
        logger.info("signInAnonymously()")
        fbAuth.signInAnonymously().addOnCompleteListener {
            fbUser = it.result!!.user
            runBlocking{
                addUserToFirestore(fbUser!!)
            }
        }
    }

    fun signInWithGoogleCredential(account:GoogleSignInAccount, callback: (exception:Exception?) -> Unit){
        logger.info("signInWithGoogleCredential()")
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)

        // If we use the Anon user this way when the accounts are linked the profile data is not
        // re-populated so that is way we have to do it this way
        dataToSet = HashMap()
        dataToSet["displayName"] = account.displayName.toString()
        dataToSet["email"] = account.email.toString()
        dataToSet["photoUrl"] = account.photoUrl.toString()

        if(fbUser!=null && fbUser!!.isAnonymous){
            fbUser!!.linkWithCredential(credential).addOnCompleteListener {
                if(it.isSuccessful){
                    fbUser = it.result!!.user
                    runBlocking{
                        addUserToFirestore(fbUser!!)
                    }
                    callback(null)
                }else{
                    if(it.exception is FirebaseAuthUserCollisionException){
                        firestoreDB.collection(USERS).document(fbUser!!.uid).delete().addOnSuccessListener {
                            fbUser!!.delete().addOnSuccessListener {
                                fbAuth.signOut()
                                fbUser = null
                                signInWithGoogleCredential(account, callback)
                            }
                        }
                    }else{
                        signInAnonymously()
                        callback(it.exception)
                        logger.info("signInWithGoogleCredential failed it.exception:${it.exception}")
                    }
                }
            }
        }else if(fbUser==null){
            fbAuth.signInWithCredential(credential).addOnCompleteListener {
                if(it.isSuccessful){
                    fbUser = fbAuth.currentUser
                    runBlocking{
                        addUserToFirestore(fbUser!!)
                    }
                    callback(null)
                }else{
                    signInAnonymously()
                    callback(it.exception)
                    logger.info("signInWithGoogleCredential failed it.exception:${it.exception}")
                }
            }
        }
    }

    fun getGoogleSignInClient(context:Context):GoogleSignInClient{
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestId()
                .requestIdToken(context.getString(R.string.default_web_client_id))
                .requestProfile()
                .requestEmail()
                .build()
        return GoogleSignIn.getClient(context, gso)
    }

    private suspend fun addUserToFirestore(user:FirebaseUser){
        logger.info("addUserToFirestore()")
        logger.info("firestoreDB.collection(USERS).document(user.uid).uid:${firestoreDB.collection(USERS).document(user.uid).id}")
        val docData = HashMap<String,Any>()
        docData["anonymous"] = user.isAnonymous
        docData["displayName"] = user.displayName.toString()
        docData["email"] = user.email.toString()
        docData["emailVerified"] = user.isEmailVerified
        if(user.metadata!=null){
            docData["creationTimestamp"] = user.metadata!!.creationTimestamp
            docData["lastSignInTimestamp"] = user.metadata!!.lastSignInTimestamp
        }
        docData["phoneNumber"] = user.phoneNumber.toString()
        docData["photoUrl"] = user.photoUrl.toString()
        docData["providerId"] = user.providerId
        if(dataToSet.size>0){
            for(entry in dataToSet){
                docData[entry.key] = entry.value
            }
        }

        firestoreDB.collection(USERS).document(user.uid).set(docData).addOnSuccessListener {
            initializationCallback?.invoke()
        }
    }

    //---------------- Util code ----------------//

    fun setInitializationCallback(callback:() -> Unit){
        initializationCallback = callback
    }

}