package no.none.runtrainer.data.DAOs

import androidx.lifecycle.LiveData
import androidx.room.*
import no.none.runtrainer.data.DataVOs

@Dao
interface SensorDataDao {
    @Query("SELECT * FROM sensordata")
    fun all(): LiveData<List<DataVOs.SensorData>>

    @Insert
    fun insert(session: DataVOs.SensorData): Long

    @Update
    fun update(session: DataVOs.SensorData)

    @Delete
    fun delete(session: DataVOs.SensorData)

    @Query("SELECT * FROM sensordata WHERE idSensorData IS :sessionId LIMIT 1")
    fun getById(sessionId:Int):DataVOs.SensorData

    @Query("SELECT * FROM sensordata WHERE idSensorData IS :sessionId LIMIT 1")
    fun getByIdLiveData(sessionId:Int):LiveData<DataVOs.SensorData?>

    @Query("SELECT * FROM sensordata WHERE idSession IS :sessionId LIMIT 1")
    fun getBySessionId(sessionId:Int):DataVOs.SensorData

    @Query("SELECT * FROM sensordata WHERE idSession IS :sessionId LIMIT 1")
    fun getBySessionIdLiveData(sessionId:Int):LiveData<DataVOs.SensorData?>
}