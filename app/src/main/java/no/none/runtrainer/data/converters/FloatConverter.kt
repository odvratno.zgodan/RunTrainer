package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class FloatConverter {
    companion object {
        private val gs= Gson()
        @JvmStatic
        @TypeConverter
        fun toString(floatReadings: ArrayList<Float>?): String? {
            return if (floatReadings != null) {
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, FloatConverter().DataGsonSerializer())
                        .create()
                gs.toJson(floatReadings)
            }else{
                ""
            }
        }

        @JvmStatic
        @TypeConverter
        fun toHeartRateReadings(value:String?): ArrayList<Float>? {
            return if(value != null && value.isNotEmpty()){
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, FloatConverter().DataGsonDeserializer())
                        .create()
                val itemType = object : TypeToken<ArrayList<Float>>() {}.type
                gs.fromJson<ArrayList<Float>>(value, itemType)
            }else{
                null
            }
        }
    }

    inner class DataGsonSerializer: JsonSerializer<ArrayList<Float>> {
        private val gs= Gson()

        override fun serialize(src: ArrayList<Float>, typeOfSrc: Type, context: JsonSerializationContext): JsonArray? {
            val jsonLocationsArray = JsonArray()
            for (i in 0 until src.size){
                val jsonLocation = JsonPrimitive(gs.toJson(src[i]))
                jsonLocationsArray.add(jsonLocation)
            }
            return jsonLocationsArray
        }
    }

    inner class DataGsonDeserializer: JsonDeserializer<ArrayList<Float>> {
        private val gs = Gson()

        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ArrayList<Float> {
            val jsonGPSArray = json.asJsonArray

            val itemType = object : TypeToken<Float>() {}.type
            val list: ArrayList<Float> = ArrayList(1)
            for (i in 0 until jsonGPSArray.size()) {
                list.add(gs.fromJson<Float>(jsonGPSArray[i].asString, itemType))
            }
            return list
        }
    }
}