package no.none.runtrainer.data

import android.content.Context
import android.os.FileObserver.CREATE
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.RoomMasterTable.TABLE_NAME
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import no.none.runtrainer.data.DAOs.SensorDataDao
import no.none.runtrainer.data.DAOs.SessionDao
import no.none.runtrainer.data.converters.*


@Database(entities = [(DataVOs.Session::class), (DataVOs.SensorData::class)], version = 8)
@TypeConverters(DateConverter::class, LocationConverter::class, HeartRateConverter::class, SessionRecordingProgressConverter::class, ProcessorNotificationConverter::class, FloatConverter::class, LongConverter::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getDataBase(context: Context): AppDatabase {
            if (INSTANCE == null) {

                INSTANCE = Room.databaseBuilder<AppDatabase>(context.applicationContext, AppDatabase::class.java, "app-db")
                        .addMigrations(MIGRATION_4_5)
                        .addMigrations(MIGRATION_5_6)
                        .addMigrations(MIGRATION_6_7)
                        .addMigrations(MIGRATION_7_8)
//                        .fallbackToDestructiveMigration()
                        .build()
            }
            return INSTANCE as AppDatabase
        }

        fun destroyInstance() {
            if (INSTANCE!!.isOpen) INSTANCE!!.close()
            INSTANCE = null
        }

        private val MIGRATION_4_5: Migration = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE sessions ADD COLUMN sessionRecordingProgress TEXT NOT NULL default '{\"distance\":0.0,\"duration\":0,\"segment\":0}'")

                database.execSQL("DROP INDEX IF EXISTS 'index_sessions_idSession'")
                database.execSQL("CREATE INDEX index_sessions_idSession ON  sessions(idSession)")
                database.execSQL("DROP INDEX IF EXISTS 'index_sessions_programUid'")
                database.execSQL("CREATE INDEX index_sessions_programUid ON  sessions(programUid)")
                database.execSQL("DROP INDEX IF EXISTS 'index_sensordata_idSensorData'")
                database.execSQL("CREATE INDEX index_sensordata_idSensorData ON  sensordata(idSensorData)")
                database.execSQL("DROP INDEX IF EXISTS 'index_sensordata_idSession'")
                database.execSQL("CREATE INDEX index_sensordata_idSession ON  sensordata(idSession)")
            }
        }
        private val MIGRATION_5_6: Migration = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // I fucked up!
            }
        }
        private val MIGRATION_6_7: Migration = object : Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // I fucked up!
            }
        }
        private val MIGRATION_7_8: Migration = object : Migration(7, 8) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE sensordata ADD COLUMN distances TEXT NOT NULL default '[]'")
                database.execSQL("ALTER TABLE sensordata ADD COLUMN durations TEXT NOT NULL default '[]'")
            }
        }
    }

    abstract fun sessionDao(): SessionDao
    abstract fun sensorDataDao(): SensorDataDao
}