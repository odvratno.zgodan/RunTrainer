package no.none.runtrainer.data.viewmodels

import androidx.lifecycle.LiveData
import android.os.AsyncTask
import no.none.runtrainer.data.AppDatabase
import no.none.runtrainer.data.DataVOs

interface ISessionModel {

    val appDb: AppDatabase
    var sessionsLiveData: LiveData<List<DataVOs.Session>>

    fun getSessions(): LiveData<List<DataVOs.Session>> {
        return sessionsLiveData
    }

    fun getSessionBeignRecorded(): LiveData<DataVOs.Session?> {
        return appDb.sessionDao().currentlyBeingRecordedLiveData()
    }

    fun getSessionByIdLiveData(sessionId:Int): LiveData<DataVOs.Session?> {
        return appDb.sessionDao().getByIdLiveData(sessionId)
    }

    fun addSession(session: DataVOs.Session){
        ISessionModel.InsertAsynTask(appDb).execute(session)
    }

    fun addSession(session: DataVOs.Session, callback: (Long?) -> Unit){
        ISessionModel.InsertAsynTaskWithCallback(appDb, callback).execute(session)
    }

    fun updateSession(session: DataVOs.Session){
        ISessionModel.UpdateAsynTask(appDb).execute(session)
    }

    fun updateSession(session: DataVOs.Session, callback: () -> Unit){
        ISessionModel.UpdateAsynTaskWithCallback(appDb, callback).execute(session)
    }

    fun deleteSession(session: DataVOs.Session){
        ISessionModel.DeleteAsynTask(appDb).execute(session)
    }

    fun currentlyBeingRecordedSession(callback: (DataVOs.Session?) -> Unit){
        ISessionModel.CurrentlyBeingRecordedAsynTask(appDb, callback).execute()
    }

    fun getSessionById(sessionId:Int, callback: (DataVOs.Session?) -> Unit){
        ISessionModel.GetByIdAsynTask(appDb, callback).execute(sessionId)
    }

    class InsertAsynTask(private var db: AppDatabase) : AsyncTask<DataVOs.Session, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.Session): Void? {
            db.sessionDao().insert(params[0])
            return null
        }
    }

    class InsertAsynTaskWithCallback(private var db: AppDatabase, private val callback: (Long?) -> Unit) : AsyncTask<DataVOs.Session, Void, Long?>() {
        override fun doInBackground(vararg params: DataVOs.Session): Long? {
            db.sessionDao().insert(params[0])
            return null
        }

        override fun onPostExecute(result: Long?) {
            super.onPostExecute(result)
            if(result==null)
            {
                callback(null)
            }
            else
            {
                callback(result)
            }
        }

    }

    class UpdateAsynTask(private var db: AppDatabase) : AsyncTask<DataVOs.Session, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.Session): Void? {
            db.sessionDao().update(params[0])
            return null
        }
    }

    class UpdateAsynTaskWithCallback(private var db: AppDatabase, private val callback: () -> Unit) : AsyncTask<DataVOs.Session, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.Session): Void? {
            db.sessionDao().update(params[0])
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            //callback function to be executed after getting the result
            callback()
        }
    }


    class DeleteAsynTask(private var db: AppDatabase) : AsyncTask<DataVOs.Session, Void, Void>() {
        override fun doInBackground(vararg params: DataVOs.Session): Void? {
            db.sessionDao().delete(params[0])
            return null
        }
    }

    class CurrentlyBeingRecordedAsynTask(private var db: AppDatabase, private val callback: (DataVOs.Session?) -> Unit) : AsyncTask<Void, Void, DataVOs.Session?>() {
        override fun doInBackground(vararg params: Void): DataVOs.Session? {
            return db.sessionDao().currentlyBeingRecorded()
        }

        override fun onPostExecute(result: DataVOs.Session?) {
            super.onPostExecute(result)
            if(result==null)
            {
                callback(null)
            }
            else
            {
                callback(result)
            }
        }
    }

    class GetByIdAsynTask(private var db: AppDatabase, private val callback: (DataVOs.Session?) -> Unit) : AsyncTask<Int, Void, DataVOs.Session?>() {
        override fun doInBackground(vararg params: Int?): DataVOs.Session? {
            return db.sessionDao().getById(params[0]!!)
        }

        override fun onPostExecute(result: DataVOs.Session?) {
            super.onPostExecute(result)
            if(result==null)
            {
                callback(null)
            }
            else
            {
                callback(result)
            }
        }
    }
}