package no.none.runtrainer.data.DAOs

import androidx.lifecycle.LiveData
import androidx.room.*
import no.none.runtrainer.data.DataVOs

@Dao
interface SessionDao {
    @Query("SELECT * FROM sessions")
    fun all(): LiveData<List<DataVOs.Session>>

    @Insert
    fun insert(session: DataVOs.Session): Long

    @Update
    fun update(session: DataVOs.Session)

    @Delete
    fun delete(session: DataVOs.Session)

    @Query("SELECT * FROM sessions WHERE beingRecorded==1 LIMIT 1")
    fun currentlyBeingRecorded():DataVOs.Session

    @Query("SELECT * FROM sessions WHERE beingRecorded==1 LIMIT 1")
    fun currentlyBeingRecordedLiveData():LiveData<DataVOs.Session?>

    @Query("SELECT * FROM sessions WHERE idSession IS :sessionId LIMIT 1")
    fun getById(sessionId:Int):DataVOs.Session

    @Query("SELECT * FROM sessions WHERE idSession IS :sessionId LIMIT 1")
    fun getByIdLiveData(sessionId:Int):LiveData<DataVOs.Session?>
}