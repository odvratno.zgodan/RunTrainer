package no.none.runtrainer.data.firestorelivedata

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*


class QueryLiveData<T>(private val query: Query, private val type: Class<T>) : LiveData<Resource<List<T>>>(), EventListener<QuerySnapshot> {
    private var registration: ListenerRegistration? = null

    override fun onEvent(snapshots: QuerySnapshot?, e: FirebaseFirestoreException?) {
        if (e != null) {
            this.value = Resource(e)
            return
        }
        this.value = Resource(documentToList(snapshots!!))
    }

    override fun onActive() {
        super.onActive()
        registration = query.addSnapshotListener(this)
    }

    override fun onInactive() {
        super.onInactive()
        if (registration != null) {
            registration!!.remove()
            registration = null
        }
    }

    private fun documentToList(snapshots: QuerySnapshot): List<T> {
        val retList = mutableListOf<T>()
        if (snapshots.isEmpty) {
            return retList
        }

        for (document in snapshots.documents) {
            retList.add(document.toObject(type)!!)
        }

        return retList
    }
}