package no.none.runtrainer.data.converters

import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.SystemClock
import androidx.room.TypeConverter
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import com.google.gson.JsonObject




class LocationConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun toString(locations: ArrayList<Location>?): String? {
            return if (locations != null) {
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, LocationConverter().DataGsonSerializer())
                        .create()
                gs.toJson(locations)
            }else{
                ""
            }
        }

        @JvmStatic
        @TypeConverter
        fun toLocation(value:String?): ArrayList<Location>? {
            return if(value != null && !value.isEmpty()){
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, LocationConverter().DataGsonDeserializer())
                        .create()
                val itemType = object : TypeToken<ArrayList<Location>>() {}.type
                gs.fromJson<ArrayList<Location>>(value, itemType)
            }else{
                null
            }
        }
    }

    inner class DataGsonSerializer: JsonSerializer<ArrayList<Location>> {
        private val gs= Gson()

        override fun serialize(src: ArrayList<Location>, typeOfSrc: Type, context: JsonSerializationContext): JsonArray? {
            val jsonLocationsArray = JsonArray()
            for (i in 0..(src.size-1)){
                val jsonObject = JsonObject()
                jsonObject.addProperty("mAltitude", src[i].altitude)
                jsonObject.addProperty("mBearing", src[i].bearing)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    jsonObject.addProperty("mBearingAccuracyDegrees", src[i].bearingAccuracyDegrees)
                    jsonObject.addProperty("mSpeedAccuracyMetersPerSecond", src[i].speedAccuracyMetersPerSecond)
                    jsonObject.addProperty("mVerticalAccuracyMeters", src[i].verticalAccuracyMeters)
                }
                jsonObject.addProperty("mElapsedRealtimeNanos", src[i].elapsedRealtimeNanos)
                jsonObject.addProperty("mHorizontalAccuracyMeters", src[i].accuracy)
                jsonObject.addProperty("mLatitude", src[i].latitude)
                jsonObject.addProperty("mLongitude", src[i].longitude)
                jsonObject.addProperty("mProvider", src[i].provider)
                jsonObject.addProperty("mSpeed", src[i].speed)
                jsonObject.addProperty("mTime", src[i].time)
                jsonLocationsArray.add(jsonObject)
            }
            return jsonLocationsArray
        }
    }

    inner class DataGsonDeserializer: JsonDeserializer<ArrayList<Location>> {
        private val gs = Gson()

        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ArrayList<Location> {
            val jsonGPSArray = json.asJsonArray
            val list: ArrayList<Location> = ArrayList(1)
            for (i in 0..(jsonGPSArray.size() - 1)) {
                val jsonObject = jsonGPSArray[i].asJsonObject
                val location = Location(jsonObject.get("mProvider").asString)
                location.latitude = jsonObject.get("mLatitude").asDouble
                location.longitude = jsonObject.get("mLongitude").asDouble
                location.altitude = jsonObject.get("mAltitude").asDouble
                location.time = jsonObject.get("mTime").asLong
                location.accuracy = jsonObject.get("mHorizontalAccuracyMeters").asFloat
                location.elapsedRealtimeNanos = jsonObject.get("mElapsedRealtimeNanos").asLong
                location.speed = jsonObject.get("mSpeed").asFloat
                location.bearing = jsonObject.get("mBearing").asFloat
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    location.bearingAccuracyDegrees = jsonObject.get("mBearingAccuracyDegrees").asFloat
                    location.speedAccuracyMetersPerSecond = jsonObject.get("mSpeedAccuracyMetersPerSecond").asFloat
                    location.verticalAccuracyMeters = jsonObject.get("mVerticalAccuracyMeters").asFloat
                }
                list.add(location)
            }
            return list
        }
    }
}