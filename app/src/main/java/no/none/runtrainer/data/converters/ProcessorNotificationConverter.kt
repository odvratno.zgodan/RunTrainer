package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import no.none.runtrainer.data.DataVOs
import java.lang.reflect.Type

class ProcessorNotificationConverter {
    companion object {
        private val gs= Gson()
        @JvmStatic
        @TypeConverter
        fun toString(processorNotifications: ArrayList<DataVOs.ProcessorNotification>?): String? {
            return if (processorNotifications != null) {
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, ProcessorNotificationConverter().DataGsonSerializer())
                        .create()
                gs.toJson(processorNotifications)
            }else{
                ""
            }
        }

        @JvmStatic
        @TypeConverter
        fun toProcessorNotifications(value:String?): ArrayList<DataVOs.ProcessorNotification>? {
            return if(value != null && !value.isEmpty()){
                val gs = GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("yyyy-MM-dd")
                        .registerTypeAdapter(ArrayList::class.java, ProcessorNotificationConverter().DataGsonDeserializer())
                        .create()
                val itemType = object : TypeToken<ArrayList<DataVOs.ProcessorNotification>>() {}.type
                gs.fromJson<ArrayList<DataVOs.ProcessorNotification>>(value, itemType)
            }else{
                ArrayList<DataVOs.ProcessorNotification>()
            }
        }
    }

    inner class DataGsonSerializer: JsonSerializer<ArrayList<DataVOs.ProcessorNotification>> {
        private val gs= Gson()

        override fun serialize(src: ArrayList<DataVOs.ProcessorNotification>, typeOfSrc: Type, context: JsonSerializationContext): JsonArray? {
            val jsonLocationsArray = JsonArray()
            for (i in 0..(src.size-1)){
                val jsonLocation = JsonPrimitive(gs.toJson(src[i]))
                jsonLocationsArray.add(jsonLocation)
            }
            return jsonLocationsArray
        }
    }

    inner class DataGsonDeserializer: JsonDeserializer<ArrayList<DataVOs.ProcessorNotification>> {
        private val gs = Gson()

        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ArrayList<DataVOs.ProcessorNotification> {
            val jsonGPSArray = json.asJsonArray

            val itemType = object : TypeToken<DataVOs.ProcessorNotification>() {}.type
            val list: ArrayList<DataVOs.ProcessorNotification> = ArrayList(1)
            for (i in 0..(jsonGPSArray.size() - 1)) {
                list.add(gs.fromJson<DataVOs.ProcessorNotification>(jsonGPSArray[i].asString, itemType))
            }
            return list
        }
    }
}