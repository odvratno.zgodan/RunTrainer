package no.none.runtrainer.data.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.Query
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.firestorelivedata.QueryLiveData
import no.none.runtrainer.data.firestorelivedata.Resource

class ProgramViewModel(application: Application): AndroidViewModel(application), IProgramModel {

    companion object {
        private const val USERS = UserModel.USERS
        private const val PROGRAMS = "programs"
    }

    override val userModel:UserModel = UserModel()
    override val firestoreDB:FirebaseFirestore = FirebaseFirestore.getInstance()
    override val programsLiveData: LiveData<Resource<List<DataVOs.Program>>>
    override val query:Query

    init {
        val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build()
        firestoreDB.firestoreSettings = settings

        query = firestoreDB.collection(USERS).document(userModel.getUser()!!.uid).collection(PROGRAMS)
        programsLiveData = QueryLiveData(query, DataVOs.Program::class.java)
    }
}