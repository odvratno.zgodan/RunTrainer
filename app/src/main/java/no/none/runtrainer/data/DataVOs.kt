package no.none.runtrainer.data

import android.location.Location
import android.os.Parcelable
import androidx.room.*
import com.fonfon.geohash.GeoHash
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize
import no.none.runtrainer.toMapLatLng
import no.none.runtrainer.data.converters.*
import no.none.runtrainer.formatMperSecToMMSSString
import java.util.*
import kotlin.collections.ArrayList


class DataVOs {

    var sessions = ArrayList<Session>();

    enum class StepType(val type: Int){
        ONE_STEP(0),
        TWO_STEP(1),
        THREE_STEP(2),
        NO_STEP(3)
    }

    enum class SegmentType(val type: Int){
        DISTANCE(0),
        DURATION(1),
        POWER(2),
        GROUP(3)
    }


    /**
     * Used for tracking the start of the current segment in the IRecordingProcessor and ISegmentProcessor
     * */
    @Parcelize
    data class SessionRecordingProgress(var segment:Int=0, var distance:Float=0.0f, var duration: Long=0L):Parcelable

    @Parcelize
    data class Segment(
            var uid:String="",
            var title:String="",
            var distance:Float=0.0f,
            var duration:Long=0L,
            var power:Float=0.0f,
            var speedMillisecondsPerKm:Long=0L,
            var bpm:Int=90,
            val segmentType:SegmentType=SegmentType.DISTANCE,
            var stepType:StepType=StepType.ONE_STEP,
            var parentSegmentUid:String = "",
            var groupOrder:Int = -1
    ):Parcelable{
        constructor(segmentType:SegmentType):this(UUID.randomUUID().toString(), "", 0.0f, 0L, 0.0f, 0, 90, segmentType, StepType.ONE_STEP)

        fun getSpeed():Float{
            return ((60.0f/((speedMillisecondsPerKm/1000).toFloat()/60.0f))*0.2777778f)
        }
    }

    @Parcelize
    @Entity(tableName = "programs")
    @JvmSuppressWildcards
    data class Program(
            @PrimaryKey(autoGenerate = true)
            @ColumnInfo(name = "programUid")
            var uid:String = "",

            @ColumnInfo(name = "name")
            var name:String,

            @ColumnInfo(name = "loop")
            var loop:Boolean,

            @ColumnInfo(name = "segments")
            @TypeConverters(SegmentConverter::class)
            var segments:ArrayList<Segment>
    ):Parcelable
    {
        @Ignore
        constructor():this("", "", false, ArrayList<Segment>())
    }

    @Parcelize
    data class HeartRateReading(var hr:Int, var timestamp:Long):Parcelable

    @Parcelize
    data class ProcessorNotification(var speed:Float, var timestamp:Long, var distance:Float, var gpsPointIndex:Int, var notified:Boolean):Parcelable{
        override fun toString(): String = "speed:${speed.formatMperSecToMMSSString()}, timestamp:$timestamp, distance:$distance, gpsPointIndex:$gpsPointIndex, notified:$notified"
    }

    @Parcelize
    @Entity(tableName = "sensordata",
            indices = [Index("idSensorData"), Index("idSession")]
    )
    @JvmSuppressWildcards
    data class SensorData(
            @PrimaryKey(autoGenerate = true)
            @ColumnInfo(name = "idSensorData")
            var id:Int? = null,

            @ColumnInfo(name = "idSession")
            var idSession:Int,

            @TypeConverters(LocationConverter::class)
            var gps:ArrayList<Location>,

            @TypeConverters(LocationConverter::class)
            var gpsKalmanized:ArrayList<Location>,

            @TypeConverters(HeartRateConverter::class)
            var heartrate:ArrayList<HeartRateReading>,

            @ColumnInfo(name = "processorsNotifications")
            @TypeConverters(ProcessorNotificationConverter::class)
            var processorsNotifications:ArrayList<ProcessorNotification>,

            @ColumnInfo(name = "distances")
            @TypeConverters(FloatConverter::class)
            var distances:ArrayList<Float>,

            @ColumnInfo(name = "durations")
            @TypeConverters(LongConverter::class)
            var durations:ArrayList<Long>

    ):Parcelable{

        constructor(idSession:Int=-1):this(null, idSession, ArrayList<Location>(), ArrayList<Location>(), ArrayList<HeartRateReading>(), ArrayList<ProcessorNotification>(), ArrayList<Float>(), ArrayList<Long>())

        fun getGpsLatLng():ArrayList<LatLng>{
            return gps.toMapLatLng()
        }

        fun deepCopy(id: Int? = this.id,
                 idSession: Int = this.idSession,
                 gps:ArrayList<Location> = ArrayList(this.gps.toList()),
                 gpsKalmanized:ArrayList<Location> = ArrayList(this.gpsKalmanized.toList()),
                 heartrate:ArrayList<HeartRateReading> = ArrayList(this.heartrate.toList()),
                 processorsNotifications:ArrayList<ProcessorNotification> = ArrayList(this.processorsNotifications.toList()),
                 distances:ArrayList<Float> = ArrayList(this.distances.toList()),
                 durations:ArrayList<Long> = ArrayList(this.durations.toList())
        ) = SensorData(id, idSession, gps, gpsKalmanized, heartrate, processorsNotifications, distances, durations)

        companion object {

            fun filterLocationWithAccuracy(locations: ArrayList<Location>, precision:Float): ArrayList<Location> {
                val list:ArrayList<Location> = ArrayList(1)
                if(locations.size>0){
                    for (i in 0 until locations.size){
                        if(locations[i].accuracy<=precision){
                            list.add(locations[i])
                        }
                    }
                }
                return list
            }

            fun filterLocationWithGeohash(locations: ArrayList<Location>, precision:Int): ArrayList<Location> {
                val list:ArrayList<Location> = ArrayList(1)
                if(locations.size>0){
                    for (i in 0 until locations.size){
                        if(list.size==0 || GeoHash.fromLocation(locations[i], precision)!=GeoHash.fromLocation(list.last(), precision)){
                            list.add(locations[i])
                        }
                    }
                }
                return list
            }
        }
    }

    @Parcelize
    @Entity(tableName = "sessions",
            indices = [Index("idSession"), Index("programUid")]
    )
    @JvmSuppressWildcards
    data class Session(
            @PrimaryKey(autoGenerate = true)
            @ColumnInfo(name = "idSession")
            var id:Int? = null,
            var title:String,
            var duration:Long = 0L,
            var distance:Float=0.0f,
            var beingRecorded:Boolean,
            @TypeConverters(DateConverter::class)
            var start:Date? = null,
            @TypeConverters(DateConverter::class)
            var end:Date? = null,
            @ColumnInfo(name = "programUid")
            var programUid:String,
            @ColumnInfo(name = "sessionRecordingProgress")
            @TypeConverters(SessionRecordingProgressConverter::class)
            var sessionRecordingProgress:SessionRecordingProgress = SessionRecordingProgress()
    ):Parcelable
    {
        @Ignore
        constructor(programUid:String):this(null, "", 0, 0.0f, false, null, null, programUid, SessionRecordingProgress())
    }
}