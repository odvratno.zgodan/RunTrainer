package no.none.runtrainer.data.viewmodels

import android.app.Application
import androidx.lifecycle.LiveData
import android.os.AsyncTask
import no.none.runtrainer.data.AppDatabase
import no.none.runtrainer.data.DataVOs

class SessionModel(application: Application):ISessionModel {

    override val appDb: AppDatabase = AppDatabase.getDataBase(application)
    override var sessionsLiveData: LiveData<List<DataVOs.Session>> = appDb.sessionDao().all()



}