package no.none.runtrainer.data.viewmodels

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.firestorelivedata.Resource
import java.util.*

interface IProgramModel {

    companion object {
        private const val USERS = UserModel.USERS
        private const val PROGRAMS = "programs"
    }

    val userModel:UserModel
    val firestoreDB: FirebaseFirestore
    val programsLiveData: LiveData<Resource<List<DataVOs.Program>>>
    val query:Query

    fun getPrograms(): LiveData<Resource<List<DataVOs.Program>>> {
        return programsLiveData
    }

    fun setProgram(program: DataVOs.Program){
        if(program.uid==""){
            program.uid = UUID.randomUUID().toString()
        }
        firestoreDB.collection(USERS).document(userModel.getUser()!!.uid).collection(PROGRAMS).document(program.uid).set(program)
    }

    fun deleteProgram(program: DataVOs.Program){
        firestoreDB.collection(USERS).document(userModel.getUser()!!.uid).collection(PROGRAMS).document(program.uid).delete()
    }

    fun getProgramById(programUid: String, callback: (DataVOs.Program?) -> Unit){
        firestoreDB.collection(USERS).document(userModel.getUser()!!.uid).collection(PROGRAMS).document(programUid).get().addOnSuccessListener {
            if(it.exists()){
                callback(it.toObject(DataVOs.Program::class.java))
            }else{
                callback(null)
            }
        }
    }
}