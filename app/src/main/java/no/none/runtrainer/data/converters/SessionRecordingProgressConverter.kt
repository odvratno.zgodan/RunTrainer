package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import no.none.runtrainer.data.DataVOs
import java.util.*


class SessionRecordingProgressConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun toString(sessionRecordingProgress: DataVOs.SessionRecordingProgress): String {
            val gs=Gson()
            return gs.toJson(sessionRecordingProgress)
        }

        @JvmStatic
        @TypeConverter
        fun toSessionRecordingProgress(string:String): DataVOs.SessionRecordingProgress {
            val gson = Gson()
            val itemType = object : TypeToken<DataVOs.SessionRecordingProgress>() {}.type
            return gson.fromJson<DataVOs.SessionRecordingProgress>(string, itemType)
        }
    }
}