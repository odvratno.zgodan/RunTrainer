package no.none.runtrainer.data.converters

import androidx.room.TypeConverter
import android.location.Location
import com.google.gson.*
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import no.none.runtrainer.data.DataVOs
import java.lang.reflect.Type

class SensorDataConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun toString(sensorData: DataVOs.SensorData?): String {
            val gs = GsonBuilder()
                    .setPrettyPrinting()
                    .setDateFormat("yyyy-MM-dd")
                    .registerTypeAdapter(DataVOs.SensorData::class.java, SensorDataConverter().DataGsonSerializer())
                    .create()
            return gs.toJson(sensorData)
        }

        @JvmStatic
        @TypeConverter
        fun toSensorData(string:String): DataVOs.SensorData? {
            val gs = GsonBuilder()
                    .setPrettyPrinting()
                    .setDateFormat("yyyy-MM-dd")
                    .registerTypeAdapter(DataVOs.SensorData::class.java, SensorDataConverter().DataGsonDeserializer())
                    .create()
            val itemType = object : TypeToken<DataVOs.SensorData>() {}.type
            return gs.fromJson<DataVOs.SensorData>(string, itemType)
        }
    }

    inner class DataGsonSerializer: JsonSerializer<DataVOs.SensorData> {
        private val gs= Gson()

        override fun serialize(src: DataVOs.SensorData, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
            var sensorDataJsonObj = JsonObject()
            sensorDataJsonObj.add("gps", convertLocationArrayList(src.gps))
            sensorDataJsonObj.add("gpsKalmanized", convertLocationArrayList(src.gpsKalmanized))
            sensorDataJsonObj.add("heartrate", convertHeartRateArrayList(src.heartrate))
            sensorDataJsonObj.add("processorNotification", convertProcessorNotificationArrayList(src.processorsNotifications))
            sensorDataJsonObj.add("durations", convertDurationArrayList(src.durations))
            sensorDataJsonObj.add("distances", convertDistancesArrayList(src.distances))

            return sensorDataJsonObj
        }

        private fun convertLocationArrayList(src:ArrayList<Location>): JsonArray? {
            val jsonLocationsArray = JsonArray()
            for (i in 0 until src.size){
                val jsonLocation = JsonPrimitive(gs.toJson(src[i]))
                jsonLocationsArray.add(jsonLocation)
            }
            return jsonLocationsArray
        }

        private fun convertHeartRateArrayList(src:ArrayList<DataVOs.HeartRateReading>): JsonArray? {
            val jsonLocationsArray = JsonArray()
            for (i in 0 until src.size){
                val jsonHeartRate = JsonPrimitive(gs.toJson(src[i]))
                jsonLocationsArray.add(jsonHeartRate)
            }
            return jsonLocationsArray
        }

        private fun convertProcessorNotificationArrayList(src:ArrayList<DataVOs.ProcessorNotification>): JsonArray? {
            val jsonProcessorNotificationArray = JsonArray()
            for (i in 0 until src.size){
                val jsonProcessorNotification = JsonPrimitive(gs.toJson(src[i]))
                jsonProcessorNotificationArray.add(jsonProcessorNotification)
            }
            return jsonProcessorNotificationArray
        }
        private fun convertDistancesArrayList(src:ArrayList<Float>): JsonArray? {
            val jsonArray = JsonArray()
            for (i in 0 until src.size){
                val json = JsonPrimitive(gs.toJson(src[i]))
                jsonArray.add(json)
            }
            return jsonArray
        }
        private fun convertDurationArrayList(src:ArrayList<Long>): JsonArray? {
            val jsonArray = JsonArray()
            for (i in 0 until src.size){
                val json = JsonPrimitive(gs.toJson(src[i]))
                jsonArray.add(json)
            }
            return jsonArray
        }
    }
    inner class DataGsonDeserializer: JsonDeserializer<DataVOs.SensorData> {
        private val gs= Gson()

        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): DataVOs.SensorData {
            val  jsonObject = json.asJsonObject
            val jsonGPSArray = jsonObject.get("gps").asJsonArray
            val jsonGPSKalmanizedArray = jsonObject.get("gpsKalmanized").asJsonArray
            val jsonHeartRateArray = jsonObject.get("heartrate").asJsonArray
            val jsonProcessorNotificationArray = jsonObject.get("processorNotification").asJsonArray
            val jsonDistancesArray = jsonObject.get("distances").asJsonArray
            val jsonDurationsArray = jsonObject.get("durations").asJsonArray
            val gps = convertLocationArrayList(jsonGPSArray)
            val gpsKlamanized = convertLocationArrayList(jsonGPSKalmanizedArray)
            val heartrate = convertHeartRateArrayList(jsonHeartRateArray)
            val processorNotification = convertProcessorNotificationArrayList(jsonProcessorNotificationArray)
            val distances = convertDistancesArrayList(jsonDistancesArray)
            val durations = convertDurationsArrayList(jsonDurationsArray)

            return DataVOs.SensorData(null, -1, gps, gpsKlamanized, heartrate, processorNotification, distances, durations)
        }

        private fun convertLocationArrayList(src:JsonArray): ArrayList<Location> {
            val itemType = object : TypeToken<Location>() {}.type
            val list:ArrayList<Location> = ArrayList(1)
            for (i in 0 until src.size()){
                list.add(gs.fromJson<Location>(src[i].asString, itemType))
            }
            return list
        }

        private fun convertHeartRateArrayList(src:JsonArray): ArrayList<DataVOs.HeartRateReading> {
            val itemType = object : TypeToken<DataVOs.HeartRateReading>() {}.type
            val list:ArrayList<DataVOs.HeartRateReading> = ArrayList(1)
            for (i in 0 until src.size()){
                list.add(gs.fromJson<DataVOs.HeartRateReading>(src[i].asString, itemType))
            }
            return list
        }

        private fun convertProcessorNotificationArrayList(src:JsonArray): ArrayList<DataVOs.ProcessorNotification> {
            val itemType = object : TypeToken<DataVOs.ProcessorNotification>() {}.type
            val list:ArrayList<DataVOs.ProcessorNotification> = ArrayList(1)
            for (i in 0 until src.size()){
                list.add(gs.fromJson<DataVOs.ProcessorNotification>(src[i].asString, itemType))
            }
            return list
        }

        private fun convertDistancesArrayList(src:JsonArray): ArrayList<Float> {
            val itemType = object : TypeToken<Float>() {}.type
            val list:ArrayList<Float> = ArrayList(1)
            for (i in 0 until src.size()){
                list.add(gs.fromJson<Float>(src[i].asString, itemType))
            }
            return list
        }

        private fun convertDurationsArrayList(src:JsonArray): ArrayList<Long> {
            val itemType = object : TypeToken<Long>() {}.type
            val list:ArrayList<Long> = ArrayList(1)
            for (i in 0 until src.size()){
                list.add(gs.fromJson<Long>(src[i].asString, itemType))
            }
            return list
        }
    }

}