package no.none.runtrainer

import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import mu.KotlinLogging
import no.none.runtrainer.data.viewmodels.SessionViewModel
import no.none.runtrainer.data.viewmodels.UserModel
import no.none.runtrainer.ui.session.SessionRecordingActivity


class SplashActivity : AppCompatActivity() {

    private val logger = KotlinLogging.logger {}

    private lateinit var sessionViewModel: SessionViewModel

    private var userModel = UserModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sessionViewModel = ViewModelProviders.of(this).get(SessionViewModel::class.java)
        userModel.setInitializationCallback {
            sessionViewModel.currentlyBeingRecordedSession {
                session ->
                val intent = if(session==null)
                {
                    Intent(this, MainActivity::class.java)
                }
                else
                {
                    SessionRecordingActivity.resumeIntent(this, session.id!!)
                }
                startActivity(intent)
            }
        }
        userModel.initializeUser()
    }
}
