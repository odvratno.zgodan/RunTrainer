package no.none.runtrainer.services.processors.subprocessors

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import com.crashlytics.android.Crashlytics
import mu.KotlinLogging
import no.none.runtrainer.R
import no.none.runtrainer.audioPlayBPM
import no.none.runtrainer.audioPlayText
import no.none.runtrainer.audioStopBPM
import no.none.runtrainer.data.DataVOs

abstract class AbstractSegmentProcessor(protected var context:Context, protected var session:DataVOs.Session, protected var segment: DataVOs.Segment, protected var sensorData: DataVOs.SensorData, protected var onlyProcess:Boolean=false):ISegmentProcessor {

    constructor(context:Context, session:DataVOs.Session, segment: DataVOs.Segment, sensorData: DataVOs.SensorData):this(context, session, segment, sensorData, false)

    protected val logger = KotlinLogging.logger {}

    private var sharedPref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private var sharedPrefListener: PreferencesChangedLisntener = PreferencesChangedLisntener()

    protected var voiceOverEnabled:Boolean = true
    protected var voiceOverInterval:Long = 15000
    protected var speedToleranceDuration:Long = 15000
    protected val movingAverageDuration:Long = 30000
    protected var playBpmEnabled:Boolean = true

    init {
        voiceOverEnabled = sharedPref.getBoolean(context.getString(R.string.pref_key_voice_over), true)
        voiceOverInterval = sharedPref.getString(context.getString(R.string.pref_key_voice_over_interval), "15")!!.toLong()*1000
        speedToleranceDuration = sharedPref.getString(context.getString(R.string.pref_key_speed_tolerance), "15")!!.toLong()*1000
        playBpmEnabled = sharedPref.getBoolean(context.getString(R.string.pref_key_play_bpm), true)
        sharedPref.registerOnSharedPreferenceChangeListener(sharedPrefListener)
    }

    //---------------------- shared preferences code ----------------------//

    protected abstract fun voiceOverEnabledChanged(value:Boolean)
    protected abstract fun voiceOverIntervalChanged(value:Long)
    protected abstract fun speedToleranceDurationChanged(value:Long)
    protected abstract fun playBPMChanged(value:Boolean)

    inner class PreferencesChangedLisntener:SharedPreferences.OnSharedPreferenceChangeListener{
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            if (key == context.getString(R.string.pref_key_voice_over)) {
                voiceOverEnabled = sharedPreferences!!.getBoolean(key, true)
                log("voiceOverEnabled:$voiceOverEnabled")
                voiceOverEnabledChanged(voiceOverEnabled)
            }
            else if (key == context.getString(R.string.pref_key_voice_over_interval)) {
                voiceOverInterval = sharedPreferences!!.getString(key, "15")!!.toLong()*1000
                log("voiceOverInterval:$voiceOverInterval")
                voiceOverIntervalChanged(voiceOverInterval)
            }
            else if (key == context.getString(R.string.pref_key_speed_tolerance)) {
                speedToleranceDuration = sharedPreferences!!.getString(key, "15")!!.toLong()*1000
                log("speedToleranceDuration:$speedToleranceDuration")
                speedToleranceDurationChanged(speedToleranceDuration)
            }
            else if (key == context.getString(R.string.pref_key_play_bpm)) {
                val newValue = sharedPreferences!!.getBoolean(key, true)
                playBPMChanged(newValue)
                log("playBpmEnabled:$playBpmEnabled")
            }
        }
    }


    //---------------------- util code ----------------------//

    protected fun audioPlayText(str:String){
        if(!onlyProcess)
        {
            context.audioPlayText(str)
        }
    }

    protected fun audioPlayBPM(stepType: DataVOs.StepType, bpm:Int){
        if(!onlyProcess)
        {
            context.audioPlayBPM(stepType, bpm)
        }
    }

    protected fun audioStopBPM(){
        if(!onlyProcess)
        {
            context.audioStopBPM()
        }
    }

    protected fun addProcessorNotification(average:Float, previousTimestamp:Long, previousDistance:Float, gpsPointIndex:Int, notified:Boolean){
        listener?.invoke(average, previousTimestamp, previousDistance, gpsPointIndex, notified)
    }

    protected fun log(msg: () -> Any?) {
        if (!onlyProcess){
            Crashlytics.log(Log.DEBUG, "AbstractSegmentProcessor", msg.toString())
            logger.info(msg)
        }
    }
    protected fun log(str:String){
        if (!onlyProcess) {
            Crashlytics.log(str)
            Crashlytics.log(Log.DEBUG, "AbstractSegmentProcessor", str)
            logger.info(str)
        }
    }

    var listener: ((average:Float, previousTimestamp:Long, previousDistance:Float, gpsPointIndex:Int, notified:Boolean) -> Unit)? = null

    fun setOnNotificationListener(l: (average:Float, previousTimestamp:Long, previousDistance:Float, gpsPointIndex:Int, notified:Boolean) -> Unit) {
        listener = l
    }
}