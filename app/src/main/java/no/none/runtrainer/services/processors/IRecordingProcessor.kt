package no.none.runtrainer.services.processors

interface IRecordingProcessor {

    fun start()
    fun stop()

    fun update()

}