package no.none.runtrainer.services.processors.subprocessors

interface ISegmentProcessor {

    fun process()
    fun destroy()

}