package no.none.runtrainer.services.processors.subprocessors

import android.content.Context
import no.none.runtrainer.*
import no.none.runtrainer.data.DataVOs

class DistanceSegmentProcessor(context:Context, session:DataVOs.Session, segment: DataVOs.Segment, sensorData: DataVOs.SensorData, onlyProcess:Boolean):AbstractSegmentProcessor(context, session, segment, sensorData, onlyProcess), ISegmentProcessor {

    constructor(context:Context, session:DataVOs.Session, segment: DataVOs.Segment, sensorData: DataVOs.SensorData):this(context, session, segment, sensorData, false)

    private var previousDistance:Float = Float.NaN
    private var previousTimestamp:Long = 0L

    init {
        if(voiceOverEnabled){
            startSpeechTimer()
        }
        if (playBpmEnabled) {
            audioPlayBPM(segment.stepType, segment.bpm)
        }
        audioPlayText("Started new ${segment.segmentType.name} segment. Target ${segment.distance} kilometers.")
    }

    override fun process() {
        if(previousTimestamp==0L){
            if(sensorData.gps.size>0)
            {
                previousTimestamp = sensorData.gps.last().time
            }
        }
        if(previousDistance.isNaN()){
            previousDistance = session.distance
        }
        checkSpeedAgainstTarget()
    }

    override fun destroy() {
        stopSpeechTimer()
    }

    override fun voiceOverEnabledChanged(value: Boolean) {
        if(voiceOverEnabled){
            startSpeechTimer()
        }else{
            stopSpeechTimer()
        }
    }

    override fun voiceOverIntervalChanged(value: Long) {
        stopSpeechTimer()
        startSpeechTimer()
    }

    override fun speedToleranceDurationChanged(value: Long) {
        //
    }

    override fun playBPMChanged(value: Boolean) {
        if(playBpmEnabled!=value){
            if(value){
                audioPlayBPM(segment.stepType, segment.bpm)
            }else{
                audioStopBPM()
            }
            playBpmEnabled = value
        }
        log("playBpmEnabled:$playBpmEnabled")
    }

    //---------------------- voice-over code ----------------------//

    private var canSpeakOut:Boolean = false
    private var canSpeakTime:Long = 0L

    private fun startSpeechTimer(){
        canSpeakTime = previousTimestamp + voiceOverInterval
    }

    private fun stopSpeechTimer(){
        canSpeakTime = 0L
    }

    private fun checkSpeedAgainstTarget(){
        log("checkSpeedAgainstTarget() "
                + ", gps.size:${sensorData.gps.size}"
                +  ", gpsKalmanized.size:${sensorData.gpsKalmanized.size}"
        )
        if(sensorData.gps.size>0) {
            var now = sensorData.gps.last().time

            if (canSpeakTime != 0L && canSpeakTime <= now) {
                canSpeakOut = true
            }

            var str = ""

            val startIndex = sensorData.gps.indexOfFirst { it.time>=sensorData.gps.last().time-movingAverageDuration }
            val durations = sensorData.durations.subList(startIndex, sensorData.durations.size)
            val distances = sensorData.distances.subList(startIndex, sensorData.distances.size)
            val speeds = distances.zip(durations){ distance, duration -> (distance*1000/(duration/1000)) }
            var average = speeds.average().toFloat()
            if(average.isNaN() || average.isInfinite()){
                average = 0f
            }
            var lastGpsPointIndex = sensorData.gps.size - 1
            var notified = false
            val currentTimestamp = sensorData.gps.last().time
            if (canSpeakOut) {
                previousDistance = session.distance
                previousTimestamp = sensorData.gps.last().time
                lastGpsPointIndex = sensorData.gpsKalmanized.size-1
                if (average.formatMperSecToMMSSFloat()<15) {   // 15 minutes * 60 seconds * 1000 milliseconds
                    if (average < (segment.speedMillisecondsPerKm + speedToleranceDuration).formatDurationToMPerSecFloat()) {
                        str = "Too slow. Current speed ${average.formatMperSecToSpeechMMSSString()}.}"
                    } else if (average > (segment.speedMillisecondsPerKm - speedToleranceDuration).formatDurationToMPerSecFloat()) {
                        str = "Too fast. Current speed ${average.formatMperSecToSpeechMMSSString()}."
                    }
                    if (str != "") {
                        notified = true
                        audioPlayText(str)
                    }
                }
                log(str)
                canSpeakOut = false
                startSpeechTimer()

                log("average pace:${average.formatMperSecToMMSSString()}, pace:${segment.speedMillisecondsPerKm.formatDurationToMMSSString()}" +
                        ", lower bound:${(segment.speedMillisecondsPerKm - speedToleranceDuration).formatDurationToMMSSString()}" +
                        ", upper bound:${(segment.speedMillisecondsPerKm + speedToleranceDuration).formatDurationToMMSSString()}" +
                        "")
            }
            addProcessorNotification(average, currentTimestamp, session.distance, lastGpsPointIndex, notified)
        }
    }

}