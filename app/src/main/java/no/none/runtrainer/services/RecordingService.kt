package no.none.runtrainer.services

import android.app.Activity
import android.app.Notification
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import com.fonfon.geohash.GeoHash
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.ProgramModel
import no.none.runtrainer.data.viewmodels.SensorDataModel
import no.none.runtrainer.data.viewmodels.SessionModel
import no.none.runtrainer.services.handlers.AudioPlaybackHandler
import no.none.runtrainer.services.handlers.MockLocationHandler
import no.none.runtrainer.services.handlers.NotificationHandler
import no.none.runtrainer.services.processors.ProgramProcessor
import org.apache.commons.collections4.multimap.HashSetValuedHashMap
import java.util.*


class RecordingService : BaseLocationService(){

    companion object {
        private const val ONGOING_NOTIFICATION_ID = 13
        const val ACTION_START_SERVICE = "no.none.runtrainer.start_service"
        const val ACTION_STOP_SERVICE = "no.none.runtrainer.stop_service"
        const val ACTION_START_RECORDING = "no.none.runtrainer.start_recording"
        const val ACTION_RESUME_RECORDING = "no.none.runtrainer.resume_recording"
        const val ACTION_STOP_RECORDING = "no.none.runtrainer.stop_recording"
        const val ACTION_BRING_APP_TO_FOREGROUND = "no.none.runtrainer.bring_app_to_foreground"
        const val ACTION_CANCEL_NOTIFICATION = "no.none.runtrainer.cancel_notification"
        const val ACTION_START_MOCK_LOCATION = MockLocationHandler.ACTION_START_MOCK_LOCATION
        const val ACTION_STARTED_MOCK_LOCATION = MockLocationHandler.ACTION_STARTED_MOCK_LOCATION
        const val ACTION_STOP_MOCK_LOCATION = MockLocationHandler.ACTION_STOP_MOCK_LOCATION
        const val ACTION_STOPED_MOCK_LOCATION = MockLocationHandler.ACTION_STOPED_MOCK_LOCATION

        private const val PERMISSION_REQUEST_LOCATION = 99
        private const val INTENT_PROGRAM_UID = "recording_service_program_uid"
        private const val INTENT_SESSION_ID = "recording_service_sssion_id"
        private const val BUNDLE = "no.none.runtrainer.recording_service.bundle"

        const val MOCK_LOCATION_FILE_PATH_EXTRA = MockLocationHandler.MOCK_LOCATION_FILE_PATH_EXTRA

        @JvmStatic
        fun startRecordingIntent(context: Context, programUid: String): Intent {
            return Intent(context, RecordingService::class.java).apply {
                var bundle = Bundle()
                bundle.putString(INTENT_PROGRAM_UID, programUid)
                putExtra(BUNDLE,bundle)
            }
        }
        @JvmStatic
        fun resumeRecordingIntent(context: Context, sessionId: Int): Intent {
            return Intent(context, RecordingService::class.java).apply {
                var bundle = Bundle()
                bundle.putInt(INTENT_SESSION_ID, sessionId)
                putExtra(BUNDLE,bundle)
            }
        }
    }

    private val mBinder = LocalBinder()
    private var activity: Callbacks? = null
    private val actions:HashSetValuedHashMap<String, ActionHandler> = HashSetValuedHashMap<String, ActionHandler>()

    private lateinit var notificationHandler:NotificationHandler
    private lateinit var mockLocationHandler:MockLocationHandler
    private lateinit var audioPlaybackHandler:AudioPlaybackHandler

    private lateinit var programProcessor:ProgramProcessor


    private val programsModel:ProgramModel = ProgramModel()
    private var sessionModel:SessionModel? = null
    private var sensorDataModel:SensorDataModel? = null
    private lateinit var sessionProgram:DataVOs.Program
    private var programUid:String = ""
    private lateinit var session:DataVOs.Session
    private lateinit var sensorData:DataVOs.SensorData
    private var sessionId:Int = -1

    private lateinit var sharedPref:SharedPreferences

    private var voiceOverEnabled:Boolean = true



    init {
        log ("init()")
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    override fun onCreate() {
        super.onCreate()
        log ("onCreate()")
        notificationHandler = NotificationHandler(applicationContext)
        mockLocationHandler = MockLocationHandler(applicationContext)
        audioPlaybackHandler = AudioPlaybackHandler(applicationContext)


        sessionModel = SessionModel(application)
        sensorDataModel = SensorDataModel(application)

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        programProcessor = ProgramProcessor(applicationContext)

        setupActionHandlers()
    }

    private fun setupActionHandlers(){
        actions.put(ACTION_CANCEL_NOTIFICATION, notificationHandler)
        actions.put(ACTION_START_MOCK_LOCATION, mockLocationHandler)
        actions.put(ACTION_STOP_MOCK_LOCATION, mockLocationHandler)
        actions.put(AudioPlaybackHandler.ACTION_PLAY_TRACK, audioPlaybackHandler)
        actions.put(AudioPlaybackHandler.ACTION_PLAY_BPM, audioPlaybackHandler)
        actions.put(AudioPlaybackHandler.ACTION_STOP_BPM, audioPlaybackHandler)
        actions.put(AudioPlaybackHandler.ACTION_TEXT_TO_SPEECH, audioPlaybackHandler)
        actions.put(AudioPlaybackHandler.ACTION_STOP_ALL, audioPlaybackHandler)

        actions.put(ACTION_START_SERVICE, object : ActionHandler {
            override fun execute(intent: Intent) {
                // notification
                setNotification()
            }
        })

        actions.put(ACTION_STOP_SERVICE, object : ActionHandler {
            override fun execute(intent: Intent) {
                // notification
                mockLocationHandler.destroy()
                audioPlaybackHandler.destroy()
                stopService()
            }
        })
        actions.put(ACTION_START_RECORDING, object : ActionHandler {
            override fun execute(intent: Intent) {
                // notification
                setNotification()
                val bundle = intent.getBundleExtra(BUNDLE)
                programUid = bundle?.getString(INTENT_PROGRAM_UID, "")!!
                session = DataVOs.Session(programUid)
                log  { "session.programUid:${session.programUid}" }
                programsModel.getProgramById(session.programUid) { program ->
                    log  { "program:$program" }
                    log  { "program.uid:${program!!.uid}" }
                    sessionProgram = program!!
                    prepareData()
                }
            }
        })
        actions.put(ACTION_RESUME_RECORDING, object : ActionHandler {
            override fun execute(intent: Intent) {
                // notification
                setNotification()
                val bundle = intent.getBundleExtra(BUNDLE)
                sessionId = bundle?.getInt(INTENT_SESSION_ID, -1)!!
                if(sessionId!=-1){
                    sessionModel!!.getSessionById(sessionId){
                        session = it!!
                        programsModel.getProgramById(session.programUid) { program ->
                            log  { "program:$program" }
                            log  { "program.uid:${program!!.uid}" }
                            sessionProgram = program!!
                            prepareData()
                        }
                    }
                }
            }
        })

        actions.put(ACTION_STOP_RECORDING, object : ActionHandler {
            override fun execute(intent: Intent) {
                if(session!=null) {
                    stopRecording()
                }else{
                    log("sessionId:$sessionId, won't be able to stop recording")
                    if (sessionId != -1) {
                        sessionModel!!.getSessionById(sessionId) {
                            session = it!!
                            programsModel.getProgramById(session.programUid) { program ->
                                log  { "program:$program" }
                                log  { "program.uid:${program!!.uid}" }
                                sessionProgram = program!!
                                stopRecording()
                            }
                        }
                    }
                }
            }
        })

        actions.put(ACTION_START_MOCK_LOCATION, object : ActionHandler {
            override fun execute(intent: Intent) {
                setNotification()
            }
        })
        actions.put(ACTION_STARTED_MOCK_LOCATION, object : ActionHandler {
            override fun execute(intent: Intent) {
                activity!!.mockLocationChanged(mockLocationHandler.enabled)
            }
        })
        actions.put(ACTION_STOPED_MOCK_LOCATION, object : ActionHandler {
            override fun execute(intent: Intent) {
                activity!!.mockLocationChanged(mockLocationHandler.enabled)
                sessionModel!!.currentlyBeingRecordedSession {
                    if(it==null){
                        stopService()
                    }
                }
            }
        })
    }

    private fun stopService(){
        stopForeground(true)
        stopSelf()
    }

    private fun handleCommand(intent:Intent) {
        if(intent.action==null) return

        val handlers = actions.get(intent.action)
        for (handler in handlers) {
            if (handler != null) {
                handler.execute(intent)
            } else {
                log ("handler not available")
            }
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            handleCommand(intent)
        }

        return START_STICKY
    }

    private fun setNotification(){
        val notification:Notification =
                notificationHandler!!.buildServiceNotification(
                        "RunTrainer",
                        "Recording in progress",
                        "Recording in progress")

        startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    //---------------------- exposed methods ----------------------//

    //Here Activity register to the service as Callbacks client
    fun registerClient(activity: Activity) {
        this.activity = activity as Callbacks
    }

    fun mockLocationRunning():Boolean{
        return mockLocationHandler.enabled
    }

    private fun prepareData(){
        session.beingRecorded = true
        if(session.id==null)
        {
            session.start = Date()
            session.title = sessionProgram.name + " session"
            sessionModel!!.addSession(session) {
                sessionModel!!.currentlyBeingRecordedSession { session ->
                    this.session = session!!
                    this.sessionId = this.session.id!!
                    this.sensorData = DataVOs.SensorData(this.sessionId)
                    sensorDataModel!!.addSensorData(sensorData){
                        sensorDataModel!!.getSensorDataBySessionId(sessionId){data ->
                            sensorData = data!!
                            println("sensorData.id:${sensorData.id}, sensorData.sessionId:${sensorData.idSession}")
                            startRecording()
                        }
                    }
                }
            }
        }
        else
        {
            sessionModel!!.updateSession(session)
            sensorDataModel!!.getSensorDataBySessionId(session.id!!){
                if(it!=null){
                    sensorData = it
                    println("sensorData.id:${sensorData.id}")
                    startRecording()
                }else{
                    this.sensorData = DataVOs.SensorData(this.sessionId)
                    sensorDataModel!!.addSensorData(sensorData){
                        sensorDataModel!!.getSensorDataBySessionId(sessionId){data ->
                            sensorData = data!!
                            println("sensorData.id:${sensorData.id}, sensorData.sessionId:${sensorData.idSession}")
                            startRecording()
                        }
                    }
                }

            }
        }
    }

    private fun startRecording(){
        buildGoogleApiClient()
        programProcessor.setData(session, sensorData, sessionProgram)
        programProcessor.start()

        if(activity!=null){
            activity!!.recordingStarted()
        }
    }

    private fun stopRecording(){
        stopGoogleApiClient()
        programProcessor.stop()

        log  { "stopRecording() session:$session" }
        session.end = Date()
        session.beingRecorded = false
        sensorData.processorsNotifications = programProcessor.processorsNotifications
        sensorDataModel!!.updateSensorData(sensorData)
        sessionModel!!.updateSession(session) {
            sessionModel!!.currentlyBeingRecordedSession {
                log  { "stopRecording() currentlyBeingRecordedSession:$it" }
            }
            activity!!.updateClient(false)
            activity!!.recordingStopped()

        }
    }

    //---------------------- recording code ----------------------//


    //---------------------- segment analysis code ----------------------//

    private val geohashPrecision = GeoHash.MAX_CHARACTER_PRECISION

    protected override fun processNewLocation(location: Location) {
        log ("processNewLocation()")
        location.extras = null
        if(
                sensorData.gps.size==0 ||
                (sensorData.gps.size>0 && GeoHash.fromLocation(location, geohashPrecision)!=GeoHash.fromLocation(sensorData.gps.last(), geohashPrecision))
        )
        {
            log ("processNewLocation() location.time:${location.time}, testTime:${System.currentTimeMillis()-10000}")
            if(
                    (sensorData.gps.size==0 && location.time>System.currentTimeMillis()-10000)
                    || (sensorData.gps.size>=1 && location.time>sensorData.gps.last().time)
            ){
                var distance = 0.0f
                var duration = 0L
                if(sensorData.gps.size>0){
                    distance = (sensorData.gps.last().distanceTo(location)/1000)
                    duration = location.time - sensorData.gps.last().time
                }
                session.distance += distance
                session.duration += duration
                sensorData.gps.add(location)
                sensorData.distances.add(distance)
                sensorData.durations.add(duration)

                sessionModel!!.updateSession(session)
                sensorDataModel!!.updateSensorData(sensorData)
                activity!!.dataUpdated(sensorData.deepCopy())
                programProcessor.update()
            }
        }
        else
        {
            if(sensorData.gps.size>0){
                log ("Skipping because of Geohash filtering, hash:${GeoHash.fromLocation(location, geohashPrecision)} lastHash:${GeoHash.fromLocation(sensorData.gps.last(), geohashPrecision)}")
            }
        }
    }

    //---------------------- callback and binder code ----------------------//

    //returns the instance of the service
    inner class LocalBinder : Binder() {
        val serviceInstance: RecordingService
            get() = this@RecordingService
    }

    interface Callbacks {
        fun recordingStarted()
        fun recordingStopped()
        fun updateClient(isRecording: Boolean)
        fun mockLocationChanged(enabled: Boolean)
        fun dataUpdated(data:DataVOs.SensorData)
    }

    interface ActionHandler {
        fun execute(intent: Intent)
    }
}
