package no.none.runtrainer.services.handlers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import no.none.runtrainer.R
import no.none.runtrainer.SplashActivity
import no.none.runtrainer.services.RecordingService


class NotificationHandler(val context: Context) :RecordingService.ActionHandler {

    companion object {
        private const val SOUND_CHANNEL_ID = "no.none.runtrainer.CHANNEL_TWO"
    }

    init {
        createNotificationChannel()
    }

    override fun execute(intent: Intent) {
        if(intent.action == RecordingService.ACTION_CANCEL_NOTIFICATION){

        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Chanel name"
            val description = "Chanel description"
            val importance = NotificationManager.IMPORTANCE_HIGH

            val soundChannel = NotificationChannel(SOUND_CHANNEL_ID, name, importance)
            soundChannel.description = description

            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = context.getSystemService(NotificationManager::class.java)
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(soundChannel)
            }
        }
    }

    fun buildServiceNotification(title: String, ticker: String, text: String): Notification {
        val notificationIntent = Intent(context.applicationContext, SplashActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        notificationIntent.addCategory(Intent.CATEGORY_DEFAULT)
        val pendingIntent = PendingIntent.getActivity(context.applicationContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            var builder = Notification.Builder(context, SOUND_CHANNEL_ID)
            builder.setSmallIcon(R.drawable.ic_launcher_foreground)
            builder.setContentIntent(pendingIntent)
            builder.setContentTitle(title)
            builder.setContentText(text)
            builder.setTicker(ticker)
            builder.setOngoing(true)
            builder.setAutoCancel(false)
            builder.setStyle(Notification.BigTextStyle().bigText(text).setBigContentTitle(title))

            return builder.build()
        } else {

            val builder = NotificationCompat.Builder(context, SOUND_CHANNEL_ID)
            builder.setSmallIcon(R.drawable.ic_launcher_foreground)
            builder.priority = NotificationCompat.PRIORITY_MAX
            builder.setContentIntent(pendingIntent)
            builder.setContentTitle(title)
            builder.setContentText(text)
            builder.setTicker(ticker)
            builder.setOngoing(true)
            builder.setAutoCancel(false)
            builder.setDefaults(NotificationCompat.DEFAULT_SOUND)
            builder.setStyle(NotificationCompat.BigTextStyle().bigText(text).setBigContentTitle(title))

            return builder.build()
        }


    }

    fun clearNotification(id: Int) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(id)
    }


}