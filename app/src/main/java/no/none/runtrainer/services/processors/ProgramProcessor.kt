package no.none.runtrainer.services.processors

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import android.preference.PreferenceManager
import android.util.Log
import com.crashlytics.android.Crashlytics
import mu.KotlinLogging
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.audioPlayText
import no.none.runtrainer.audioStopBPM
import no.none.runtrainer.services.BaseLocationService
import no.none.runtrainer.services.processors.subprocessors.DistanceSegmentProcessor
import no.none.runtrainer.services.processors.subprocessors.DurationSegmentProcessor
import no.none.runtrainer.services.processors.subprocessors.ISegmentProcessor

class ProgramProcessor(var context:Context) : IRecordingProcessor {
    private val logger = KotlinLogging.logger {}

    var onlyProcess:Boolean = false

    lateinit var session:DataVOs.Session
    lateinit var sensorData: DataVOs.SensorData
    lateinit var sessionProgram:DataVOs.Program
    var processorsNotifications:ArrayList<DataVOs.ProcessorNotification> = ArrayList()

    private var currentSegment:DataVOs.Segment? = null

    private var segmentProcessor:ISegmentProcessor? = null

    private var sharedPref:SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private var sharedPrefListener:PreferencesChangedLisntener = PreferencesChangedLisntener()

    private var voiceOverEnabled:Boolean = true
    private var voiceOverInterval:Long = 15000
    private var speedToleranceDuration:Long = 15000
    private var playBpmEnabled:Boolean = true


    init {
        voiceOverEnabled = sharedPref.getBoolean(context.getString(R.string.pref_key_voice_over), true)
        voiceOverInterval = sharedPref.getString(context.getString(R.string.pref_key_voice_over_interval), "15")!!.toLong()*1000
        speedToleranceDuration = sharedPref.getString(context.getString(R.string.pref_key_speed_tolerance), "15")!!.toLong()*1000
        playBpmEnabled = sharedPref.getBoolean(context.getString(R.string.pref_key_play_bpm), true)
        sharedPref.registerOnSharedPreferenceChangeListener(sharedPrefListener)
    }

    fun setData(session:DataVOs.Session, sensorData: DataVOs.SensorData, sessionProgram:DataVOs.Program){
        this.session = session
        this.sensorData = sensorData
        this.sessionProgram = sessionProgram
//        log("setData() session$session, sensorData:$sensorData, sessionProgram$sessionProgram")

    }

    fun onlyProcessRecordedSessionData(sensorData: DataVOs.SensorData, sessionProgram:DataVOs.Program){
        val blankSession:DataVOs.Session = DataVOs.Session(sessionProgram.uid)
        val blankSensorData:DataVOs.SensorData = DataVOs.SensorData(-1)

        setData(blankSession, blankSensorData, sessionProgram)
        start()

        for (location:Location in sensorData.gps){
            var distance = 0.0f
            var duration = 0L
            if(blankSensorData.gps.size>0){
                distance = (blankSensorData.gps.last().distanceTo(location)/1000)
                duration = location.time - blankSensorData.gps.last().time
            }
            blankSession.distance += distance
            blankSession.duration += duration
            blankSensorData.gps.add(location)
            blankSensorData.distances.add(distance)
            blankSensorData.durations.add(duration)
            setData(blankSession, blankSensorData, sessionProgram)
            update()
        }
    }

    override fun start() {
        findCurrentSegment()
    }

    override fun stop() {
    }

    override fun update() {
        log("update()")
        findCurrentSegment()
        checkIfProgramFinished()
        if(segmentProcessor!=null){
            segmentProcessor!!.process()
        }
    }

    //---------------------- DataVOs.Segment processor code ----------------------//

    private fun findCurrentSegment(){
        var newSegment:DataVOs.Segment? = null
        log("findCurrentSegment()")
        if(currentSegment==null){
            newSegment = sessionProgram.segments[session.sessionRecordingProgress.segment]
        }
        else{
            if(currentSegment!!.segmentType==DataVOs.SegmentType.DISTANCE){
                if(currentSegment!!.distance<=(session.distance-session.sessionRecordingProgress.distance)){
                    newSegment = getNextSegment()
                }
            }
            if(currentSegment!!.segmentType==DataVOs.SegmentType.DURATION){
                if(currentSegment!!.duration<=(session.duration-session.sessionRecordingProgress.duration)){
                    newSegment = getNextSegment()
                }
            }
        }

        if (newSegment!=null && currentSegment != newSegment) {
            currentSegment = newSegment
            setupNextSegmentProcessor()
        }
    }

    private fun checkIfProgramFinished(){
        if(isProgramCompleted()){

            // Destroy the current segmentProcessor
            if(segmentProcessor!=null){
                segmentProcessor!!.destroy()
                segmentProcessor = null
                audioPlayText("Program finished!")
                audioStopBPM()
            }
        }
    }

    private fun setupNextSegmentProcessor(){
        // Set the values to the current distance and duration. If we set it here it guarantees that it
        // will be set to the values corresponding to the start of the segment
        session.sessionRecordingProgress.distance = session.distance
        session.sessionRecordingProgress.duration = session.duration

        // Destroy the current segmentProcessor and re-instantiate it
        if(segmentProcessor!=null){
            segmentProcessor!!.destroy()
            segmentProcessor = null
        }

        if(currentSegment!!.segmentType==DataVOs.SegmentType.DISTANCE)
        {
            segmentProcessor = DistanceSegmentProcessor(context, session, currentSegment!!, sensorData, onlyProcess)
            (segmentProcessor as DistanceSegmentProcessor).setOnNotificationListener { average, previousTimestamp, previousDistance, gpsPointIndex, notified ->
                val pn = DataVOs.ProcessorNotification(average, previousTimestamp, previousDistance, gpsPointIndex, notified)
//                println("Listener triggered pn:$pn")
                processorsNotifications.add(pn)
            }
        }
        else if(currentSegment!!.segmentType==DataVOs.SegmentType.DURATION)
        {
            segmentProcessor = DurationSegmentProcessor(context, session, currentSegment!!, sensorData, onlyProcess)
            (segmentProcessor as DurationSegmentProcessor).setOnNotificationListener { average, previousTimestamp, previousDistance, gpsPointIndex, notified ->
                val pn = DataVOs.ProcessorNotification(average, previousTimestamp, previousDistance, gpsPointIndex, notified)
//                println("Listener triggered pn:$pn")
                processorsNotifications.add(pn)
            }
        }
    }

    private fun getNextSegment():DataVOs.Segment?{
        if(session.sessionRecordingProgress.segment<sessionProgram.segments.size-1){
            session.sessionRecordingProgress.segment++
            return sessionProgram.segments[session.sessionRecordingProgress.segment]
        }
        else if(session.sessionRecordingProgress.segment>=sessionProgram.segments.size-1 && sessionProgram.loop){
            session.sessionRecordingProgress.segment = 0
            return sessionProgram.segments[session.sessionRecordingProgress.segment]
        }
        return null
    }

    private fun isProgramCompleted():Boolean{
        if(sessionProgram.loop)
        {
            return false
        }
        else
        {
            return (
                    session.sessionRecordingProgress.segment==sessionProgram.segments.size-1 &&
                        (
                            (currentSegment!!.segmentType == DataVOs.SegmentType.DISTANCE &&
                                currentSegment!!.distance <= (session.distance - session.sessionRecordingProgress.distance)
                            )
                        ||
                            (currentSegment!!.segmentType==DataVOs.SegmentType.DURATION &&
                                    currentSegment!!.duration<=(session.duration-session.sessionRecordingProgress.duration)
                            )
                        )
                    )
        }
    }

    //---------------------- shared preferences code ----------------------//

    inner class PreferencesChangedLisntener:SharedPreferences.OnSharedPreferenceChangeListener{
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        }
    }

    //---------------------- util code ----------------------//

    protected fun audioPlayText(str:String){
        if(!onlyProcess)
        {
            context.audioPlayText(str)
        }
    }

    protected fun audioStopBPM(){
        if(!onlyProcess)
        {
            context.audioStopBPM()
        }
    }

    protected fun log(msg: () -> Any?){
        if (!onlyProcess) {
            Crashlytics.log(Log.DEBUG, "ProgramProcessor", msg.toString())
            logger.info(msg)
        }
    }
    protected fun log(str:String){
        if (!onlyProcess) {
            Crashlytics.log(str)
            Crashlytics.log(Log.DEBUG, "ProgramProcessor", str)
            logger.info(str)
        }
    }
}