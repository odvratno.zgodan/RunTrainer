package no.none.runtrainer.services

import android.Manifest
import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.core.content.ContextCompat
import com.crashlytics.android.Crashlytics
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import mu.KotlinLogging

abstract class BaseLocationService : Service() {

    private val logger = KotlinLogging.logger {}

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    //---------------------- location services code ----------------------//

    private var googleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null


    @Synchronized
    protected fun buildGoogleApiClient() {
        log("buildGoogleApiClient()")
        if(googleApiClient==null){
            googleApiClient = GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(connectionCallbacks)
                    .addOnConnectionFailedListener(connectionCallbacks)
                    .addApi(LocationServices.API)
                    .build()
        }
        if(googleApiClient!=null && (!googleApiClient!!.isConnected || !googleApiClient!!.isConnecting)){
            googleApiClient!!.connect()
        }
    }

    protected fun stopGoogleApiClient() {
        if (googleApiClient != null) {
            LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)
        }
    }

    private val connectionCallbacks:APICallbacks = APICallbacks()

    inner class APICallbacks: GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
        override fun onConnected(p0: Bundle?) {

            mLocationRequest = LocationRequest()
            mLocationRequest!!.interval = 2000
            mLocationRequest!!.fastestInterval = 2000
            mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.getFusedLocationProviderClient(applicationContext).requestLocationUpdates(mLocationRequest, locationCallback, null)
            }
        }

        override fun onConnectionSuspended(p0: Int) {
            googleApiClient!!.connect()
        }

        override fun onConnectionFailed(p0: ConnectionResult) {
            //
        }
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
            super.onLocationAvailability(locationAvailability)
            // This method can be called at any time so we shouldn't start or stop recoding from it.
            // If we do start/stop it from here the ongoing trip
            if (locationAvailability!!.isLocationAvailable) {
                log ("Location available.")
            } else {
                log ("Location not available!")
                //
            }
        }

        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            for (i in 0 until locationResult!!.locations.size) {
                onLocationChanged(locationResult.locations[i])
            }
        }
    }

    protected fun onLocationChanged(location: Location?) {
        processNewLocation(location!!)
    }

    protected abstract fun processNewLocation(location: Location)


    //---------------------- util code ----------------------//

    protected fun log(msg: () -> Any?){
        Crashlytics.log(Log.DEBUG, "RecordingService", msg.toString())
        logger.info(msg)
    }
    protected fun log(str:String){
        Crashlytics.log(str)
        Crashlytics.log(Log.DEBUG, "RecordingService", str)
        logger.info(str)
    }
}