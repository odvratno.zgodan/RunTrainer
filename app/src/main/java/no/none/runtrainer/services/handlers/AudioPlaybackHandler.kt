package no.none.runtrainer.services.handlers

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.SoundPool
import android.speech.tts.TextToSpeech
import mu.KotlinLogging
import no.none.runtrainer.R
import no.none.runtrainer.services.RecordingService
import java.util.*


class AudioPlaybackHandler(val context: Context): RecordingService.ActionHandler, SoundPool.OnLoadCompleteListener, TextToSpeech.OnInitListener {

    companion object {
        const val ACTION_PLAY_TRACK = "no.none.runtrainer.play_track"
        const val ACTION_PLAY_BPM = "no.none.runtrainer.play_bpm"
        const val ACTION_STOP_BPM = "no.none.runtrainer.stop_bpm"
        const val ACTION_TEXT_TO_SPEECH = "no.none.runtrainer.text_to_speech"
        const val ACTION_STOP_ALL = "no.none.runtrainer.stop_all"

        const val AUDIO_FILE = "no.none.runtrainer.audio_file"
        const val TEXT_TO_SPEAK = "no.none.runtrainer.text_to_speak"
        const val BPM_VALUE = "no.none.runtrainer.bpm_value"
        const val TOO_SLOW = "no.none.runtrainer.too_slow"
        const val TOO_FAST = "no.none.runtrainer.too_fast"
        const val BPM_ONE_STEP = "no.none.runtrainer.bpm_one_step"
        const val BPM_TWO_STEP = "no.none.runtrainer.bpm_two_step"

        @JvmStatic
        // This Intent class is RecordingService::class.java because we wan't it to pass it into the AudioPlaybackHandler
        fun playAudioIntent(context: Context, audioFile:String): Intent {
            return Intent(context, RecordingService::class.java).apply {
                action = ACTION_PLAY_TRACK
                putExtra(AUDIO_FILE, audioFile)
            }
        }

        @JvmStatic
        // This Intent class is RecordingService::class.java because we wan't it to pass it into the AudioPlaybackHandler
        fun playBPMIntent(context: Context, audioFile:String, bpm:Int?=null): Intent {
            return Intent(context, RecordingService::class.java).apply {
                this.action = ACTION_PLAY_BPM
                putExtra(AUDIO_FILE, audioFile)
                putExtra(BPM_VALUE, bpm)
            }
        }

        @JvmStatic
        // This Intent class is RecordingService::class.java because we wan't it to pass it into the AudioPlaybackHandler
        fun textToSpeechIntent(context: Context, textToSpeak:String): Intent {
            return Intent(context, RecordingService::class.java).apply {
                action = ACTION_TEXT_TO_SPEECH
                putExtra(TEXT_TO_SPEAK, textToSpeak)
            }
        }


        @JvmStatic
        // This Intent class is RecordingService::class.java because we wan't it to pass it into the AudioPlaybackHandler
        fun stopBPMIntent(context: Context): Intent {
            return Intent(context, RecordingService::class.java).apply {
                this.action = ACTION_STOP_BPM
            }
        }

        @JvmStatic
        // This Intent class is RecordingService::class.java because we wan't it to pass it into the AudioPlaybackHandler
        fun stopAllIntent(context: Context): Intent {
            return Intent(context, RecordingService::class.java).apply {
                this.action = ACTION_STOP_ALL
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    private var audioFile = ""
    private var textToSpeak = ""

    private var maxStreams = 2

    private var soundPool:SoundPool

    private var sounds:HashMap<String, Int>

    private var voiceSoundId:Int? = null
    private var bpmSoundId:Int? = null

    private var delayedVoiceSoundName:String = ""
    private var delayedBPMSoundName:String = ""
    private var delayedBPMRate:Int = 0

    private var numLoadedSounds:Int = 0
    private var finishedLoadingSounds:Boolean = false

    private var tts: TextToSpeech
    private var textToSpeechInitialized:Boolean = false

    init {
        /*val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()*/

        soundPool = SoundPool.Builder()
                .setMaxStreams(maxStreams)
                .build()

        tts = TextToSpeech(context, this)

        val am = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val sampleRateStr: String? = am.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE)
        var sampleRate: Int = sampleRateStr?.let { str ->
            Integer.parseInt(str).takeUnless { it == 0 }
        } ?: 44100 // Use a default value if property not found

        sounds = HashMap(2)
        sounds[TOO_SLOW] = soundPool.load(context, R.raw.too_slow, 1)
        sounds[TOO_FAST] = soundPool.load(context, R.raw.too_fast, 1)
        if(sampleRate==44100){
            sounds[BPM_ONE_STEP] = soundPool.load(context, R.raw.one_step_60bpm_mp3_44khz, 1)
            sounds[BPM_TWO_STEP] = soundPool.load(context, R.raw.two_step_60bpm_mp3_44khz, 1)
        }else{
            sounds[BPM_ONE_STEP] = soundPool.load(context, R.raw.one_step_60bpm_mp3_48khz, 1)
            sounds[BPM_TWO_STEP] = soundPool.load(context, R.raw.two_step_60bpm_mp3_48khz, 1)
        }

        soundPool.setOnLoadCompleteListener(this)
    }

    override fun execute(intent: Intent) {
        val extras = intent.extras!!
        if(intent.action == AudioPlaybackHandler.ACTION_PLAY_TRACK){
            audioFile = extras.getString(AUDIO_FILE)!!
            if(audioFile!=""){
                playTrack(audioFile)
            }
        }
        else if(intent.action == AudioPlaybackHandler.ACTION_PLAY_BPM){
            audioFile = extras.getString(AUDIO_FILE)!!
            val bpm = extras.getInt(BPM_VALUE, 60)
            if(audioFile!=""){
                playBPM(audioFile, bpm)
            }
        }
        else if(intent.action == AudioPlaybackHandler.ACTION_TEXT_TO_SPEECH){
            textToSpeak = extras.getString(TEXT_TO_SPEAK)!!
            if(textToSpeak!=""){
                textToSpeech(textToSpeak)
            }
        }
        else if(intent.action == AudioPlaybackHandler.ACTION_STOP_BPM){
            stopBPM()
        }
        else if(intent.action == AudioPlaybackHandler.ACTION_STOP_ALL){
            destroy()
        }
    }

    private fun playTrack(sound:String){
        if(!finishedLoadingSounds){
            delayedVoiceSoundName = sound;
            return
        }
        if(voiceSoundId!=null){
            soundPool.stop(voiceSoundId!!)
            voiceSoundId = null
        }
        voiceSoundId = soundPool.play(sounds[sound]!!, 1.0f, 1.0f, 1, 0, 1f)
    }

    private fun textToSpeech(text:String){
        logger.info { "textToSpeech() textToSpeechInitialized:$textToSpeechInitialized, text:$text" }
        if(textToSpeechInitialized){
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null,"")
        }
    }

    private fun playBPM(sound:String, BPM:Int){
        if(!finishedLoadingSounds){
            delayedBPMSoundName = sound;
            delayedBPMRate = BPM
            return
        }
        val rate:Float = BPM.toFloat()/60.0f
        if(bpmSoundId!=null){
            soundPool.stop(bpmSoundId!!)
            bpmSoundId = null
        }
        bpmSoundId = soundPool.play(sounds[sound]!!, 1.0f, 1.0f, 1, -1, rate)
    }

    private fun stopBPM(){
        if(bpmSoundId!=null){
            soundPool.stop(bpmSoundId!!)
            bpmSoundId = null
        }
    }
    fun destroy(){
        if(voiceSoundId!=null){
            soundPool.stop(voiceSoundId!!)
            voiceSoundId = null
        }

        if(bpmSoundId!=null){
            soundPool.stop(bpmSoundId!!)
            bpmSoundId = null
        }

        tts.stop()
        tts.shutdown()

        soundPool.release()
    }

    //---------------------- SoundPool code ----------------------//

    override fun onLoadComplete(soundPool: SoundPool?, sampleId: Int, status: Int) {
        numLoadedSounds++
        if(numLoadedSounds>=4){
            soundPool?.setOnLoadCompleteListener(null)
            finishedLoadingSounds = true
            if(delayedVoiceSoundName!=""){
                playTrack(delayedVoiceSoundName)
            }
            if(delayedBPMSoundName!="" && delayedBPMRate!=0){
                playBPM(delayedBPMSoundName, delayedBPMRate)
            }
        }
    }

    //---------------------- TextToSpeech code ----------------------//

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            // set US English as language for tts
            val result = tts.setLanguage(Locale.ENGLISH)
            tts.setSpeechRate(0.8f)

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                logger.error { "The Language specified is not supported!"}
            } else {
                textToSpeechInitialized = true
            }

        } else {
            logger.error{"Initialisation Failed!"}
        }
    }
}