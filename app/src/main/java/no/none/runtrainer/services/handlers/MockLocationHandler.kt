package no.none.runtrainer.services.handlers

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.SystemClock
import androidx.core.content.ContextCompat
import com.codebutchery.androidgpx.data.GPXDocument
import com.codebutchery.androidgpx.data.GPXTrackPoint
import com.codebutchery.androidgpx.xml.GPXListeners
import com.codebutchery.androidgpx.xml.GPXParser
import com.google.android.gms.location.LocationServices
import mu.KotlinLogging
import no.none.runtrainer.services.RecordingService
import java.io.IOException
import java.io.InputStream
import java.util.*
import kotlin.concurrent.schedule

class MockLocationHandler(val context: Context): RecordingService.ActionHandler {

    companion object {
        const val ACTION_START_MOCK_LOCATION = "no.none.runtrainer.start_mock_location"
        const val ACTION_STARTED_MOCK_LOCATION = "no.none.runtrainer.started_mock_location"
        const val ACTION_STOP_MOCK_LOCATION = "no.none.runtrainer.stop_mock_location"
        const val ACTION_STOPED_MOCK_LOCATION = "no.none.runtrainer.stopped_mock_location"

        const val MOCK_LOCATION_FILE_PATH_EXTRA = "no.none.runtrainer.file_path"
    }

    private val logger = KotlinLogging.logger {}

    private var fileName = ""
    private var inputStream:InputStream? = null

    public var enabled:Boolean = false

    override fun execute(intent: Intent) {
        if(intent.action == RecordingService.ACTION_START_MOCK_LOCATION){
            fileName = intent.getStringExtra(RecordingService.MOCK_LOCATION_FILE_PATH_EXTRA)
            if(fileName!=""){
                startMockLocation()
            }
        }
        if(intent.action == RecordingService.ACTION_STOP_MOCK_LOCATION){
            destroy()
        }
    }

    public fun startMockLocation(){
        logger.info ("startMockLocation()")
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.getFusedLocationProviderClient(context).setMockMode(true)
        }
        var mParser: GPXParser?
        try {
            inputStream = context.assets.open(fileName)
            // The GpxParser automatically closes the InputStream so we do not have to bother about it
            mParser = GPXParser(gpxParserListener, null)
            mParser.parse(inputStream)
        } catch (e: IOException) {
            logger.info ("IOExeption opening file")
        }
    }

    fun destroy(){
        if(timer != null){
            timer!!.cancel()
            timer!!.purge()
        }

        if(inputStream != null){
            inputStream!!.close()
        }

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.getFusedLocationProviderClient(context).setMockMode(false)
        }
        enabled = false

        val intent = Intent(context, RecordingService::class.java)
        intent.action = RecordingService.ACTION_STOPED_MOCK_LOCATION
        context.startService(intent)
    }

    private var timer:Timer? = null
    private var parsedGpx:GPXDocument? = null
    private var currentPoint = 0
    private var previousPointTime:Long = 0

    private fun replayGpxData(){
        enabled = true

        val intent = Intent(context, RecordingService::class.java)
        intent.action = RecordingService.ACTION_STARTED_MOCK_LOCATION
        context.startService(intent)

        if(parsedGpx!=null){
            if(parsedGpx!!.tracks.size>0 && parsedGpx!!.tracks[0].segments.size>0 && parsedGpx!!.tracks[0].segments[0].trackPoints.size>0) {
                currentPoint = 0
                previousPointTime = parsedGpx!!.tracks[0].segments[0].trackPoints[currentPoint].timeStamp.time
                timerComplete()
            }
        }
    }

    private fun timerComplete(){
        if(currentPoint<parsedGpx!!.tracks[0].segments[0].trackPoints.size){
            val point = parsedGpx!!.tracks[0].segments[0].trackPoints[currentPoint]
            val delay=point.timeStamp.time-previousPointTime
            if(delay>0){
                previousPointTime = point.timeStamp.time
                timer = Timer("mock_schedule", true)
                timer!!.schedule(delay) {
                    timer!!.cancel()
                    doSomethingWithMockPoint()
                }
            }else{
                doSomethingWithMockPoint()
            }
        }
    }

    private fun doSomethingWithMockPoint(){
        val point = parsedGpx!!.tracks[0].segments[0].trackPoints[currentPoint]
        var previousLocation: Location? = null
        if(currentPoint>0){
            previousLocation = trackPointToLocation(parsedGpx!!.tracks[0].segments[0].trackPoints[currentPoint-1])
        }
        val currentLocation = trackPointToLocation(point)
        if(previousLocation!=null){
            currentLocation.speed = currentLocation.distanceTo(previousLocation)/((currentLocation.time - previousLocation.time)/1000)
            currentLocation.time = System.currentTimeMillis()
        }

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.getFusedLocationProviderClient(context).setMockLocation(currentLocation)
        }

        currentPoint++
        timerComplete()
    }

    private fun trackPointToLocation(point: GPXTrackPoint):Location{
        val location = Location(LocationManager.GPS_PROVIDER)
        location.latitude = point.latitude.toDouble()
        location.longitude = point.longitude.toDouble()
        location.altitude = point.elevation.toDouble()
        location.time = point.timeStamp.time
        location.accuracy = 5.5f
        location.elapsedRealtimeNanos = SystemClock.elapsedRealtimeNanos()
        location.speed = 0.0f
        return location
    }

    private val gpxParserListener = object: GPXListeners.GPXParserListener {
        override fun onGpxParseStarted() {
            //
        }

        override fun onGpxParseError(type: String?, message: String?, lineNumber: Int, columnNumber: Int) {
            logger.info ("onGpxParseError() message:$message")
        }

        override fun onGpxParseCompleted(document: GPXDocument?) {
            parsedGpx = document
            inputStream!!.close()
            replayGpxData()
        }
    }

}