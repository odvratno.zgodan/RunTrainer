package no.none.runtrainer

import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Build
import com.google.android.gms.maps.model.LatLng
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.services.handlers.AudioPlaybackHandler
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.round
import kotlin.math.sqrt

fun Context.startRecordingServiceWithIntent(intent:Intent){
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        startForegroundService(intent)
    } else {
        this.startService(intent)
    }
}

fun Context.audioPlayTooSlow(){
    val intent = AudioPlaybackHandler.playAudioIntent(this, AudioPlaybackHandler.TOO_SLOW)
    this.startRecordingServiceWithIntent(intent)
}

fun Context.audioPlayTooFast(){
    val intent = AudioPlaybackHandler.playAudioIntent(this, AudioPlaybackHandler.TOO_FAST)
    this.startRecordingServiceWithIntent(intent)
}

fun Int.PxToDp(context:Context):Int{
    return (this / context.resources.displayMetrics.density).toInt()
}

fun Int.DpToPx(context:Context):Int{
    return (this * context.resources.displayMetrics.density).toInt()
}

fun Context.audioPlayText(textToSpeak:String){
    val intent = AudioPlaybackHandler.textToSpeechIntent(this, textToSpeak)
    this.startRecordingServiceWithIntent(intent)
}

fun Context.audioPlayBPM(stepType: DataVOs.StepType, bpm:Int){
    if(stepType == DataVOs.StepType.NO_STEP) {
        val intent = AudioPlaybackHandler.stopBPMIntent(this)
        this.startRecordingServiceWithIntent(intent)
    }
    else if(stepType == DataVOs.StepType.ONE_STEP) {
        val intent = AudioPlaybackHandler.playBPMIntent(this, AudioPlaybackHandler.BPM_ONE_STEP, bpm)
        this.startRecordingServiceWithIntent(intent)
    }
    else if(stepType == DataVOs.StepType.TWO_STEP) {
        val intent = AudioPlaybackHandler.playBPMIntent(this, AudioPlaybackHandler.BPM_TWO_STEP, bpm)
        this.startRecordingServiceWithIntent(intent)
    }
}

fun Context.audioStopBPM(){
    val intent = AudioPlaybackHandler.stopBPMIntent(this)
    this.startRecordingServiceWithIntent(intent)
}

fun Long.formatDurationToMMSSFloat(): Float {
    val date = Date(this)
    val seconds = (date.time / 1000).toInt() % 60
    var minutes = (date.time / 1000).toInt() / 60 % 60
    val hours = (date.time / 1000).toInt() / 3600
    if(hours!=0){
        minutes += hours*60
    }
    return minutes + (seconds/60f)
}

fun Long.formatDurationToMMSSString(): String {
    val date = Date(this)
    val hours = (date.time / 1000).toInt() / 3600
    val minutes = ((date.time / 1000).toInt() / 60 % 60) + hours*60
    val seconds = (date.time / 1000).toInt() % 60
    return minutes.toString() + ":" + seconds.toString().padStart(2, '0')
}

fun Long.formatDurationToSpeechMMSSString(): String {
    val date = Date(this)
    val hours = (date.time / 1000).toInt() / 3600
    val minutes = ((date.time / 1000).toInt() / 60 % 60) + hours*60
    val seconds = (date.time / 1000).toInt() % 60
    return "$minutes minutes $seconds seconds"
}

fun Long.formatDurationToMPerSecFloat(): Float {
    return if(this>0)
        (1f/(this/1000f/60f))*16.666667f
    else{
        0f
    }
}

fun Float.formatDurationToMMSSString(): String {

    val minutes = Math.floor(this.toDouble())
    val seconds = Math.floor((this - minutes)*60)
    return minutes.toInt().toString() + ":" + seconds.toInt().toString().padStart(2, '0')
}

fun Float.formatMperSecToMMSSFloat(): Float {
    return if(this>0){
        (1000/this)/60
    }else{
        0f
    }
}

fun Float.formatMperSecToMMSSString(): String {
    return if(this>0){
        val totalSeconds = (1000/this).toInt()
        val hours = (totalSeconds) / 3600
        val minutes = (totalSeconds / 60 % 60) + hours*60
        val seconds = (totalSeconds) % 60
        return minutes.toString() + ":" + seconds.toString().padStart(2, '0')

    }else{
        "Not moving"
    }
}

fun Float.formatMperSecToSpeechMMSSString(): String {
    return if(this>0){
        val totalSeconds = (1000/this).toInt()
        val hours = (totalSeconds) / 3600
        val minutes = (totalSeconds / 60 % 60) + hours*60
        val seconds = (totalSeconds) % 60
        return "$minutes minutes $seconds seconds"
    }else{
        "Not moving"
    }
}

fun ArrayList<Location>.toMapLatLng():ArrayList<LatLng>{
    val list:ArrayList<LatLng> = ArrayList(1)
    if(this.size>0){
        for (i in 0 until this.size){
            list.add(this[i].toMapLatLng())
        }
    }
    return list
}
fun List<Location>.toMapLatLng():ArrayList<LatLng>{
    val list:ArrayList<LatLng> = ArrayList(1)
    if(this.isNotEmpty()){
        for (i in 0 until this.size){
            list.add(this[i].toMapLatLng())
        }
    }
    return list
}
fun Location.toMapLatLng():LatLng{
    return LatLng(this.latitude, this.longitude)
}

fun Iterable<Float>.weightedMovingAverage(): Double {
    var sum: Double = 0.0
    var count: Int = 0
    for (element in this) {
        sum += element*(count+1)
        ++count
    }
    return if (count == 0) Double.NaN else sum / ((count*(count+1))/2)
}
fun Iterable<Float>.hullMovingAverage(): Double {
    /**
    https://searchcode.com/codesearch/view/64893970/
    */
    var sqrtTimePeriod = round(sqrt(this.count().toDouble())).toInt()
    if(sqrtTimePeriod<1){
        sqrtTimePeriod = 1
    }
    val arraySize=this.count()
    val len = arraySize-sqrtTimePeriod
    var wmaValues = mutableListOf<Float>()
    for(i in 0 until sqrtTimePeriod){
        var fullList = this.toList().subList(arraySize - 1 - i - len, arraySize - i)
        var halfList = this.toList().subList(arraySize - 1 - i - len/2, arraySize - i)
        var wmaFullPeriod: Double = fullList.weightedMovingAverage()
        var wmaHalfPeriod: Double = halfList.weightedMovingAverage()
//        println("fullList:$fullList, halfList:$halfList")
//        println("wmaFullPeriod:$wmaFullPeriod, wmaHalfPeriod:$wmaHalfPeriod")
        wmaValues.add((2.0 * wmaHalfPeriod - wmaFullPeriod).toFloat())
    }
//    println("wmaValues:$wmaValues")
    return wmaValues.weightedMovingAverage()
}