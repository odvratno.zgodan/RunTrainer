package no.none.runtrainer.ui.programdetails.pickers

import mobi.upod.timedurationpicker.TimeDurationPicker
import mobi.upod.timedurationpicker.TimeDurationPickerDialogFragment
import mu.KotlinLogging

class DurationPickerDialogFragment: TimeDurationPickerDialogFragment() {

    private val logger = KotlinLogging.logger {}

    interface DurationPickerDialogListener {
        fun durationChanged(duration: Long)
    }

    private var initialDuration= (6 * 60 * 1000).toLong()
    private var valueSetCallback: ((Long) -> Unit)? = null
    private var listener: DurationPickerDialogListener? = null

    fun setListeners(listener: DurationPickerDialogListener){
        this.listener = listener
    }

    fun onSetListener(callback:(position: Long) -> Unit){
        valueSetCallback = callback
    }

    fun setInitialDuration(value: Long){
        initialDuration = value
    }

    override fun getInitialDuration(): Long {
        return initialDuration
    }


    override fun setTimeUnits(): Int {
        return TimeDurationPicker.MM_SS
    }

    override fun onDurationSet(view: TimeDurationPicker?, duration: Long) {
        logger.info ("duration:$duration")
        listener?.durationChanged(duration)
        valueSetCallback?.invoke(duration)
    }
}