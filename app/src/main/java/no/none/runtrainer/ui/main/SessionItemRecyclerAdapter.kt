package no.none.runtrainer.ui.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import java.text.SimpleDateFormat


class SessionItemRecyclerAdapter(val listener: ClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val SECTION_TYPE = 1
    private val HEADER_TYPE = 2

    private var sessionsList: ArrayList<PairedSession> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder:RecyclerView.ViewHolder

        val itemView: View
        when (viewType) {
            SECTION_TYPE -> {
                itemView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_session, parent, false)
                holder = ViewHolder(itemView)
                itemView.setOnClickListener{ v ->
                    listener.itemOnClick(v, sessionsList[holder.adapterPosition].session!!.id!!)
                }
            }
            HEADER_TYPE -> {
                itemView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_session_header, parent, false)
                return SectionHeaderViewHolder(itemView)
            }
            else -> {
                itemView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_session, parent, false)
                holder = ViewHolder(itemView)
                itemView.setOnClickListener{ v ->
                    listener.itemOnClick(v, sessionsList[holder.adapterPosition].session!!.id!!)
                }
            }
        }

        return holder
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemViewType = getItemViewType(position)
        if (itemViewType == SECTION_TYPE) {
            val itemHolder = holder as ViewHolder
            val session = sessionsList[position].session!!
            itemHolder.title.text = session.title
            val distString = if(session.distance.toString().length>=4){
                "${session.distance.toString().substring(0, 4)}Km"
            }else{
                "${session.distance.toString().substring(0, session.distance.toString().length-1)}Km"
            }
            val format = SimpleDateFormat("d.M.yyyy, H:mm")
            itemHolder.startDate.text = format.format(session.start)
            itemHolder.distance.text = distString
        } else {
            val headerViewHolder = holder as SectionHeaderViewHolder
            headerViewHolder.sectionLabel.text = sessionsList[position].sectionHeader!!.name
        }
    }

    fun setData(data: List<DataVOs.Session>) {
        @SuppressLint("SimpleDateFormat")
        val format = SimpleDateFormat("M.yyyy")
        val map = data.groupBy{format.format(it.start)}
        sessionsList = arrayListOf()
        for(it in map){
            sessionsList.add(PairedSession(SectionHeaderData(it.key), null))
            for(session in it.value){
                sessionsList.add(PairedSession(null, session))
            }

        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return sessionsList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (sessionsList[position].sectionHeader!=null) {
            HEADER_TYPE
        } else {
            SECTION_TYPE
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.title)
        val startDate: TextView = view.findViewById(R.id.start_date)
        val distance: TextView = view.findViewById(R.id.distance)
    }

    inner class SectionHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val sectionLabel: TextView = view.findViewById(R.id.section_label)
    }

    interface ClickListener {
        fun itemOnClick(v: View, sessionId: Int)
    }

    inner class PairedSession(val sectionHeader:SectionHeaderData?=null, val session:DataVOs.Session?=null)

    inner class SectionHeaderData(val name:String="")

}