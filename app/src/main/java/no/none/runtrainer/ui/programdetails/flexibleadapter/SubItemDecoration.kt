package no.none.runtrainer.ui.programdetails.flexibleadapter

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration
import no.none.runtrainer.DpToPx

class SubItemDecoration(var context: Context) : FlexibleItemDecoration(context) {

    override fun getItemOffsets(outRect: Rect, view: View, recyclerView: RecyclerView, state: RecyclerView.State) {
        if (recyclerView.adapter is FlexibleAdapter<*>) {
            val adapter = recyclerView.adapter as FlexibleAdapter<*>?
            val position = recyclerView.getChildAdapterPosition(view)
            if ((adapter!!.getItem(position) is SegmentItem)) {
                var segmentItem = (adapter.getItem(position) as SegmentItem).segment
                if(segmentItem.parentSegmentUid!="")
                {
                    outRect.set(20.DpToPx(context), 0, 0, 0)
                }
            }
        }
    }
}
