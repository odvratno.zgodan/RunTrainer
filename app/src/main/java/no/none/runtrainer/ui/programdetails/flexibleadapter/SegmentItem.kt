package no.none.runtrainer.ui.programdetails.flexibleadapter

import android.view.View
import android.widget.*
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.items.IHeader
import eu.davidea.viewholders.FlexibleViewHolder
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.formatDurationToMMSSString
import no.none.runtrainer.formatDurationToSpeechMMSSString

class SegmentItem(override var segment: DataVOs.Segment) : AbstractSegmentItem<FlexibleViewHolder>() {

    internal var header: IHeader<*>? = null

    init {
        isDraggable = true
    }

    override fun equals(o: Any?): Boolean {
        if (o is SegmentItem) {
            val inItem = o as SegmentItem?
            return this.segment.uid == inItem!!.segment.uid
        }
        return false
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<*>>): FlexibleViewHolder {
        return when(segment.segmentType){
            DataVOs.SegmentType.DISTANCE -> {
                DistanceViewHolder(view, adapter)
            }
            DataVOs.SegmentType.DURATION -> {
                DurationViewHolder(view, adapter)
            }
            else -> {
                DistanceViewHolder(view, adapter)
            }
        }
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<*>>, holder: FlexibleViewHolder, position: Int, payloads: List<Any>) {
        when(segment.segmentType){
            DataVOs.SegmentType.DISTANCE -> {
                (holder as DistanceViewHolder).bind(segment)
            }
            DataVOs.SegmentType.DURATION -> {
                (holder as DurationViewHolder).bind(segment)
            }
            else -> {

            }
        }
    }

    override fun getLayoutRes(): Int {
        return when(segment.segmentType){
            DataVOs.SegmentType.DISTANCE -> {
                R.layout.list_item_program_details_distance_segment_expandable
            }
            DataVOs.SegmentType.DURATION -> {
                R.layout.list_item_program_details_duration_segment_expandable
            }
            else -> {
                -1
            }
        }
    }

    /**
     * The ViewHolder used by this item.
     * Extending from FlexibleViewHolder is recommended especially when you will use
     * more advanced features.
     */
    class DistanceViewHolder(private val view: View, val adapter: FlexibleAdapter<*>) : FlexibleViewHolder(view, adapter), ISegmentViewHolder {

        lateinit var title: TextView
        lateinit var details: TextView
        lateinit var distance: EditText
        lateinit var speed: EditText
        lateinit var bpm: EditText
        lateinit var stepType: Spinner
        lateinit var deleteBtn: ImageButton

        override fun bind(segment: DataVOs.Segment) {
            title = view.findViewById(R.id.title)
            details = view.findViewById(R.id.details)
            distance = view.findViewById(R.id.distance)
            speed = view.findViewById(R.id.speed)
            bpm = view.findViewById(R.id.bpm)
            stepType = view.findViewById(R.id.step_type)
            deleteBtn = view.findViewById(R.id.delete_btn)

            distance.setOnClickListener {
                if(adapter is SegmentFlexibleAdapter){
                    adapter.distanceCallback?.invoke(adapterPosition)
                }
            }

            speed.setOnClickListener {
                if(adapter is SegmentFlexibleAdapter) {
                    adapter.speedCallback?.invoke(adapterPosition)
                }
            }

            bpm.setOnClickListener {
                if(adapter is SegmentFlexibleAdapter) {
                    adapter.bpmCallback?.invoke(adapterPosition)
                }
            }

            val arrayAdapter = ArrayAdapter(view.context, android.R.layout.simple_spinner_item, DataVOs.StepType.values())
            stepType.adapter = arrayAdapter
            stepType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    //
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    segment.stepType = (p0?.selectedItem as DataVOs.StepType)
                    println("segment.stepType:${segment.stepType}")
                }
            }

            deleteBtn.setOnClickListener {
                adapter.removeItem(adapterPosition)
            }

            val distanceStr =  "${segment.distance} Km"
            distance.setText(distanceStr)
            val speedStr = "${segment.speedMillisecondsPerKm.formatDurationToMMSSString()} min/Km"
            speed.setText(speedStr)
            val bpmStr = "${segment.bpm} BPM"
            bpm.setText(bpmStr)
            stepType.setSelection(segment.stepType.type)

            val detailText = segment.distance.toString() + "Km, " +
                    segment.speedMillisecondsPerKm.formatDurationToMMSSString() +
                    "min/Km, " + segment.bpm.toString() + "BPM"

            title.text = segment.title
            details.text = detailText
        }
    }

    class DurationViewHolder(private val view: View, val adapter: FlexibleAdapter<*>) : FlexibleViewHolder(view, adapter), ISegmentViewHolder {

        lateinit var title: TextView
        lateinit var details: TextView
        lateinit var duration: EditText
        lateinit var speed: EditText
        lateinit var bpm: EditText
        lateinit var stepType: Spinner
        lateinit var deleteBtn: ImageButton

        override fun bind(segment: DataVOs.Segment) {
            title = view.findViewById(R.id.title)
            details = view.findViewById(R.id.details)
            duration = view.findViewById(R.id.duration)
            speed = view.findViewById(R.id.speed)
            bpm = view.findViewById(R.id.bpm)
            stepType = view.findViewById(R.id.step_type)
            deleteBtn = view.findViewById(R.id.delete_btn)

            duration.setOnClickListener {
                if(adapter is SegmentFlexibleAdapter){
                    adapter.durationCallback?.invoke(adapterPosition)
                }
            }

            speed.setOnClickListener {
                if(adapter is SegmentFlexibleAdapter) {
                    adapter.speedCallback?.invoke(adapterPosition)
                }
            }

            bpm.setOnClickListener {
                if(adapter is SegmentFlexibleAdapter) {
                    adapter.bpmCallback?.invoke(adapterPosition)
                }
            }

            val arrayAdapter = ArrayAdapter(view.context, android.R.layout.simple_spinner_item, DataVOs.StepType.values())
            stepType.adapter = arrayAdapter
            stepType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    //
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    segment.stepType = (p0?.selectedItem as DataVOs.StepType)
                    println("segment.stepType:${segment.stepType}")
                }
            }

            deleteBtn.setOnClickListener {
                if(adapter is SegmentFlexibleAdapter) {
                    adapter.deleteCallback?.invoke(adapterPosition)
                }
            }


            val durationStr = segment.duration.formatDurationToSpeechMMSSString()
            duration.setText(durationStr)
            val speedStr = "${segment.speedMillisecondsPerKm.formatDurationToMMSSString()} min/Km"
            speed.setText(speedStr)
            val bpmStr = "${segment.bpm} BPM"
            bpm.setText(bpmStr)
            stepType.setSelection(segment.stepType.type)

            val detailText = segment.duration.formatDurationToMMSSString() + "min, " +
                    segment.speedMillisecondsPerKm.formatDurationToMMSSString() +
                    "min/Km, " + segment.bpm.toString() + "BPM"

            title.text = segment.title
            details.text = detailText
        }
    }
}
