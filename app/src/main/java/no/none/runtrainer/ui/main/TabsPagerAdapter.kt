package no.none.runtrainer.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class TabsPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

    var pages: ArrayList<Fragment> = ArrayList()

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return pages[position].toString()
    }

    fun addPage(f: Fragment) {
        pages.add(f)
    }
}