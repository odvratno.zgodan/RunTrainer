package no.none.runtrainer.ui.programdetails.flexibleadapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible
import no.none.runtrainer.data.DataVOs

abstract class AbstractSegmentItem<VH : RecyclerView.ViewHolder> : IFlexible<VH> {

    /* Item flags recognized by the FlexibleAdapter */
    protected var mEnabled = true
    protected var mHidden = false
    protected var mSelectable = true
    protected var mDraggable = true
    protected var mSwipeable = true
    open lateinit var segment:DataVOs.Segment

    /*---------------*/
    /* BASIC METHODS */
    /*---------------*/

    /**
     * You **MUST** implement this method to compare items **unique** identifiers.
     *
     * Adapter needs this method to distinguish them and pick up correct items.
     * See [
 * Writing a correct `equals` method](http://developer.android.com/reference/java/lang/Object.html#equals(java.lang.Object)) to implement your own `equals` method.
     *
     * **Hint:** If you don't use unique IDs, reimplement with Basic Java implementation:
     * <pre>
     * public boolean equals(Object o) {
     * return this == o;
     * }</pre>
     *
     * **Important Note:** When used with `Hash[Map,Set]`, the general contract for the
     * `equals` and [.hashCode] methods is that if `equals` returns `true`
     * for any two objects, then `hashCode()` must return the same value for these objects.
     * This means that subclasses of `Object` usually override either both methods or neither
     * of them.
     *
     * @param o instance to compare
     * @return true if items are equals, false otherwise.
     */
    abstract override fun equals(o: Any?): Boolean

    override fun isEnabled(): Boolean {
        return mEnabled
    }

    override fun setEnabled(enabled: Boolean) {
        mEnabled = enabled
    }

    override fun isHidden(): Boolean {
        return mHidden
    }

    override fun setHidden(hidden: Boolean) {
        mHidden = hidden
    }

    override fun getSpanSize(spanCount: Int, position: Int): Int {
        return 1
    }

    override fun shouldNotifyChange(newItem: IFlexible<*>): Boolean {
        return true
    }

    /*--------------------*/
    /* SELECTABLE METHODS */
    /*--------------------*/

    override fun isSelectable(): Boolean {
        return mSelectable
    }

    override fun setSelectable(selectable: Boolean) {
        this.mSelectable = selectable
    }

    override fun getBubbleText(position: Int): String {
        return (position + 1).toString()
    }

    /*-------------------*/
    /* TOUCHABLE METHODS */
    /*-------------------*/

    override fun isDraggable(): Boolean {
        return mDraggable
    }

    override fun setDraggable(draggable: Boolean) {
        mDraggable = draggable
    }

    override fun isSwipeable(): Boolean {
        return mSwipeable
    }

    override fun setSwipeable(swipeable: Boolean) {
        mSwipeable = swipeable
    }

    /*---------------------*/
    /* VIEW HOLDER METHODS */
    /*---------------------*/

    /**
     * {@inheritDoc}
     *
     * If not overridden return value is the same of [.getLayoutRes].
     */
    override fun getItemViewType(): Int {
        return layoutRes
    }

    /**
     * {@inheritDoc}
     */
    abstract override fun getLayoutRes(): Int

    /**
     * {@inheritDoc}
     */
    abstract override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<*>>): VH

    /**
     * {@inheritDoc}
     */
    abstract override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<*>>, holder: VH, position: Int, payloads: List<Any>)

    /**
     * {@inheritDoc}
     */
    override fun unbindViewHolder(adapter: FlexibleAdapter<IFlexible<*>>, holder: VH, position: Int) {}

    /**
     * {@inheritDoc}
     */
    override fun onViewAttached(adapter: FlexibleAdapter<IFlexible<*>>, holder: VH, position: Int) {}

    /**
     * {@inheritDoc}
     */
    override fun onViewDetached(adapter: FlexibleAdapter<IFlexible<*>>, holder: VH, position: Int) {}
}
