package no.none.runtrainer.ui.programdetails.flexibleadapter

import eu.davidea.flexibleadapter.FlexibleAdapter
import no.none.runtrainer.data.DataVOs

class SegmentFlexibleAdapter(items: List<AbstractSegmentItem<*>>?, listeners: Any?) : FlexibleAdapter<AbstractSegmentItem<*>>(items, listeners, true){

    var distanceCallback: ((Int) -> Unit)? = null
    var durationCallback: ((Int) -> Unit)? = null
    var speedCallback: ((Int) -> Unit)? = null
    var bpmCallback: ((Int) -> Unit)? = null
    var deleteCallback: ((Int) -> Unit)? = null


    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        collapseAll()
        var moved: Boolean = false
        // Check if the item that is moved is an instance of SegmentItem, otherwise just move it
        if(getItem(fromPosition) is SegmentItem){
            var item= getItem(fromPosition) as SegmentItem
            var target = getItem(toPosition)
            var itemParent:GroupSegmentItem? = (getExpandableOf(fromPosition) as GroupSegmentItem?)
            var targetParent:GroupSegmentItem? = (getExpandableOf(toPosition) as GroupSegmentItem?)
            if(targetParent!=null){
                targetParent.isExpanded = false
            }

            // Move sub segments inside same group
            if(itemParent!=null && itemParent==targetParent)
            {
                moved = super.onItemMove(fromPosition, toPosition)
            }
            // Move segment that is not part of a group and move it only if the targetParent is not a group
            else if(itemParent==null && targetParent==null/* && ((target is SegmentItem) || (toPosition==0 || toPosition==itemCount-1))*/){
                moved = super.onItemMove(fromPosition, toPosition)
            }
        }else{
            var target = getItem(toPosition)
            var targetParent:GroupSegmentItem? = (getExpandableOf(toPosition) as GroupSegmentItem?)
            // Move the group only if the target is a SegmentItem that is not part of a group
            if(targetParent==null){
                moved = super.onItemMove(fromPosition, toPosition)
            }
        }

        invalidateItemDecorations(0L)
        println("moved:$moved")
        return moved
    }

    val segmentData:ArrayList<DataVOs.Segment>
    get(){
        var list= arrayListOf<DataVOs.Segment>()
        for(i in 0..itemCount){
            var item = getItem(i)
            if (item != null) {
                list.add(item.segment)
                if(item is GroupSegmentItem && !item.isExpanded && item.hasSubItems()){
                    item.subItems?.forEach {
                        list.add(it.segment)
                    }
                }
            }
        }
        return list
    }


}
