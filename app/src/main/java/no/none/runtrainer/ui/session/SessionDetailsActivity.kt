package no.none.runtrainer.ui.session

import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.SyncStateContract.Helpers.update
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams
import kotlinx.android.synthetic.main.activity_session_details.*
import mu.KotlinLogging
import no.none.runtrainer.*
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.ProgramViewModel
import no.none.runtrainer.data.viewmodels.SensorDataViewModel
import no.none.runtrainer.data.viewmodels.SessionViewModel
import no.none.runtrainer.services.processors.ProgramProcessor
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.nield.kotlinstatistics.*


class SessionDetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {
        private const val INTENT_SESSION = "session"
        private const val INTENT_SESSION_ID = "session_id"
        private const val BUNDLE = "bundle"

        @JvmStatic
        fun startIntent(context: Context, sessionId: Int): Intent {
            return Intent(context, SessionDetailsActivity::class.java).apply {
                action = INTENT_SESSION
                var bundle = Bundle()
                bundle.putInt(INTENT_SESSION_ID, sessionId)
                putExtra(BUNDLE,bundle)
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    private lateinit var programViewModel: ProgramViewModel
    private lateinit var sessionViewModel: SessionViewModel
    private lateinit var sensorDataViewModel: SensorDataViewModel
    private lateinit var session:DataVOs.Session
    private lateinit var sessionProgram:DataVOs.Program
    private lateinit var sensorData:DataVOs.SensorData
    private var sessionId:Int = -1
    private var dataSets = ArrayList<ILineDataSet>()

    private lateinit var mMap: GoogleMap

    private val xAxisFormatter = IAxisValueFormatter { value, axis ->
        if (value > 0) {
            value.formatDurationToMMSSString()
        } else {
            "0:00"
        }
    }
    private val yAxisFormatter = IAxisValueFormatter { value, axis ->
        if (value > 0) {
            value.formatMperSecToMMSSString()
        } else {
            "0:00"
        }
    }
    private val speedFormatter = IValueFormatter { value, entry, dataSetIndex, viewPortHandler ->
        if (value > 0) {
            value.formatMperSecToMMSSString()
        } else {
            "0:00"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(no.none.runtrainer.R.layout.activity_session_details)

        logger.info ( "onCreate()" )

        programViewModel = ViewModelProviders.of(this).get(ProgramViewModel::class.java)
        sessionViewModel = ViewModelProviders.of(this).get(SessionViewModel::class.java)
        sensorDataViewModel = ViewModelProviders.of(this).get(SensorDataViewModel::class.java)

        val bundle:Bundle? = intent.getBundleExtra(BUNDLE)
        sessionId = bundle?.getInt(INTENT_SESSION_ID, -1)!!
        sessionViewModel.getSessionById(sessionId){session ->
            this.session = session!!
            logger.info { "session:${this.session.id}" }
        }

        sensorDataViewModel.getSensorDataBySessionId(sessionId){sensorData ->
            logger.info { "sensorData.id:${sensorData!!.id}, sensorData.sessionId:${sensorData.idSession}" }
            this.sensorData = sensorData!!
            setupUI()

            programViewModel.getProgramById(session.programUid){
                sessionProgram = it!!
//                simulateNotifications()
                if(sensorData.processorsNotifications.isNotEmpty()){
                    setupChartProcessedData(sensorData)
                }
            }
        }
    }

    private fun setupUI(){
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.title = session.title


        accuracySeekBar.setProgress(200f)
        accuracySeekBar.max = sensorData.gps.maxBy { it.accuracy }!!.accuracy
        accuracySeekBar.onSeekChangeListener = object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {}

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {
                logger.info ("Location accuracy precision:${seekBar.progress}")
                var gpsData = DataVOs.SensorData.filterLocationWithAccuracy(sensorData.gps, accuracySeekBar.progressFloat)
                updateMap(gpsData)
            }
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        setupChartGPSData()
    }

    private fun setupChartGPSData(){
        val entries = ArrayList<Entry>()

        val std = sensorData.gps.map { it.speed }.standardDeviation().toFloat()
        val median = sensorData.gps.map { it.speed }.median().toFloat()
        val lowerBound = median+std
        val upperBound = median-std

        /*logger.info { "std:$std, MM:SS:${std.formatMperSecToMMSSString()}" }
        logger.info { "median:$median, MM:SS:${median.formatMperSecToMMSSString()}" }
        logger.info { "upperBound:$upperBound, MM:SS:${upperBound.formatMperSecToMMSSString()}" }
        logger.info { "lowerBound:$lowerBound, MM:SS:${lowerBound.formatMperSecToMMSSString()}" }*/

        val firstTimestamp = sensorData.gps.first().time

        sensorData.gps/*.filter { it.speed>upperBound }*/.forEach {
            entries.add(Entry((it.time - firstTimestamp).formatDurationToMMSSFloat(), it.speed, it.time))
        }

        val dataSet = LineDataSet(entries, "GPS Speed")
        var color = ResourcesCompat.getColor(resources, R.color.colorMapPrimary, theme)
        dataSet.color = color
        dataSet.fillColor = color
        dataSet.setCircleColor(ResourcesCompat.getColor(resources, R.color.colorMapPrimary, theme))
        dataSet.setDrawCircles(true)
        dataSet.setDrawFilled(true)

        dataSets.add(dataSet)
        val lineData = LineData(dataSets)
        lineData.setValueFormatter(speedFormatter)

        chartSpeed.data = lineData
        chartSpeed.invalidate()


        val xAxis= chartSpeed.xAxis
        xAxis.valueFormatter = xAxisFormatter

        val left = chartSpeed.axisLeft
        left.valueFormatter = yAxisFormatter
        val right = chartSpeed.axisRight
        right.valueFormatter = yAxisFormatter
    }

    private fun setupChartProcessedData(sensorData:DataVOs.SensorData){
        val processorsNotifications = sensorData.processorsNotifications

        val entriesRaw = ArrayList<Entry>()
        val entries = ArrayList<Entry>()
        val entriesNotified = ArrayList<Entry>()

        val std = processorsNotifications.map { it.speed }.standardDeviation().toFloat()
        val median = processorsNotifications.map { it.speed }.median().toFloat()
        val lowerBound = median+std
        val upperBound = median-std

        /*logger.info { "notification std:$std, MM:SS:${std.formatMperSecToMMSSString()}" }
        logger.info { "notification median:$median, MM:SS:${median.formatMperSecToMMSSString()}" }
        logger.info { "notification upperBound:$upperBound, MM:SS:${upperBound.formatMperSecToMMSSString()}" }
        logger.info { "notification lowerBound:$lowerBound, MM:SS:${lowerBound.formatMperSecToMMSSString()}" }*/

        val firstTimestamp = processorsNotifications.first().timestamp

        processorsNotifications/*.filter { it.speed>upperBound }*/.forEach {
            entries.add(Entry((it.timestamp - firstTimestamp).formatDurationToMMSSFloat(), it.speed, it.timestamp))
            if(it.notified){
                entriesNotified.add(Entry((it.timestamp - firstTimestamp).formatDurationToMMSSFloat(), it.speed, it.timestamp))
            }
        }

        val speeds = sensorData.distances.zip(sensorData.durations){ distance, duration -> (distance*1000/(duration/1000)) }

        val firstTimestampGps = sensorData.gps.first().time
        for(i in 0 until speeds.size){
            entriesRaw.add(Entry((sensorData.gps[i].time - firstTimestampGps).formatDurationToMMSSFloat(), speeds[i], processorsNotifications[i].timestamp))
        }

        val dataSetRaw = LineDataSet(entriesRaw, "Raw pace")
        dataSetRaw.lineWidth = 1.5f
        dataSetRaw.color = ResourcesCompat.getColor(resources, R.color.colorPrimary, theme)
        dataSetRaw.fillColor = ResourcesCompat.getColor(resources, R.color.colorPrimary, theme)
        dataSetRaw.setCircleColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
        dataSetRaw.setDrawCircles(true)

        val dataSet = LineDataSet(entries, "Processor pace")
        dataSet.lineWidth = 1.5f
        dataSet.color = ResourcesCompat.getColor(resources, R.color.colorMapSecondary, theme)
        dataSet.fillColor = ResourcesCompat.getColor(resources, R.color.colorMapSecondary, theme)
        dataSet.setCircleColor(ResourcesCompat.getColor(resources, R.color.colorMapSecondary, theme))
        dataSet.setDrawCircles(true)


        val dataSetNotified = LineDataSet(entriesNotified, "Notified pace")
        dataSetNotified.mode = LineDataSet.Mode.CUBIC_BEZIER
        dataSetNotified.lineWidth = 3f
        dataSetNotified.color = ResourcesCompat.getColor(resources, R.color.colorMapTertiary, theme)
        dataSetNotified.fillColor = ResourcesCompat.getColor(resources, R.color.colorMapTertiary, theme)
        dataSetNotified.setCircleColor(ResourcesCompat.getColor(resources, R.color.colorMapTertiary, theme))
        dataSetNotified.setDrawCircles(true)
        dataSetNotified.circleRadius = 4f

        while(dataSets.size>1){
            dataSets.removeAt(dataSets.size-1)
        }

        dataSets.add(dataSetRaw)
        dataSets.add(dataSet)
        dataSets.add(dataSetNotified)
        val lineData = LineData(dataSets)
        lineData.setValueFormatter(speedFormatter)

        chartSpeed.data = lineData
        chartSpeed.invalidate()

        chartSpeed.setOnChartValueSelectedListener(object:OnChartValueSelectedListener {

            override fun onNothingSelected() {
                removeMarker()
            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                val timestamp = e!!.data as Long
                val gpsPoint = sensorData.gps.find { location -> location.time==timestamp }
                val processorNotification = sensorData.processorsNotifications.find { notification -> notification.timestamp==timestamp }

                if(gpsPoint!=null && processorNotification!=null){
                    logger.info { "gpsPoint speed:${gpsPoint.speed} accuracy:${gpsPoint.accuracy}, processorNotification speed:${processorNotification.speed}" }
                }
                if(gpsPoint!=null){
                    setMarker(LatLng(gpsPoint.latitude, gpsPoint.longitude))
                }
            }
        })
    }

    private fun simulateNotifications(){
        var programProcessor = ProgramProcessor(applicationContext)
        programProcessor.onlyProcess = true

        doAsync {
            programProcessor.onlyProcessRecordedSessionData(sensorData, sessionProgram)
            uiThread {
                    sensorData.processorsNotifications = programProcessor.processorsNotifications
                    setupChartProcessedData(sensorData)
            }
        }



    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_session_details, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // action with ID action_refresh was selected
            R.id.action_delete -> {
                sensorDataViewModel.deleteSensorData(sensorData)
                sessionViewModel.deleteSession(session)
                finish()
            }
            android.R.id.home -> {
                finish()
            }
            else -> {
                //
            }
        }

        return true
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState?.putParcelable("session_saved_instance", session)
        logger.info ("onSaveInstanceState")
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        session = savedInstanceState?.getParcelable("session_saved_instance")!!
        logger.info ( "onRestoreInstanceState")
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.uiSettings.isZoomControlsEnabled = true
        updateMap(
                sensorData.gps
        )
    }

    private var polyline: Polyline? = null
    private var kalmanizedPolyline: Polyline? = null

    private var initializeMapCamera:Boolean = true

    private fun updateMap(sessionData:ArrayList<Location>) {
        val sessionLatLngData = sessionData.toMapLatLng()
        logger.info ( "updateMap() GPS:${sessionLatLngData.size}")
        if (polyline != null) {
            polyline!!.points = sessionLatLngData
        } else {
            polyline = mMap.addPolyline(PolylineOptions().color(applicationContext.getColor(R.color.colorMapPrimary)).width(8f).addAll(sessionLatLngData).zIndex(1f))
        }

        if(initializeMapCamera) {
//            initializeMapCamera = false
            val builder = LatLngBounds.Builder()
            sessionLatLngData.forEach { point -> builder.include(point) }
            if(sessionLatLngData.size>2){
                val bounds = builder.build()

                val padding = 50
                val mWidth: Int = resources.displayMetrics.widthPixels
                val mHeight: Int = resources.displayMetrics.heightPixels
                try {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private var mapMarker:Marker? = null

    private fun setMarker(point:LatLng){
        removeMarker()

        val markerOptions = MarkerOptions().position(point).title("Selected position")
        mapMarker = mMap.addMarker(markerOptions)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(point))
    }

    private fun removeMarker(){
        mapMarker?.remove()
    }
}
