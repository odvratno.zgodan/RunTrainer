package no.none.runtrainer.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.activity_login.*
import mu.KotlinLogging
import no.none.runtrainer.MainActivity
import no.none.runtrainer.R
import no.none.runtrainer.R.id.sign_in_button
import no.none.runtrainer.data.viewmodels.UserModel


class LoginActivity : AppCompatActivity() {

    companion object {
        private const val RC_GOOGLE_SIGN_IN = 1
    }

    private val logger = KotlinLogging.logger {}

    private var userModel = UserModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        sign_in_button.setSize(SignInButton.SIZE_WIDE)
        sign_in_button.setOnClickListener {
            val signInIntent = userModel.getGoogleSignInClient(this).signInIntent
            startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                logger.info("onActivityResult() account:$account")
                userModel.signInWithGoogleCredential(account!!) {exception:Exception? ->
                    if(exception==null){
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }else{
                        Toast.makeText(this, "There was a problem while logging in." + exception.localizedMessage, Toast.LENGTH_LONG).show()
                    }
                }
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                logger.warn("Google sign in failed", e)
            }
        }
    }
}
