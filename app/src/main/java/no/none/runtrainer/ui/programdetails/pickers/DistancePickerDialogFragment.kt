package no.none.runtrainer.ui.programdetails.pickers

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.LinearLayoutCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import mu.KotlinLogging
import no.none.runtrainer.R


class DistancePickerDialogFragment: DialogFragment(), NumberPicker.OnValueChangeListener  {

    private val logger = KotlinLogging.logger {}

    interface DistancePickerDialogListener {
        fun valueChanged(newVal: Float)
        fun valueSet(newVal: Float)
    }
    private var listener: DistancePickerDialogListener? = null
    private var valueSetCallback: ((Float) -> Unit)? = null

    private val input = DistanceString()
    private var displayRow: View? = null
    private var durationView: View? = null
    private var kilometersView: TextView? = null
    private var metersView: TextView? = null
    private var displayViews: Array<TextView>? = null
    private var unitLabelViews: Array<TextView>? = null
    private var backspaceButton: ImageButton? = null
    private var clearButton: ImageButton? = null
    private var separatorView: View? = null
    private var numPad: View? = null
    private var numPadButtons: Array<Button>? = null
    private var numPadMeasureButton: Button? = null
    private var kilometersLabel: TextView? = null
    private var metersLabel: TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.number_numpad_picker_dialog, container,
                false)

        displayRow = rootView.findViewById(R.id.display_row) as RelativeLayout
        durationView = rootView.findViewById(R.id.duration_layout) as LinearLayoutCompat
        kilometersView = rootView.findViewById(R.id.kilometers_value) as TextView
        metersView = rootView.findViewById(R.id.meters_value) as TextView
        displayViews = arrayOf<TextView>(kilometersView!!, metersView!!)

        kilometersLabel = (rootView.findViewById(R.id.kilometers_label) as TextView)
        metersLabel = (rootView.findViewById(R.id.meters_label) as TextView)
        unitLabelViews = arrayOf<TextView>(kilometersLabel!!, metersLabel!!)

        backspaceButton = rootView.findViewById(R.id.backspace) as ImageButton
        clearButton = rootView.findViewById(R.id.clear) as ImageButton

        separatorView = rootView.findViewById(R.id.separator) as View

        numPad = rootView.findViewById(R.id.numPad) as LinearLayoutCompat
        numPadButtons = arrayOf<Button>(
                rootView.findViewById(R.id.numPad1) as AppCompatButton
                , rootView.findViewById(R.id.numPad2) as AppCompatButton
                , rootView.findViewById(R.id.numPad3) as AppCompatButton
                , rootView.findViewById(R.id.numPad4) as AppCompatButton
                , rootView.findViewById(R.id.numPad5) as AppCompatButton
                , rootView.findViewById(R.id.numPad6) as AppCompatButton
                , rootView.findViewById(R.id.numPad7) as AppCompatButton
                , rootView.findViewById(R.id.numPad8) as AppCompatButton
                , rootView.findViewById(R.id.numPad9) as AppCompatButton
                , rootView.findViewById(R.id.numPad0) as AppCompatButton
                , rootView.findViewById(R.id.numPad00) as AppCompatButton
                , rootView.findViewById(R.id.numPad000) as AppCompatButton
        )

        //
        // init actions
        //

        backspaceButton!!.setOnClickListener {
            onBackspace()
        }

        clearButton!!.setOnClickListener {
            onClear()
        }

        val okButton = rootView.findViewById(R.id.ok_button) as AppCompatButton
        val cancelButton = rootView.findViewById(R.id.cancel_button) as AppCompatButton

        okButton.setOnClickListener {
            listener?.valueSet(input.getDistance())
            valueSetCallback?.invoke(input.getDistance())
            this.dismiss()
        }

        cancelButton.setOnClickListener {
            this.dismiss()
        }

        val numberClickListener = View.OnClickListener { v -> onNumberClick((v as AppCompatButton).text) }

        for (button in numPadButtons!!) {
            button.setOnClickListener(numberClickListener)
        }

        val distance:Float? = savedInstanceState?.getFloat("input_distance")
            if(distance!=null){
                input.setDistance(distance)
            }

        //
        // init default value
        //
        updateHoursMinutesSeconds()

        return rootView
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putFloat("input_distance", input.getDistance())
    }

    override fun onValueChange(picker: NumberPicker?, oldVal: Int, newVal: Int) {
        //
    }

    fun setValue(value: Float){
        input.setDistance(value)
    }

    fun setListeners(listener: DistancePickerDialogListener){
        this.listener = listener
    }

    fun onSetListener(callback:(position: Float) -> Unit){
        valueSetCallback = callback
    }

    private fun updateHoursMinutesSeconds() {
        logger.info ("updateHoursMinutesSeconds() input:${input.inputString}")
        kilometersView!!.text = input.kilometersString
        metersView!!.text = input.metersString
        fireDurationChangeListener()
    }

    private fun fireDurationChangeListener() {
        listener?.valueChanged(input.getDistance())
    }

    private fun onNumberClick(digits: CharSequence) {
        input.pushNumber(digits)
        updateHoursMinutesSeconds()
    }

    private fun onBackspace() {
        input.popDigit()
        updateHoursMinutesSeconds()
    }

    private fun onClear() {
        input.clear()
        updateHoursMinutesSeconds()
    }

    private inner class DistanceString {


        private var maxDigits = 5
        private val input = StringBuilder(maxDigits)

        val kilometersString: String
            get() = when {
                input.toString().toInt()>=10000 -> input.substring(0, 2)
                input.toString().toInt()>=1000 -> input.substring(1, 2)
                else -> "0"
            }

        val metersString: String
            get() = when {
                input.toString().toInt()>=100 -> input.substring(2, 5)
                input.toString().toInt()>=10 -> "0" + input.substring(3, 5)
                else -> "00" + input.substring(4, 5)
            }

        val inputString: String
            get() = input.toString()

        init {
            padWithZeros()
        }

        fun pushNumber(digits: CharSequence) {
            for (i in 0 until digits.length)
                pushDigit(digits[i])
        }

        fun pushDigit(digit: Char) {
            if (!Character.isDigit(digit))
                throw IllegalArgumentException("Only numbers are allowed")
            logger.info ("pushDigit() digit:$digit")
            removeLeadingZeros()
            if (input.length < maxDigits && (input.isNotEmpty() || digit != '0')) {
                input.append(digit)
            }
            padWithZeros()
        }

        fun popDigit() {
            if (input.isNotEmpty())
                input.deleteCharAt(input.length - 1)
            padWithZeros()
        }

        fun clear() {
            input.setLength(0)
            padWithZeros()
        }

        fun getDistance(): Float {
            return (input.toString().toFloat()/1000)
        }

        fun setDistance(value: Float) {
            input.setLength(0)
            input.append((value*1000).toInt().toString())
            padWithZeros()
        }

        private fun removeLeadingZeros() {
            while (input.isNotEmpty() && input[0] == '0')
                input.deleteCharAt(0)
        }

        private fun padWithZeros() {
            while (input.length < maxDigits)
                input.insert(0, '0')
        }
    }

}