package no.none.runtrainer.ui.session

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.activity_session_recording.*
import mu.KotlinLogging
import no.none.runtrainer.*
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.SensorDataViewModel
import no.none.runtrainer.data.viewmodels.SessionViewModel
import no.none.runtrainer.services.RecordingService
import no.none.runtrainer.ui.settings.SettingsActivity
import java.util.*


class SessionRecordingActivity : AppCompatActivity(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ServiceConnection, RecordingService.Callbacks {

    companion object {
        private const val PERMISSION_REQUEST_LOCATION = 99
        private const val INTENT_NEW_SESSION = "new_session"
        private const val INTENT_NEW_SESSION_PROGRAM_UID = "program_uid"
        private const val INTENT_RESUME_SESSION = "resume_session"
        private const val INTENT_NEW_SESSION_ID = "session_id"
        private const val BUNDLE = "bundle"

        @JvmStatic
        fun startIntent(context: Context, programUid: String): Intent {
            return Intent(context, SessionRecordingActivity::class.java).apply {
                action = INTENT_NEW_SESSION
                var bundle = Bundle()
                bundle.putString(INTENT_NEW_SESSION_PROGRAM_UID, programUid)
                putExtra(BUNDLE,bundle)
            }
        }
        @JvmStatic
        fun resumeIntent(context: Context, sessionId: Int): Intent {
            return Intent(context, SessionRecordingActivity::class.java).apply {
                action = INTENT_RESUME_SESSION
                var bundle = Bundle()
                bundle.putInt(INTENT_NEW_SESSION_ID, sessionId)
                putExtra(BUNDLE,bundle)
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    private lateinit var sessionViewModel: SessionViewModel
    private lateinit var sensorDataViewModel: SensorDataViewModel
    private var sessionId:Int = -1
    private var programUid:String = ""

    private lateinit var mMap: GoogleMap
    private var mapZoomSet:Boolean = false
    private var polyline: Polyline? = null
    private var kalmanizedPolyline: Polyline? = null
    private var googleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null

    private var recordingService: RecordingService? = null
    private var isBound: Boolean = false
    private var isResumed: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session_recording)

        logger.info ( "onCreate()" )

        sessionViewModel = ViewModelProviders.of(this).get(SessionViewModel::class.java)
        sensorDataViewModel = ViewModelProviders.of(this).get(SensorDataViewModel::class.java)

        if(intent.action==INTENT_NEW_SESSION)
        {
            logger.info { "INTENT_NEW_SESSION" }
            val bundle:Bundle? = intent.getBundleExtra(BUNDLE)
            programUid = bundle?.getString(INTENT_NEW_SESSION_PROGRAM_UID, "")!!
        }
        else if(intent.action== INTENT_RESUME_SESSION)
        {
            logger.info { "INTENT_RESUME_SESSION" }
            val bundle:Bundle? = intent.getBundleExtra(BUNDLE)
            sessionId = bundle?.getInt(INTENT_NEW_SESSION_ID, -1)!!
            if(sessionId!=-1){
                resumeRecording()
            }
        }
        logger.info { "programUid:$programUid, sessionId:$sessionId" }
        sessionViewModel.getSessionBeignRecorded().observe(this, Observer {
            if(it!=null){
                logger.info { "1." }
                start_recording_btn.visibility = AppCompatButton.GONE
                stop_recording_btn.visibility = AppCompatButton.VISIBLE
            }else{
                logger.info { "3." }
                start_recording_btn.visibility = AppCompatButton.VISIBLE
                stop_recording_btn.visibility = AppCompatButton.GONE
            }
        })


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        start_recording_btn.setOnClickListener {
            startRecording()
            start_recording_btn.visibility = AppCompatButton.GONE
            stop_recording_btn.visibility = AppCompatButton.VISIBLE
        }

        stop_recording_btn.setOnClickListener {
            stopRecording()
            start_recording_btn.visibility = AppCompatButton.VISIBLE
            stop_recording_btn.visibility = AppCompatButton.GONE
        }

        start_mock_location_btn.setOnClickListener {
            val intent = Intent(this, RecordingService::class.java)
            intent.action = RecordingService.ACTION_START_MOCK_LOCATION
//            intent.putExtra(RecordingService.MOCK_LOCATION_FILE_PATH_EXTRA, "Evening_Run.gpx")
            intent.putExtra(RecordingService.MOCK_LOCATION_FILE_PATH_EXTRA, "Afternoon_Run.gpx")
//            intent.putExtra(RecordingService.MOCK_LOCATION_FILE_PATH_EXTRA, "Afternoon_Run_noise.gpx")
//            intent.putExtra(RecordingService.MOCK_LOCATION_FILE_PATH_EXTRA, "Afternoon_Run_noise_2.gpx")
//            intent.putExtra(RecordingService.MOCK_LOCATION_FILE_PATH_EXTRA, "Afternoon_Run_noise_3.gpx")
            this.startRecordingServiceWithIntent(intent)
        }

        stop_mock_location_btn.setOnClickListener {
            val intent = Intent(this, RecordingService::class.java)
            intent.action = RecordingService.ACTION_STOP_MOCK_LOCATION
            this.startRecordingServiceWithIntent(intent)
        }

        setMockButtons()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_session_recording, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // action with ID action_refresh was selected
            R.id.action_settings -> {
                startActivity(Intent(this@SessionRecordingActivity, SettingsActivity::class.java))
            }
            android.R.id.home -> {
                finish()
            }
            else -> {
                //
            }
        }

        return true
    }

    override fun onStart() {
        super.onStart()
        logger.info { "onStart()" }
        val intent = Intent(this, RecordingService::class.java)
        bindService(intent, this, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        logger.info { "onStop()" }
        if (isBound) {
            unbindService(this)
        }
        isResumed = false
    }

    override fun onPause() {
        super.onPause()
        logger.info { "onPause()" }

        if (googleApiClient != null) {
            LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)
        }
    }

    override fun onPostResume() {
        super.onPostResume()
        logger.info { "onPostResume()" }


        if (googleApiClient != null && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, locationCallback, null)
        }
        isResumed = true
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
//        outState?.putParcelable("session_saved_instance", session)
        logger.info ("onSaveInstanceState")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
//        session = savedInstanceState?.getParcelable("session_saved_instance")!!
        logger.info ( "onRestoreInstanceState")
    }

    private fun startRecording(){
        val intent = RecordingService.startRecordingIntent(this, programUid)
        intent.action = RecordingService.ACTION_START_RECORDING
        startRecordingServiceWithIntent(intent)
    }

    private fun resumeRecording(){
        if(sessionId!=-1){
            val intent = RecordingService.resumeRecordingIntent(this, sessionId)
            intent.action = RecordingService.ACTION_RESUME_RECORDING
            startRecordingServiceWithIntent(intent)
        }
    }

    private fun stopRecording(){
        val intent = Intent(this, RecordingService::class.java)
        intent.action = RecordingService.ACTION_STOP_RECORDING
        startRecordingServiceWithIntent(intent)
    }

    private fun setMockButtons(){
        if(!BuildConfig.DEBUG){
            start_mock_location_btn.visibility = AppCompatButton.GONE
            stop_mock_location_btn.visibility = AppCompatButton.GONE
        }else {
            if(isBound){
                if (recordingService!!.mockLocationRunning()) {
                    start_mock_location_btn.visibility = AppCompatButton.GONE
                    stop_mock_location_btn.visibility = AppCompatButton.VISIBLE
                } else {
                    start_mock_location_btn.visibility = AppCompatButton.VISIBLE
                    stop_mock_location_btn.visibility = AppCompatButton.GONE
                }
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient()
                mMap.isMyLocationEnabled = true
                googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                googleMap.uiSettings.isZoomControlsEnabled = true
                googleMap.uiSettings.isCompassEnabled = true
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST_LOCATION)
            }
        } else {
            buildGoogleApiClient()
            mMap.isMyLocationEnabled = true
            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            googleMap.uiSettings.isZoomControlsEnabled = true
            googleMap.uiSettings.isCompassEnabled = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                if (googleApiClient == null) {
                    buildGoogleApiClient()
                }
                mMap.isMyLocationEnabled = true
            }
        } else {
            Toast.makeText(this, "Please Accept Location Permission", Toast.LENGTH_LONG).show()
        }
    }

    private fun updateMap(sessionGpsData:ArrayList<Location>) {
        if(sessionGpsData.size>1) {
            if (polyline != null) {
                val subList = sessionGpsData.subList(polyline!!.points.size, sessionGpsData.size)
                var points = polyline!!.points
                points.addAll(subList.toMapLatLng())
                polyline!!.points = points
            } else {
                polyline = mMap.addPolyline(PolylineOptions().color(applicationContext.getColor(R.color.colorMapPrimary)).width(8f).addAll(sessionGpsData.toMapLatLng()))
            }
        }
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        googleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        googleApiClient!!.connect()
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, locationCallback, null)
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        //
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
            super.onLocationAvailability(locationAvailability)
            // This method can be called at any time so we shouldn't start or stop recoding from it.
            // If we do start/stop it from here the ongoing trip
            if (locationAvailability!!.isLocationAvailable) {
                //
            } else {
                //
            }
        }

        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            for (i in 0 until locationResult!!.locations.size) {
                onLocationChanged(locationResult.locations[i])
            }
        }
    }

    fun onLocationChanged(location: Location?) {
        val latLng = LatLng(location!!.latitude, location.longitude)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        if(!mapZoomSet){
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15f))
            mapZoomSet = true
        }
    }

    //---------------------- service code ----------------------//

    override fun onServiceDisconnected(p0: ComponentName?) {
        logger.info ("onServiceDisconnected()")
        isBound = false

    }

    override fun onServiceConnected(p0: ComponentName?, service: IBinder?) {
        logger.info ("onServiceConnected()")
        isBound = true
        val binder = service as RecordingService.LocalBinder
        recordingService = binder.serviceInstance
        recordingService!!.registerClient(this@SessionRecordingActivity)
        setMockButtons()
    }

    override fun recordingStarted() {
        logger.info ("recordingStarted()")
        start_recording_btn.visibility = AppCompatButton.GONE
        stop_recording_btn.visibility = AppCompatButton.VISIBLE
    }

    override fun recordingStopped() {
        logger.info ("recordingStopped()")
        start_recording_btn.visibility = AppCompatButton.VISIBLE
        stop_recording_btn.visibility = AppCompatButton.GONE

        val intent = Intent(this, RecordingService::class.java)
        intent.action = RecordingService.ACTION_STOP_SERVICE
        this.startRecordingServiceWithIntent(intent)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun updateClient(isRecording: Boolean) {
        logger.info ("updateClient() isRecording:$isRecording")
    }

    override fun mockLocationChanged(enabled: Boolean) {
        setMockButtons()
    }

    override fun dataUpdated(data: DataVOs.SensorData) {
        // Don't update the map from here because it's not lifcycle aware, better to use a ViewModel
        if(isResumed){
            updateMap(data.gps)
        }
    }
}
