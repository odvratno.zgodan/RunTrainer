package no.none.runtrainer.ui.main

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mu.KotlinLogging
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.ProgramViewModel
import no.none.runtrainer.ui.programdetails.ProgramDetailsActivity
import no.none.runtrainer.ui.programdetails.ProgramDetailsActivityOld
import no.none.runtrainer.ui.session.SessionRecordingActivity


class ProgramsFragment : Fragment() {

    private val logger = KotlinLogging.logger {}

    private lateinit var programModel: ProgramViewModel
    private var recyclerView: RecyclerView? = null
    private var sAdapter: ProgramItemRecyclerAdapter? = null
    private var listData = ArrayList<DataVOs.Program>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_programs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.programsRecyclerView)

        sAdapter = ProgramItemRecyclerAdapter(listData as List<DataVOs.Program>, object : ProgramItemRecyclerAdapter.ClickListener {

            override fun itemOnClick(v: View, position: Int) {
                startActivity(SessionRecordingActivity.startIntent(v.context, listData[position].uid), null)
            }

            override fun editButtonOnClick(v: View, position: Int) {
                startActivity(ProgramDetailsActivityOld.startIntent(v.context, listData[position]), null)
            }

            override fun deleteButtonOnClick(v: View, position: Int) {
                removeProgramData(position)
            }
        })

        val mLayoutManager = LinearLayoutManager(context)
        recyclerView!!.layoutManager = mLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = sAdapter

        programModel = ViewModelProviders.of(this).get(ProgramViewModel::class.java)
        programModel.getPrograms().observe(this, Observer { listResource ->
            var programs = listResource!!.data()
            for(i in 0..(programs.size-1)){
                logger.info ("programs[$i]: ${programs[i]}")
            }
            setData(programs as ArrayList<DataVOs.Program>)
        })
    }

    private fun setData(programs:ArrayList<DataVOs.Program>) {
        listData.clear()
        listData.addAll(programs)

        sAdapter!!.notifyDataSetChanged()
    }

    private fun removeProgramData(position:Int) {
        programModel.deleteProgram(listData[position])
    }

    override fun toString(): String {
        return "Programs"
    }
}
