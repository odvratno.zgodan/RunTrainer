package no.none.runtrainer.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mu.KotlinLogging
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.SessionViewModel
import no.none.runtrainer.ui.session.SessionDetailsActivity
import java.util.*


class SessionsFragment : Fragment() {

    private val logger = KotlinLogging.logger {}

    private lateinit var sessionViewModel: SessionViewModel
    private var recyclerView: RecyclerView? = null
    private var sAdapter: SessionItemRecyclerAdapter? = null
    private var listData = ArrayList<DataVOs.Session>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_sessions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.sessionsRecyclerView)
        sAdapter = SessionItemRecyclerAdapter(object : SessionItemRecyclerAdapter.ClickListener {

            override fun itemOnClick(v: View, sessionId: Int) {
                startActivity(SessionDetailsActivity.startIntent(v.context, sessionId), null)
            }
        })

        recyclerView!!.layoutManager = mLayoutManager
        recyclerView!!.adapter = sAdapter

        sessionViewModel = ViewModelProviders.of(this).get(SessionViewModel::class.java)
        sessionViewModel.getSessions().observe(this, Observer { sessions ->
            sAdapter!!.setData(sessions.reversed())
        })
    }

    private val mLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

    override fun toString(): String {
        return "Sessions"
    }
}
