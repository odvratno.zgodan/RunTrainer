package no.none.runtrainer.ui.programdetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.leinardi.android.speeddial.SpeedDialActionItem
import eu.davidea.flexibleadapter.FlexibleAdapter
import kotlinx.android.synthetic.main.activity_program_details.*
import kotlinx.android.synthetic.main.content_program_details.*
import mu.KotlinLogging
import no.none.runtrainer.MainActivity
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.ProgramModel
import no.none.runtrainer.ui.programdetails.flexibleadapter.*
import no.none.runtrainer.ui.programdetails.pickers.DistancePickerDialogFragment
import no.none.runtrainer.ui.programdetails.pickers.DurationPickerDialogFragment
import no.none.runtrainer.ui.programdetails.pickers.NumberPickerDialogFragment


class ProgramDetailsActivity : AppCompatActivity(),FlexibleAdapter.OnItemClickListener, FlexibleAdapter.OnItemLongClickListener {

    private val logger = KotlinLogging.logger {}

    companion object {
        private const val INTENT_PROGRAM = "program"
        private const val BUNDLE = "bundle"

        @JvmStatic
        fun startIntent(context: Context, program: DataVOs.Program): Intent {
            return Intent(context, ProgramDetailsActivity::class.java).apply {
                var bundle = Bundle()
                bundle.putParcelable(INTENT_PROGRAM, program)
                putExtra(BUNDLE,bundle)
            }
        }
    }

    private val programsModel: ProgramModel = ProgramModel()
    private lateinit var program:DataVOs.Program

    private var recyclerView: RecyclerView? = null

    private lateinit var adapter:SegmentFlexibleAdapter
    private val myItems = arrayListOf<AbstractSegmentItem<*>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(no.none.runtrainer.R.layout.activity_program_details)

        val bundle = intent.getBundleExtra(BUNDLE)
        program = bundle.getParcelable<DataVOs.Program>(INTENT_PROGRAM) as DataVOs.Program
        requireNotNull(program) { "no program provided in Intent extras" }

        setupUI()

    }

    private fun setupUI(){
        recyclerView = findViewById(no.none.runtrainer.R.id.programsRecyclerView)

        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView!!.layoutManager = mLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        // Optional but strongly recommended: Compose the initial list

        val groupSegments = program.segments.filter { it.segmentType==DataVOs.SegmentType.GROUP }

        program.segments.forEach { segment ->
            if (groupSegments.contains(segment)){
                var groupSegmentItem = GroupSegmentItem(segment)
                var counter=1
                program.segments.filter { it.parentSegmentUid==segment.uid }.forEach {
                    groupSegmentItem.addSubItem(SegmentItem(it))
                    counter++
                }
                groupSegmentItem.isExpanded = true
                myItems.add(groupSegmentItem)
            }else if(segment.parentSegmentUid==""){
                myItems.add(SegmentItem(segment))
            }
        }

        // Initialize the Adapter
        adapter = SegmentFlexibleAdapter(myItems, this)
        adapter.addListener(this)
        adapter.expandItemsAtStartUp()
                .setAutoCollapseOnExpand(false)
                .setAutoScrollOnExpand(true)
                .setAnimateToLimit(Integer.MAX_VALUE) //Size limit = MAX_VALUE will always animate the changes
                .setNotifyMoveOfFilteredItems(true) //When true, filtering on big list is very slow!
                .setAnimationOnReverseScrolling(true)


        recyclerView!!.adapter = adapter
        recyclerView!!.setHasFixedSize(true)

        adapter.setLongPressDragEnabled(true) //Enable long press to drag items
                .setHandleDragEnabled(true) //Enable handle drag


        recyclerView!!.addItemDecoration(
                SubItemDecoration(applicationContext)
        )

        adapter.distanceCallback = {position ->
            val np = DistancePickerDialogFragment()
            np.onSetListener { distance ->
                adapter.getItem(position)!!.segment.distance = distance
                adapter.notifyItemChanged(position)
            }
            np.setValue(program.segments[position].distance)
            np.show(supportFragmentManager, "dialog")
        }

        adapter.durationCallback = {position ->
            val dp = DurationPickerDialogFragment()
            dp.onSetListener { duration ->
                adapter.getItem(position)!!.segment.duration = duration
                adapter.notifyItemChanged(position)
            }
            dp.setInitialDuration(program.segments[position].duration)
            dp.show(supportFragmentManager, "dialog")
        }

        adapter.speedCallback = {position ->
            val dp = DurationPickerDialogFragment()
            dp.onSetListener { duration ->
                adapter.getItem(position)!!.segment.speedMillisecondsPerKm = duration
                adapter.notifyItemChanged(position)
            }
            dp.setInitialDuration(program.segments[position].speedMillisecondsPerKm)
            dp.show(supportFragmentManager, "dialog")
        }

        adapter.bpmCallback = {position ->
            val np = NumberPickerDialogFragment()
            np.onSetListener {bpm ->
                adapter.getItem(position)!!.segment.bpm = bpm
                adapter.notifyItemChanged(position)
            }
            np.setValue(program.segments[position].bpm)
            np.setTitle("BPM")
            np.show(supportFragmentManager, "dialog")
        }

        adapter.deleteCallback = {position ->
            removeSegmentData(position)
        }

        program_name_text_edit.setText(program.name)
        program_name_text_edit.clearFocus()

        program_name_text_edit.setOnFocusChangeListener { view, hasFocus ->
            if (view != null) {
                if (!hasFocus){
                    println("hasFocus:$hasFocus")
                    val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    if (imm.isActive)
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                }
            }
        }

        program_name_text_edit.clearFocus()

        loop_switch.isChecked = program.loop
        loop_switch.setOnClickListener {
            program.loop = loop_switch.isChecked
        }

        DataVOs.SegmentType.values().forEach {
            speedDial.addActionItem(
                    SpeedDialActionItem.Builder(it.type, no.none.runtrainer.R.drawable.ic_menu_camera)
                            .setLabelBackgroundColor(ResourcesCompat.getColor(resources, no.none.runtrainer.R.color.colorPrimary, theme))
                            .setFabImageTintColor(ResourcesCompat.getColor(resources, no.none.runtrainer.R.color.colorAccent, theme))
                            .setFabBackgroundColor(ResourcesCompat.getColor(resources, no.none.runtrainer.R.color.colorPrimary, theme))
                            .setLabelColor(Color.WHITE)
                            .setLabel("${it.name.capitalize()} segment")
                            .create())
        }

        speedDial.setOnActionSelectedListener { speedDialActionItem ->
            when (speedDialActionItem.id) {
                DataVOs.SegmentType.DISTANCE.type -> {
                    val segment = addSegmentData(DataVOs.SegmentType.DISTANCE, "")
                    adapter.addItem(SegmentItem(segment))
                    adapter.notifyDataSetChanged()
                    false // true to keep the Speed Dial open
                }
                DataVOs.SegmentType.DURATION.type -> {
                    val segment = addSegmentData(DataVOs.SegmentType.DURATION, "")
                    adapter.addItem(SegmentItem(segment))
                    adapter.notifyDataSetChanged()
                    false // true to keep the Speed Dial open
                }
                DataVOs.SegmentType.POWER.type -> {
                    Toast.makeText(this, "${DataVOs.SegmentType.POWER.name} segment not implemented!", Toast.LENGTH_SHORT).show()
//                   val segment = addSegmentData(DataVOs.SegmentType.POWER, "")
                    false // true to keep the Speed Dial open
                }
                DataVOs.SegmentType.GROUP.type -> {
                    val segment = addSegmentData(DataVOs.SegmentType.GROUP, "")
                    adapter.addItem(GroupSegmentItem(segment))
                    adapter.notifyDataSetChanged()
                    false // true to keep the Speed Dial open
                }
                else -> false
            }
        }

        speedDial.useReverseAnimationOnClose = true
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        return true
    }

    override fun onItemLongClick(position: Int) {

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(no.none.runtrainer.R.menu.menu_program_details, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // action with ID action_refresh was selected
            no.none.runtrainer.R.id.action_save -> {
                saveProgram()
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
            android.R.id.home -> {
                println("adapter.segmentData.size:${adapter.segmentData.size}")
                adapter.segmentData.forEach {
                    println("name:${it.title}")
                }
                finish()
            }
            else -> {
                //
            }
        }

        return true
    }

    private fun saveProgram(){
        logger.info ("program?.uid: ${program.uid}")
        if(program_name_text_edit.text.toString()=="")
        {
            program.name = resources.getString(no.none.runtrainer.R.string.default_program_name)
        }
        else
        {
            program.name = program_name_text_edit.text.toString()
        }
        program.segments.clear()
        program.segments.addAll(adapter.segmentData)
        programsModel.setProgram(program)
    }


    private fun addSegmentData(segmentType:DataVOs.SegmentType, parentSegmentUid:String):DataVOs.Segment {
        var segment = DataVOs.Segment(segmentType)
        if(program.segments.size>0){
            segment.distance = program.segments.last().distance
            segment.duration = program.segments.last().duration
            segment.power = program.segments.last().power
            segment.speedMillisecondsPerKm = program.segments.last().speedMillisecondsPerKm
            segment.stepType = program.segments.last().stepType
            segment.bpm = program.segments.last().bpm
        }
        val count = program.segments.count {
            it.segmentType == segmentType
        }
        segment.title =  segment.segmentType.toString().capitalize() + " segment " + (count+1)
        segment.parentSegmentUid = parentSegmentUid
        program.segments.add(segment)
        return segment
    }

    private fun removeSegmentData(position:Int) {
        program.segments.removeAt(position)
        adapter.removeItem(position)
        adapter.notifyItemRemoved(position)
    }

}
