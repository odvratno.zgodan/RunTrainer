package no.none.runtrainer.ui.programdetails.flexibleadapter

import no.none.runtrainer.data.DataVOs

interface ISegmentViewHolder {
    fun bind(segment:DataVOs.Segment)
}