package no.none.runtrainer.ui.main

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs


class ProgramItemRecyclerAdapter(private val programsList: List<DataVOs.Program>, val listener: ClickListener) : RecyclerView.Adapter<ProgramItemRecyclerAdapter.MyViewHolder>() {

    interface ClickListener {
        fun itemOnClick(v: View, position: Int)
        fun editButtonOnClick(v: View, position: Int)
        fun deleteButtonOnClick(v: View, position: Int)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.title)
        var distance: TextView = view.findViewById(R.id.distance)
        var startActivityBtn: ImageButton = view.findViewById(R.id.edit_activity_btn)
        var deleteBtn: ImageButton = view.findViewById(R.id.delete_btn)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_program, parent, false)

        val holder = MyViewHolder(itemView)

        itemView.setOnClickListener { v ->
            listener.itemOnClick(v, holder.adapterPosition)
        }

        holder.startActivityBtn.setOnClickListener{ v ->
            listener.editButtonOnClick(v, holder.adapterPosition)
        }

        holder.deleteBtn.setOnClickListener { v ->
            listener.deleteButtonOnClick(v, holder.adapterPosition)
        }



        return holder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val program = programsList[position]
        holder.title.text = program.name
        var dist = 0.0f
        for(i in 0 until program.segments.size){
            dist += program.segments[i].distance
        }
        val distString = "distance $dist Km"
        holder.distance.text = distString
    }

    override fun getItemCount(): Int {
        return programsList.size
    }

}