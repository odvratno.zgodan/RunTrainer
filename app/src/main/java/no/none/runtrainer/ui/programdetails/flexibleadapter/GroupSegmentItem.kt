package no.none.runtrainer.ui.programdetails.flexibleadapter

import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.Payload
import eu.davidea.flexibleadapter.items.IExpandable
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.items.IHeader
import eu.davidea.viewholders.ExpandableViewHolder
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.formatDurationToSpeechMMSSString


class GroupSegmentItem(override var segment: DataVOs.Segment):AbstractSegmentItem<GroupSegmentItem.GroupSegmentViewHolder>(), IExpandable<GroupSegmentItem.GroupSegmentViewHolder, SegmentItem>, IHeader<GroupSegmentItem.GroupSegmentViewHolder> {

    private var mExpanded = false
    private var mSubItems: ArrayList<SegmentItem>? = null

    init {
        isDraggable = true
        //We start with header shown and expanded
        isHidden = false
        isExpanded = true
        //NOT selectable (otherwise ActionMode will be activated on long click)!
        isSelectable = false
    }

    override fun isExpanded(): Boolean {
        return mExpanded
    }

    override fun setExpanded(expanded: Boolean) {
        mExpanded = expanded
    }

    override fun getExpansionLevel(): Int {
        return 0
    }

    override fun getSubItems(): List<SegmentItem>? {
        return mSubItems
    }

    fun hasSubItems(): Boolean {
        return mSubItems != null && (mSubItems!!.size > 0)
    }

    fun removeSubItem(item: SegmentItem?): Boolean {
        return item != null && mSubItems!!.remove(item)
    }

    fun removeSubItem(position: Int): Boolean {
        if (mSubItems != null && position >= 0 && position < mSubItems!!.size) {
            mSubItems!!.removeAt(position)
            return true
        }
        return false
    }

    fun addSubItem(subItem: SegmentItem) {
        if (mSubItems == null)
            mSubItems = ArrayList()
        mSubItems?.add(subItem)
    }

    fun addSubItem(position: Int, subItem: SegmentItem) {
        if (mSubItems != null && position >= 0 && position < mSubItems!!.size) {
            mSubItems!!.add(position, subItem)
        } else
            addSubItem(subItem)
    }

    override fun equals(o: Any?): Boolean {
        if (o is GroupSegmentItem) {
            val inItem = o as GroupSegmentItem?
            return this.segment.uid == inItem!!.segment.uid
        }
        return false
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<*>>): GroupSegmentViewHolder {
        return GroupSegmentViewHolder(view, adapter)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<*>>, holder: GroupSegmentViewHolder, position: Int, payloads: List<Any>) {
        holder.bind(segment)
    }

    override fun getLayoutRes(): Int {
        return R.layout.list_item_program_details_group_segment
    }

    /**
     * The ViewHolder used by this item.
     * Extending from FlexibleViewHolder is recommended especially when you will use
     * more advanced features.
     */
    class GroupSegmentViewHolder(private val view: View, adapter: FlexibleAdapter<*>) : ExpandableViewHolder(view, adapter, true), ISegmentViewHolder {

        lateinit var title: TextView
        lateinit var distance: EditText
        lateinit var duration: EditText
        lateinit var deleteBtn: ImageButton

        override fun bind(segment: DataVOs.Segment) {
            title = view.findViewById(R.id.title)
            distance = view.findViewById(no.none.runtrainer.R.id.distance)
            duration = view.findViewById(R.id.duration)
            deleteBtn = view.findViewById(no.none.runtrainer.R.id.delete_btn)


            title.text = segment.title

            val distanceStr =  "${segment.distance} Km"
            distance.setText(distanceStr)
            val durationStr = segment.duration.formatDurationToSpeechMMSSString()
            duration.setText(durationStr)
        }

        /**
         * Allows to expand or collapse child views of this itemView when [View.OnClickListener]
         * event occurs on the entire view.
         *
         * This method returns always true; Extend with "return false" to Not expand or collapse
         * this ItemView onClick events.
         *
         * @return always true, if not overridden
         * @since 5.0.0-b1
         */
        override fun isViewExpandableOnClick(): Boolean {
            return true//default=true
        }

        /**
         * Allows to collapse child views of this ItemView when [View.OnClickListener]
         * event occurs on the entire view.
         *
         * This method returns always true; Extend with "return false" to Not collapse this
         * ItemView onClick events.
         *
         * @return always true, if not overridden
         * @since 5.0.4
         */
        override fun isViewCollapsibleOnClick(): Boolean {
            return true//default=true
        }

        /**
         * Allows to collapse child views of this ItemView when [View.OnLongClickListener]
         * event occurs on the entire view.
         *
         * This method returns always true; Extend with "return false" to Not collapse this
         * ItemView onLongClick events.
         *
         * @return always true, if not overridden
         * @since 5.0.0-b1
         */
        override fun isViewCollapsibleOnLongClick(): Boolean {
            return true//default=true
        }

        /**
         * Allows to notify change and rebound this itemView on expanding and collapsing events,
         * in order to update the content (so, user can decide to display the current expanding status).
         *
         * This method returns always false; Override with `"return true"` to trigger the
         * notification.
         *
         * @return true to rebound the content of this itemView on expanding and collapsing events,
         * false to ignore the events
         * @see .expandView
         * @see .collapseView
         * @since 5.0.0-rc1
         */
        override fun shouldNotifyParentOnClick(): Boolean {
            return true//default=false
        }

        /**
         * Expands or Collapses based on the current state.
         *
         * @see .shouldNotifyParentOnClick
         * @see .expandView
         * @see .collapseView
         * @since 5.0.0-b1
         */
        override fun toggleExpansion() {
            super.toggleExpansion() //If overridden, you must call the super method
        }

        /**
         * Triggers expansion of this itemView.
         *
         * If [.shouldNotifyParentOnClick] returns `true`, this view is rebound
         * with payload [Payload.EXPANDED].
         *
         * @see .shouldNotifyParentOnClick
         * @since 5.0.0-b1
         */
        override fun expandView(position: Int) {
            super.expandView(position) //If overridden, you must call the super method
            // Let's notify the item has been expanded. Note: from 5.0.0-rc1 the next line becomes
            // obsolete, override the new method shouldNotifyParentOnClick() as showcased here
            //if (mAdapter.isExpanded(position)) mAdapter.notifyItemChanged(position, true);
        }

        /**
         * Triggers collapse of this itemView.
         *
         * If [.shouldNotifyParentOnClick] returns `true`, this view is rebound
         * with payload [Payload.COLLAPSED].
         *
         * @see .shouldNotifyParentOnClick
         * @since 5.0.0-b1
         */
        override fun collapseView(position: Int) {
            super.collapseView(position) //If overridden, you must call the super method
            // Let's notify the item has been collapsed. Note: from 5.0.0-rc1 the next line becomes
            // obsolete, override the new method shouldNotifyParentOnClick() as showcased here
            //if (!mAdapter.isExpanded(position)) mAdapter.notifyItemChanged(position, true);
        }


    }
}