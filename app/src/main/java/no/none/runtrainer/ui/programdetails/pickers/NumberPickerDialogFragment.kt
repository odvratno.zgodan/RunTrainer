package no.none.runtrainer.ui.programdetails.pickers

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.widget.AppCompatButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import android.widget.TextView
import no.none.runtrainer.R


class NumberPickerDialogFragment: DialogFragment(),NumberPicker.OnValueChangeListener {

    interface NumberPickerDialogListener {
        fun valueChanged(oldVal: Int, newVal: Int)
        fun valueSet(newVal: Int)
    }

    private var listener: NumberPickerDialogListener? = null
    private var valueSetCallback: ((Int) -> Unit)? = null
    private var np:NumberPicker? = null
    private var title:TextView? = null

    private var pickerMinValue:Int = 60
    private var pickerMaxValue:Int = 90
    private var pickerValue:Int = 90
    private var titleValue:String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.number_picker_dialog, container,
                false)

        val title = rootView.findViewById(R.id.title) as TextView
        title.text = titleValue

        val numberPicker = rootView.findViewById(R.id.number_picker) as NumberPicker
        numberPicker.minValue = pickerMinValue
        numberPicker.maxValue = pickerMaxValue
        numberPicker.value = pickerValue
        numberPicker.setOnValueChangedListener(this)

        val okButton = rootView.findViewById(R.id.ok_button) as AppCompatButton
        val cancelButton = rootView.findViewById(R.id.cancel_button) as AppCompatButton

        okButton.setOnClickListener {
            listener?.valueSet(numberPicker.value)
            valueSetCallback?.invoke(numberPicker.value)
            this.dismiss()
        }

        cancelButton.setOnClickListener {
            this.dismiss()
        }

        return rootView
    }

    override fun onValueChange(picker: NumberPicker?, oldVal: Int, newVal: Int) {
        listener?.valueChanged(oldVal, newVal)
    }

    fun setListeners(listener: NumberPickerDialogListener){
        this.listener = listener
    }

    fun onSetListener(callback:(position: Int) -> Unit){
        valueSetCallback = callback
    }

    fun setPickerMinMax(minValue: Int, maxValue: Int){
        pickerMinValue = minValue
        pickerMaxValue = maxValue
        if(np!=null) {
            np!!.minValue = pickerMinValue
            np!!.maxValue = pickerMaxValue
        }
    }

    fun setValue(value: Int){
        pickerValue = value
        if(np!=null){
            np!!.value = pickerValue
        }
    }
    fun setTitle(value: String){
        titleValue = value
        if(title!=null){
            title!!.text = titleValue
        }
    }


}