package no.none.runtrainer.ui.databaseutil

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_database_util.*
import no.none.runtrainer.MainActivity
import no.none.runtrainer.data.AppDatabase
import androidx.documentfile.provider.DocumentFile






class DatabaseUtilActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(no.none.runtrainer.R.layout.activity_database_util)

        ActivityCompat.requestPermissions(this@DatabaseUtilActivity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                100)

        exportBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
//            intent.type = "file/*"
            startActivityForResult(intent, 1)
        }

        importBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            startActivityForResult(intent, 2)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==1 && resultCode==RESULT_OK){

            val userChosenUri = data!!.data
            println("userChosenUri:$userChosenUri")
            exportDB(userChosenUri!!)
        }
        else if(requestCode==2 && resultCode==RESULT_OK){

            val userChosenUri = data!!.data
            println("userChosenUri:$userChosenUri")
            importDB(userChosenUri!!)
        }
    }

    //importing database
    private fun importDB(uri:Uri) {
        AppDatabase.destroyInstance()

        val inStream = contentResolver.openInputStream(uri)
        val outStream = getDatabasePath("app-db").outputStream()

        inStream.use { input ->
            outStream.use { output ->
                input!!.copyTo(output)
            }
        }

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    //exporting database
    private fun exportDB(uri:Uri) {

        val selectedDir = DocumentFile.fromTreeUri(this, uri)
        println("documentFile!!.uri:${selectedDir!!.uri}")
        if(selectedDir.isDirectory){
            val newFile = selectedDir.createFile("application/octet-stream", "runtrainer-app-backup.db")
            val inStream = getDatabasePath("app-db").inputStream()
            val outStream = contentResolver.openOutputStream(newFile!!.uri)

            inStream.use { input ->
                outStream.use { output ->
                    input.copyTo(output)
                }
            }

            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

}
