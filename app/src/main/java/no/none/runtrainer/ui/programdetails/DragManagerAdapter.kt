package no.none.runtrainer.ui.programdetails

import android.content.Context
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class  DragManagerAdapter(adapter: SegmentsExpandableRecyclerAdapter, dragDirs: Int, swipeDirs: Int) : ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs) {

    private var segmentAdapter = adapter

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        segmentAdapter.swapItems(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if(actionState==ItemTouchHelper.ACTION_STATE_DRAG && viewHolder!=null){
            segmentAdapter.dragStarted(viewHolder.adapterPosition)
        }
        else if(actionState==ItemTouchHelper.ACTION_STATE_IDLE){
            segmentAdapter.dragEnded()
        }
        super.onSelectedChanged(viewHolder, actionState)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        segmentAdapter.deleteItem(viewHolder.adapterPosition)
    }
}