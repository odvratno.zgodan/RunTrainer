package no.none.runtrainer.ui.programdetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.*
import com.leinardi.android.speeddial.SpeedDialActionItem
import kotlinx.android.synthetic.main.activity_program_details.*
import kotlinx.android.synthetic.main.content_program_details.*
import mu.KotlinLogging
import no.none.runtrainer.DpToPx
import no.none.runtrainer.MainActivity
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.data.viewmodels.ProgramModel
import no.none.runtrainer.ui.programdetails.pickers.DistancePickerDialogFragment
import no.none.runtrainer.ui.programdetails.pickers.DurationPickerDialogFragment
import no.none.runtrainer.ui.programdetails.pickers.NumberPickerDialogFragment


class ProgramDetailsActivityOld : AppCompatActivity() {

    private val logger = KotlinLogging.logger {}

    companion object {
        private const val INTENT_PROGRAM = "program"
        private const val BUNDLE = "bundle"

        @JvmStatic
        fun startIntent(context: Context, program: DataVOs.Program): Intent {
            return Intent(context, ProgramDetailsActivityOld::class.java).apply {
                var bundle = Bundle()
                bundle.putParcelable(INTENT_PROGRAM, program)
                putExtra(BUNDLE,bundle)
            }
        }
    }

    private val programsModel: ProgramModel = ProgramModel()
    private lateinit var program:DataVOs.Program

    private var segmentsList = ArrayList<DataVOs.Segment>()
    private var recyclerView: RecyclerView? = null
    private var sAdapter: SegmentsExpandableRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_program_details)

        val bundle = intent.getBundleExtra(BUNDLE)
        program = bundle.getParcelable<DataVOs.Program>(INTENT_PROGRAM) as DataVOs.Program
        requireNotNull(program) { "no program provided in Intent extras" }

        recyclerView = findViewById<RecyclerView>(R.id.programsRecyclerView)


        sAdapter = SegmentsExpandableRecyclerAdapter(program.segments)
        logger.info { "sAdapter!!.itemCount:${sAdapter!!.itemCount}" }

        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView!!.layoutManager = mLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = sAdapter

        val dividerItemDecoration = SegmentsExpandableRecyclerAdapter.SubItemDecoration(this , mLayoutManager.orientation, 20.DpToPx(applicationContext))
        recyclerView!!.addItemDecoration(dividerItemDecoration)

        // Drag & drop code
        val callback = DragManagerAdapter(sAdapter!!, 
                ItemTouchHelper.UP.or(ItemTouchHelper.DOWN), ItemTouchHelper.LEFT.or(ItemTouchHelper.RIGHT))
        val helper = ItemTouchHelper(callback)
        helper.attachToRecyclerView(recyclerView)

        sAdapter!!.setDistanceOnClick {position ->
            val np = DistancePickerDialogFragment()
            np.onSetListener { distance ->
                program.segments[position].distance = distance
                sAdapter!!.notifyDataSetChanged()
            }
            np.setValue(program.segments[position].distance)
            np.show(supportFragmentManager, "dialog")
        }

        sAdapter!!.setDurationOnClick {position ->
            val dp = DurationPickerDialogFragment()
            dp.onSetListener { duration ->
                program.segments[position].duration = duration
                sAdapter!!.notifyDataSetChanged()
            }
            dp.setInitialDuration(program.segments[position].duration)
            dp.show(supportFragmentManager, "dialog")
        }

        sAdapter!!.setSpeedOnClick {position ->
            val dp = DurationPickerDialogFragment()
            dp.onSetListener { duration ->
                program.segments[position].speedMillisecondsPerKm = duration
                sAdapter!!.notifyDataSetChanged()
            }
            dp.setInitialDuration(program.segments[position].speedMillisecondsPerKm)
            dp.show(supportFragmentManager, "dialog")
        }

        sAdapter!!.setBpmOnClick { position ->
            val np = NumberPickerDialogFragment()
            np.onSetListener {bpm ->
                program.segments[position].bpm = bpm
                sAdapter!!.notifyDataSetChanged()
            }
            np.setValue(program.segments[position].bpm)
            np.setTitle("BPM")
            np.show(supportFragmentManager, "dialog")
        }

        sAdapter!!.setDeleteOnClick {position ->
            removeSegmentData(position)
        }

        program_name_text_edit.setText(program.name)
        program_name_text_edit.clearFocus()

        program_name_text_edit.setOnFocusChangeListener { view, hasFocus ->
            if (view != null) {
                if (!hasFocus){
                    println("hasFocus:$hasFocus")
                    val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    if (imm.isActive)
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                }
            }
        }

        program_name_text_edit.clearFocus()

        loop_switch.isChecked = program.loop
        loop_switch.setOnClickListener {
            program.loop = loop_switch.isChecked
        }

        DataVOs.SegmentType.values().forEach {
            speedDial.addActionItem(
                    SpeedDialActionItem.Builder(it.type, R.drawable.ic_menu_camera)
                            .setLabelBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
                            .setFabImageTintColor(ResourcesCompat.getColor(resources, R.color.colorAccent, theme))
                            .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
                            .setLabelColor(Color.WHITE)
                            .setLabel("${it.name.capitalize()} segment")
                            .create())
        }

        speedDial.setOnActionSelectedListener { speedDialActionItem ->
            when (speedDialActionItem.id) {
                DataVOs.SegmentType.DISTANCE.type -> {
                    addSegmentData(DataVOs.SegmentType.DISTANCE)
                    false // true to keep the Speed Dial open
                }
                DataVOs.SegmentType.DURATION.type -> {
                    addSegmentData(DataVOs.SegmentType.DURATION)
                    false // true to keep the Speed Dial open
                }
                DataVOs.SegmentType.GROUP.type -> {
                    addSegmentData(DataVOs.SegmentType.GROUP)
                    false // true to keep the Speed Dial open
                }
                DataVOs.SegmentType.POWER.type -> {
                    // TODO: Enable this when PowerSegmentProcessor is implemented
                    Toast.makeText(this, "${DataVOs.SegmentType.POWER.name} segment not implemented!", Toast.LENGTH_SHORT).show()
                    addSegmentData(DataVOs.SegmentType.POWER)
                    false // true to keep the Speed Dial open
                }
                else -> false
            }
        }

        speedDial.useReverseAnimationOnClose = true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_program_details, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // action with ID action_refresh was selected
            R.id.action_save -> {
                saveProgram()
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
            android.R.id.home -> {
                finish()
            }
            else -> {
                //
            }
        }

        return true
    }

    private fun saveProgram(){
        logger.info ("program?.uid: ${program.uid}")
        if(program_name_text_edit.text.toString()=="")
        {
            program.name = resources.getString(R.string.default_program_name)
        }
        else
        {
            program.name = program_name_text_edit.text.toString()
        }
        programsModel.setProgram(program)
    }


    private fun addSegmentData(segmentType:DataVOs.SegmentType) {
        var segment = DataVOs.Segment(segmentType)
        if(program.segments.size>0){
            segment.distance = program.segments.last().distance
            segment.duration = program.segments.last().duration
            segment.power = program.segments.last().power
            segment.speedMillisecondsPerKm = program.segments.last().speedMillisecondsPerKm
            segment.stepType = program.segments.last().stepType
            segment.bpm = program.segments.last().bpm
        }
        val count = program.segments.count {
            it.segmentType == segmentType
        }
        segment.title =  segment.segmentType.toString().capitalize() + " segment " + (count+1)
        program.segments.add(segment)

        sAdapter!!.notifyDataSetChanged()
    }

    private fun removeSegmentData(position:Int) {
        val parentUid=program.segments[position].uid
        program.segments.removeAt(position)
        var list=program.segments.filter { it.parentSegmentUid==parentUid }
        program.segments.removeAll(list)
        /*var i=0
        while(i<program.segments.size){
            if(program.segments[i].parentSegmentUid==parentUid){
                println("title:${program.segments[i].title}")
                program.segments.removeAt(i)
            }else{
                i++
            }
        }*/
        sAdapter!!.notifyDataSetChanged()
    }
}
