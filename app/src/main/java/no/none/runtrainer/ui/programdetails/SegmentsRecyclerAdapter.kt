package no.none.runtrainer.ui.programdetails

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import mu.KotlinLogging
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.formatDurationToMMSSString
import java.util.regex.Pattern


class SegmentsRecyclerAdapter(private val segmentsList: List<DataVOs.Segment>) : RecyclerView.Adapter<SegmentsRecyclerAdapter.MyViewHolder>() {

    private val logger = KotlinLogging.logger {}

    interface ClickListener {
        fun distanceOnClick(v: View, position: Int)
        fun speedOnClick(v: View, position: Int)
        fun bpmOnClick(v: View, position: Int)
        fun deleteButtonOnClick(v: View, position: Int)
    }

    private var listener:ClickListener? = null
    private var distanceCallback: ((Int) -> Unit)? = null
    private var speedCallback: ((Int) -> Unit)? = null
    private var bpmCallback: ((Int) -> Unit)? = null
    private var deleteCallback: ((Int) -> Unit)? = null

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById<TextView>(R.id.title)
        var distance: EditText = view.findViewById<EditText>(R.id.distance)
        var speed: EditText = view.findViewById<EditText>(R.id.speed)
        var bpm: EditText = view.findViewById<EditText>(R.id.bpm)
        var stepType: Spinner = view.findViewById<Spinner>(R.id.step_type)
        var deleteBtn: ImageButton = view.findViewById<ImageButton>(R.id.delete_btn)
    }


    fun setListeners(listener: ClickListener){
        this.listener = listener
    }

    fun setDistanceOnClick(callback:(position: Int) -> Unit){
        distanceCallback = callback
    }

    fun setSpeedOnClick(callback: (position: Int) -> Unit){
        speedCallback = callback
    }

    fun setBpmOnClick(callback: (position: Int) -> Unit){
        bpmCallback = callback
    }

    fun setDeleteOnClick(callback: (position: Int) -> Unit){
        deleteCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_program_details_distance_segment, parent, false)

        val holder = MyViewHolder(itemView)

        holder.stepType.adapter = ArrayAdapter(parent.context, android.R.layout.simple_spinner_item, DataVOs.StepType.values())

        /*holder.distance.setOnFocusChangeListener { it, hasFocus ->
            if(!hasFocus){
                segmentsList[holder.adapterPosition].distance = extractFloat(holder.distance.text.toString())
                val newText = segmentsList[holder.adapterPosition].distance.toString() + " Km"
                holder.distance.setText(newText)
            }else{
                holder.distance.setText(segmentsList[holder.adapterPosition].distance.toString())
            }
        }*/

        holder.distance.setOnClickListener {
            listener?.distanceOnClick(it, holder.adapterPosition)
            distanceCallback?.invoke(holder.adapterPosition)
        }

        holder.speed.setOnClickListener {
            listener?.speedOnClick(it, holder.adapterPosition)
            speedCallback?.invoke(holder.adapterPosition)
        }

        holder.bpm.setOnClickListener {
            listener?.bpmOnClick(it, holder.adapterPosition)
            bpmCallback?.invoke(holder.adapterPosition)
        }


        holder.stepType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                segmentsList[holder.adapterPosition].stepType = (p0?.selectedItem as DataVOs.StepType)
            }
        }

        holder.deleteBtn.setOnClickListener(View.OnClickListener { v ->
            listener?.deleteButtonOnClick(v, holder.adapterPosition)
            deleteCallback?.invoke(holder.adapterPosition)
        })


        return holder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val segments = segmentsList[position]
        holder.title.text = segments.title
        val distanceStr = segments.distance.toString() + " Km"
        holder.distance.setText(distanceStr)
        val speedStr = segments.speedMillisecondsPerKm.formatDurationToMMSSString() + " min/Km"
        holder.speed.setText(speedStr)
        val bpmStr = segments.bpm.toString() + " BPM"
        holder.bpm.setText(bpmStr)
        holder.stepType.setSelection(segments.stepType.type-1)
    }

    override fun getItemCount(): Int {
        return segmentsList.size
    }

    private fun appendSuffix(string:String, suffix:String):String{
        return string + suffix
    }

    private fun extractFloat(string:String):Float{
        logger.info ("string:$string")
        val p = Pattern.compile("[0-9.]+")
        val m = p.matcher(string)
        logger.info ("m:$m")
        logger.info ("m.lookingAt():${m.lookingAt()}")
        if(m!=null && (m.lookingAt()))
        {
            logger.info ("m.group(0):${m.group(0)}")
            return m.group(0).toFloat()
        }
        else
        {
            return 0.0f
        }
    }
}