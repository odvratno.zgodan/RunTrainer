package no.none.runtrainer.ui.programdetails

import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.github.florent37.expansionpanel.ExpansionLayout
import com.github.florent37.expansionpanel.viewgroup.ExpansionLayoutCollection
import mu.KotlinLogging
import no.none.runtrainer.R
import no.none.runtrainer.data.DataVOs
import no.none.runtrainer.formatDurationToMMSSString
import no.none.runtrainer.formatDurationToSpeechMMSSString
import org.jetbrains.anko.backgroundColor
import java.util.*
import java.util.regex.Pattern


class SegmentsExpandableRecyclerAdapter(private val segmentsList: ArrayList<DataVOs.Segment>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private val logger = KotlinLogging.logger {}

    private val DISTANCE_TYPE = 0
    private val DURATION_TYPE = 1
    private val POWER_TYPE = 2
    private val GROUP_TYPE = 3

    private val expansionsCollection = ExpansionLayoutCollection()
    private lateinit var parentContext:Context

    private var itemsToRemove = mutableListOf<DataVOs.Segment>()

    init {
        expansionsCollection.openOnlyOne(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder:RecyclerView.ViewHolder
        val subView:View
        parentContext = parent.context
        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_program_details_segment_expandable, parent, false)
        val expansionLayout:ExpansionLayout = itemView.findViewById(R.id.expansionLayout)
        when (viewType) {
            DISTANCE_TYPE -> {
                holder = DistanceSegmentViewHolder(itemView)
                subView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_program_details_distance_segment, parent, false)
            }
            DURATION_TYPE -> {
                holder = DurationSegmentViewHolder(itemView)
                subView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_program_details_duration_segment, parent, false)
            }
            GROUP_TYPE -> {
                holder = GroupSegmentViewHolder(itemView)
                subView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_program_details_group_segment, parent, false)
            }
            else -> {
                holder = DistanceSegmentViewHolder(itemView)
                subView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_program_details_distance_segment, parent, false)
            }
        }
        expansionLayout.addView(subView)

        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemViewType = getItemViewType(position)
        val segment = segmentsList[position]
        when (itemViewType) {
            DISTANCE_TYPE -> {
                (holder as DistanceSegmentViewHolder).bind(segment)
                expansionsCollection.add(holder.expansionLayout)
            }
            DURATION_TYPE -> {
                (holder as DurationSegmentViewHolder).bind(segment)
                expansionsCollection.add(holder.expansionLayout)
            }
            GROUP_TYPE -> {
                (holder as GroupSegmentViewHolder).bind(segment)
                expansionsCollection.add(holder.expansionLayout)
            }
        }
    }



    override fun getItemCount(): Int {
        return segmentsList.size
    }

    fun getItemData(position: Int):DataVOs.Segment{
        return segmentsList[position]
    }

    fun deleteItem(position: Int){
        listener?.deleteButtonOnClick(position)
        deleteCallback?.invoke(position)
    }

    private fun performSwapItems(fromPosition: Int, toPosition: Int){
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                segmentsList[i] = segmentsList.set(i+1, segmentsList[i])
            }
        } else {
            for (i in fromPosition..toPosition + 1) {
                segmentsList[i] = segmentsList.set(i-1, segmentsList[i])
            }
        }

        notifyItemMoved(fromPosition, toPosition)
    }

    fun dragStarted(position: Int){
        var item= getItemData(position)
        if(item.segmentType==DataVOs.SegmentType.GROUP){
            val items = segmentsList.filter { it.parentSegmentUid!="" }
            var i=0
            while(i<segmentsList.size){
                if(segmentsList[i].parentSegmentUid!=""){
                    itemsToRemove.add(segmentsList.removeAt(i))
                    notifyItemRemoved(i)
                }else{
                    i++
                }
            }
        }
    }

    fun dragEnded(){
        if(itemsToRemove.size>0) {
            while(itemsToRemove.size>0){
                val position = segmentsList.indexOfLast { it.parentSegmentUid == itemsToRemove.first().parentSegmentUid || it.uid == itemsToRemove.first().parentSegmentUid }
                segmentsList.add(position+1, itemsToRemove.removeAt(0))
                notifyItemInserted(position+1)
            }
        }
    }

    /**
     * Function called to swap dragged items
     */
    fun swapItems(fromPosition: Int, toPosition: Int) {

        var item= getItemData(fromPosition)
        var target = getItemData(toPosition)

        // Check if the item that is moved is an instance of SegmentItem, otherwise just move it
        if(item.segmentType!=DataVOs.SegmentType.GROUP){
            var itemParent:DataVOs.Segment? = null
            var targetParent:DataVOs.Segment? = null
            if(item.parentSegmentUid!="") itemParent = segmentsList.find { it.parentSegmentUid==item.parentSegmentUid }
            if(target.parentSegmentUid!="") targetParent = segmentsList.find { it.parentSegmentUid==target.parentSegmentUid }

            // Move sub segments inside same group
            if(itemParent!=null && itemParent==targetParent) {
                performSwapItems(fromPosition, toPosition)
            }
            // Move segment that is not part of a group and move it only if the targetParent is not a group
            else if(itemParent==null && targetParent==null){
                performSwapItems(fromPosition, toPosition)
            }
        }else{
            var targetParent:DataVOs.Segment? = null
            if(target.parentSegmentUid!="") targetParent = segmentsList.find { it.parentSegmentUid==target.parentSegmentUid }
            // Move the group only if the target is a SegmentItem that is not part of a group
            if(targetParent==null && target.parentSegmentUid==""){
                performSwapItems(fromPosition, toPosition)
            }
        }
    }

    inner class DistanceSegmentViewHolder(val view: View) : RecyclerView.ViewHolder(view), ISegmentViewHolder {

        lateinit var title: TextView
        lateinit var details: TextView
        lateinit var distance: EditText
        lateinit var speed: EditText
        lateinit var bpm: EditText
        lateinit var stepType: Spinner
        lateinit var deleteBtn: ImageButton

        var expansionLayout: ExpansionLayout? = null

        init {
            expansionLayout = view.findViewById(R.id.expansionLayout)
        }

        override fun bind(segment:DataVOs.Segment) {
            title = view.findViewById(R.id.title)
            details = view.findViewById(R.id.details)
            distance = view.findViewById(R.id.distance)
            speed = view.findViewById(R.id.speed)
            bpm = view.findViewById(R.id.bpm)
            stepType = view.findViewById(R.id.step_type)
            deleteBtn = view.findViewById(R.id.delete_btn)

            distance.setOnClickListener {
                listener?.distanceOnClick(adapterPosition)
                distanceCallback?.invoke(adapterPosition)
            }

            speed.setOnClickListener {
                listener?.speedOnClick(adapterPosition)
                speedCallback?.invoke(adapterPosition)
            }



            bpm.setOnClickListener {
                listener?.bpmOnClick(adapterPosition)
                bpmCallback?.invoke(adapterPosition)
            }

            val adapter = ArrayAdapter(parentContext, android.R.layout.simple_spinner_item, DataVOs.StepType.values())
            stepType.adapter = adapter
            stepType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    //
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    segmentsList[adapterPosition].stepType = (p0?.selectedItem as DataVOs.StepType)
                }
            }

            deleteBtn.setOnClickListener {
                deleteItem(adapterPosition)
            }

            val distanceStr = segment.distance.toString() + " Km"
            distance.setText(distanceStr)
            val speedStr = segment.speedMillisecondsPerKm.formatDurationToMMSSString() + " min/Km"
            speed.setText(speedStr)
            val bpmStr = segment.bpm.toString() + " BPM"
            bpm.setText(bpmStr)
            stepType.setSelection(segment.stepType.type)

            val detailText = segment.distance.toString() + "Km " +
                    segment.speedMillisecondsPerKm.formatDurationToMMSSString() +
                    "min/Km " + segment.bpm.toString() + "BPM"

            title.text = segment.title
            details.text = detailText
        }
    }

    inner class DurationSegmentViewHolder(val view: View) : RecyclerView.ViewHolder(view), ISegmentViewHolder {

        lateinit var title: TextView
        lateinit var details: TextView
        lateinit var duration: EditText
        lateinit var speed: EditText
        lateinit var bpm: EditText
        lateinit var stepType: Spinner
        lateinit var deleteBtn: ImageButton

        var expansionLayout: ExpansionLayout? = null

        init {
            expansionLayout = view.findViewById(R.id.expansionLayout)
        }

        override fun bind(segment:DataVOs.Segment) {
            title = view.findViewById(R.id.title)
            details = view.findViewById(R.id.details)
            duration = view.findViewById(R.id.duration)
            speed = view.findViewById(R.id.speed)
            bpm = view.findViewById(R.id.bpm)
            stepType = view.findViewById(R.id.step_type)
            deleteBtn = view.findViewById(R.id.delete_btn)

            duration.setOnClickListener {
                listener?.durationOnClick(adapterPosition)
                durationCallback?.invoke(adapterPosition)
            }

            speed.setOnClickListener {
                listener?.speedOnClick(adapterPosition)
                speedCallback?.invoke(adapterPosition)
            }



            bpm.setOnClickListener {
                listener?.bpmOnClick(adapterPosition)
                bpmCallback?.invoke(adapterPosition)
            }

            stepType.adapter = ArrayAdapter(parentContext, android.R.layout.simple_spinner_item, DataVOs.StepType.values())
            stepType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    //
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    segmentsList[adapterPosition].stepType = (p0?.selectedItem as DataVOs.StepType)
                }
            }

            deleteBtn.setOnClickListener {
                deleteItem(adapterPosition)
            }

            val durationStr = segment.duration.formatDurationToSpeechMMSSString()
            duration.setText(durationStr)
            val speedStr = segment.speedMillisecondsPerKm.formatDurationToMMSSString() + " min/Km"
            speed.setText(speedStr)
            val bpmStr = segment.bpm.toString() + " BPM"
            bpm.setText(bpmStr)
            stepType.setSelection(segment.stepType.type)

            val detailText = segment.duration.formatDurationToSpeechMMSSString() + "\n" +
                    segment.speedMillisecondsPerKm.formatDurationToMMSSString() +
                    "min/Km " + segment.bpm.toString() + "BPM"

            title.text = segment.title
            details.text = detailText
        }
    }

    inner class GroupSegmentViewHolder(val view: View) : RecyclerView.ViewHolder(view), ISegmentViewHolder {
        lateinit var header: RelativeLayout
        lateinit var title: TextView
        lateinit var details: TextView
        lateinit var distance: EditText
        lateinit var duration: EditText
        lateinit var deleteBtn: ImageButton

        private var recyclerView: RecyclerView? = null
        private var sAdapter: SegmentsExpandableRecyclerAdapter? = null

        var expansionLayout: ExpansionLayout? = null

        init {
            expansionLayout = view.findViewById(R.id.expansionLayout)
        }

        override fun bind(segment:DataVOs.Segment) {
            header = view.findViewById(R.id.header)
            header.backgroundColor = ContextCompat.getColor(view.context, R.color.colorAccentSecondary)
            title = view.findViewById(R.id.title)
            details = view.findViewById(R.id.details)
            distance = view.findViewById(R.id.distance)
            duration = view.findViewById(R.id.duration)
            deleteBtn = view.findViewById(R.id.delete_btn)

            distance.setOnClickListener {
                listener?.distanceOnClick(adapterPosition)
                distanceCallback?.invoke(adapterPosition)
            }

            duration.setOnClickListener {
                listener?.durationOnClick(adapterPosition)
                durationCallback?.invoke(adapterPosition)
            }

            deleteBtn.setOnClickListener {
                deleteItem(adapterPosition)
            }

            val distanceStr = segment.distance.toString() + " Km"
            distance.setText(distanceStr)

            val durationStr = segment.duration.formatDurationToSpeechMMSSString()
            duration.setText(durationStr)
            val speedStr = segment.speedMillisecondsPerKm.formatDurationToMMSSString() + " min/Km"

            val detailText = segment.duration.formatDurationToSpeechMMSSString() + "\n" +
                    segment.distance.toString() + "Km "

            title.text = segment.title
            details.text = detailText

        }
    }

    interface ISegmentViewHolder{
        fun bind(segment:DataVOs.Segment)
    }

    //--------------- Exposed methods ---------------//

    interface ClickListener {
        fun distanceOnClick(position: Int)
        fun durationOnClick(position: Int)
        fun speedOnClick(position: Int)
        fun bpmOnClick(position: Int)
        fun deleteButtonOnClick(position: Int)
    }

    private var listener:ClickListener? = null
    private var distanceCallback: ((Int) -> Unit)? = null
    private var durationCallback: ((Int) -> Unit)? = null
    private var speedCallback: ((Int) -> Unit)? = null
    private var bpmCallback: ((Int) -> Unit)? = null
    private var deleteCallback: ((Int) -> Unit)? = null


    fun setListeners(listener: ClickListener){
        this.listener = listener
    }

    fun setDistanceOnClick(callback:(position: Int) -> Unit){
        distanceCallback = callback
    }
    fun setDurationOnClick(callback:(position: Int) -> Unit){
        durationCallback = callback
    }

    fun setSpeedOnClick(callback: (position: Int) -> Unit){
        speedCallback = callback
    }

    fun setBpmOnClick(callback: (position: Int) -> Unit){
        bpmCallback = callback
    }

    fun setDeleteOnClick(callback: (position: Int) -> Unit){
        deleteCallback = callback
    }



    override fun getItemViewType(position: Int): Int {
        return if (segmentsList[position].segmentType==DataVOs.SegmentType.DISTANCE) {
            DISTANCE_TYPE
        } else if (segmentsList[position].segmentType==DataVOs.SegmentType.DURATION) {
            DURATION_TYPE
        }else if (segmentsList[position].segmentType==DataVOs.SegmentType.GROUP) {
            GROUP_TYPE
        }else{
            -1
        }
    }

    private fun appendSuffix(string:String, suffix:String):String{
        return string + suffix
    }

    private fun extractFloat(string:String):Float{
        val p = Pattern.compile("[0-9.]+")
        val m = p.matcher(string)
        if(m!=null && (m.lookingAt()))
        {
            return m.group(0).toFloat()
        }
        else
        {
            return 0.0f
        }
    }

    //--------------- Item decoration ---------------//
    class SubItemDecoration(val context:Context, val orientation:Int, var spacing: Int):DividerItemDecoration(context, orientation){

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            if(parent.adapter is SegmentsExpandableRecyclerAdapter){
                val adapter = parent.adapter as SegmentsExpandableRecyclerAdapter
                val position = parent.getChildAdapterPosition(view)
                if (position!=-1 && adapter!!.getItemData(position).parentSegmentUid!="") {
                    outRect.left = spacing
                }
            }
        }
    }


}