# RunTrainer

##### TODO
1. - [ ] Change Segment so that it supports multiple types
    * **Types: normal, sprint, recovery/pause(no timing)**
    * eg. duration based insted of distance, for sprints
    * sprint is an example of duration based instead of distance based
2. - [*] Change **Add segment** UI to use the expanding floating button instead of the static button
    * in that case the floating button could nest the other segment types
    * it would clean up the view
    * add a duplicate button to the segment item view
    * enable reordering of segments
3. - [*] Change ProgramDetails save button from floating to something else
4. - [*] Fix mime-type when uploading to GoogleDrive through upload.sh script, now it's uploaded as an archive and when trying to install on smartphone it doesn't give an option to Install only to unzip
5. - [ ] Create Docker image to speed up CI jobs(don't forget to turn on caching)
6. - [*] Add Firebase authentication
7. - [*] Save user programs and segments to Firebase DB